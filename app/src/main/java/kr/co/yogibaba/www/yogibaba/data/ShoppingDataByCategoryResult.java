package kr.co.yogibaba.www.yogibaba.data;

import java.util.List;

/**
 * Created by jah on 2016-05-23.
 */
public class ShoppingDataByCategoryResult {
    public String  message;
    public List<ShoppingDataByCategory> item;
}
