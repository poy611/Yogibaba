package kr.co.yogibaba.www.yogibaba.data.order;

import java.util.List;

/**
 * Created by jah on 2016-05-30.
 */
public class OrderAndDeliveryResult {
    public String message;
    public  List<OrderAndDeliveryPurchases> purchases;

    public List<OrderAndDeliveryPurchases> getPurchases() {
        return purchases;
    }

    public void setPurchases(List<OrderAndDeliveryPurchases> purchases) {
        this.purchases = purchases;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
