package kr.co.yogibaba.www.yogibaba.data.order;

import java.util.List;

/**
 * Created by jah on 2016-05-25.
 */
public class OrderDetailPurchases {
    //OrderDetailSuccess -> OrderDetailResult -> OrderDetailPurchases -> OrderDetailPurchaseItem
    public int purchase_id;
    public String purchase_date;
    public String purchase_number;
    public List<OrderDetailPurchaseItem> purchase_item;

    public int order_category;
    public String recipient_name;
    public  int total_price;
    public String recipient_phone_number;
    public String recipient_address;
    public int delivery_charge;

    public String orderer_name;
    public String orderer_email;
    public String orderer_phone_number;

    public int getPurchase_id() {
        return purchase_id;
    }

    public void setPurchase_id(int purchase_id) {
        this.purchase_id = purchase_id;
    }

    public String getPurchase_date() {
        return purchase_date;
    }

    public void setPurchase_date(String purchase_date) {
        this.purchase_date = purchase_date;
    }

    public String getPurchase_number() {
        return purchase_number;
    }

    public void setPurchase_number(String purchase_number) {
        this.purchase_number = purchase_number;
    }

    public List<OrderDetailPurchaseItem> getPurchase_item() {
        return purchase_item;
    }

    public void setPurchase_item(List<OrderDetailPurchaseItem> purchase_item) {
        this.purchase_item = purchase_item;
    }

    public int getOrder_category() {
        return order_category;
    }

    public void setOrder_category(int order_category) {
        this.order_category = order_category;
    }

    public String getRecipient_name() {
        return recipient_name;
    }

    public void setRecipient_name(String recipient_name) {
        this.recipient_name = recipient_name;
    }

    public int getTotal_price() {
        return total_price;
    }

    public void setTotal_price(int total_price) {
        this.total_price = total_price;
    }

    public String getRecipient_phone_number() {
        return recipient_phone_number;
    }

    public void setRecipient_phone_number(String recipient_phone_number) {
        this.recipient_phone_number = recipient_phone_number;
    }

    public String getRecipient_address() {
        return recipient_address;
    }

    public void setRecipient_address(String recipient_address) {
        this.recipient_address = recipient_address;
    }

    public int getDelivery_charge() {
        return delivery_charge;
    }

    public void setDelivery_charge(int delivery_charge) {
        this.delivery_charge = delivery_charge;
    }

    public String getOrderer_name() {
        return orderer_name;
    }

    public void setOrderer_name(String orderer_name) {
        this.orderer_name = orderer_name;
    }

    public String getOrderer_email() {
        return orderer_email;
    }

    public void setOrderer_email(String orderer_email) {
        this.orderer_email = orderer_email;
    }

    public String getOrderer_phone_number() {
        return orderer_phone_number;
    }

    public void setOrderer_phone_number(String orderer_phone_number) {
        this.orderer_phone_number = orderer_phone_number;
    }
}
