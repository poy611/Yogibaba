package kr.co.yogibaba.www.yogibaba.settingTab.orderAndDelivery;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import kr.co.yogibaba.www.yogibaba.R;
import kr.co.yogibaba.www.yogibaba.data.order.OrderDetailPurchases;

/**
 * Created by jah on 2016-05-26.
 */
public class OrderDetailListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private static final int VIEW_TYPE_TOP = 1;
    private static final int VIEW_TYPE_MID = 2;
    private static final int VIEW_TYPE_BOTTOM = 3;

    //    List<OrderDetailPurchaseItem> pData=new ArrayList<>();
    OrderDetailPurchases purchasesData;

    public void setPurchasesData(OrderDetailPurchases purchasesData) {
        this.purchasesData = purchasesData;
        notifyDataSetChanged();
    }
//
//    void add(OrderDetailPurchaseItem data)
//    {
//        pData.add(data);
//        notifyDataSetChanged();
//    }
//    void addAll(List<OrderDetailPurchaseItem> mData)
//    {
//        this.pData.addAll(mData);
//        notifyDataSetChanged();
//    }
//    void clear(){
//        pData.clear();
//        notifyDataSetChanged();
//    }



    @Override
    public int getItemViewType(int position) {
        int size;
        size = purchasesData.getPurchase_item().size();
        if(position ==0){
            return VIEW_TYPE_TOP;
        }
        if(position == size+1) {
            return VIEW_TYPE_BOTTOM;
        }

        return VIEW_TYPE_MID;

//        throw new IllegalArgumentException("invalid position");
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_TOP : {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_order_detail_header_entry, null);
                return new OrderDetailDateViewHolder(view);
            }

            case VIEW_TYPE_MID : {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_order_item_list, null);
                return new OrderContentViewHolder(view);
            }
            case VIEW_TYPE_BOTTOM : {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_order_detail_bottom_entry, null);
                return new OrderDetailFinishViewHolder(view);
            }
        }
        throw new IllegalArgumentException("invalid position");
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int size;
        size = purchasesData.getPurchase_item().size();
        if(position ==0){
            OrderDetailDateViewHolder header = (OrderDetailDateViewHolder) holder;
            header.setOrderDetailDate(purchasesData);
            return;
        }
        if(position == size+1) {
            OrderDetailFinishViewHolder footer = (OrderDetailFinishViewHolder)holder;
            footer.setOrderConditionData(purchasesData);//(mData.get(mData.size()));
            return;
        }
        OrderContentViewHolder h = (OrderContentViewHolder) holder;
        h.setOrderDetailContentData(purchasesData.getPurchase_item().get(position-1));
        return;
    }

    @Override
    public int getItemCount() {
        if (purchasesData == null) return 0;
        int size;
        size = (purchasesData.getPurchase_item().size())+2;
        return size;

    }
}
