package kr.co.yogibaba.www.yogibaba.data.order;

import java.util.List;

/**
 * Created by jah on 2016-05-25.
 */
public class OrderAndDeliveryPurchases {
    //OrderAndDeliverySuccess -> OrderAndDeliveryResult -> OrderAndDeliveryPurchases -> OrderAndDeliveryData
    public int purchase_id;
    public int total_price;
    public int order_category;
    public String purchase_date;
    public List<OrderAndDeliveryData> purchase_items;

    public int getTotal_price() {
        return total_price;
    }

    public void setTotal_price(int total_price) {
        this.total_price = total_price;
    }

    public int getPurchase_id() {
        return purchase_id;
    }

    public void setPurchase_id(int purchase_id) {
        this.purchase_id = purchase_id;
    }

    public String getPurchase_date() {
        return purchase_date;
    }

    public void setPurchase_date(String purchase_date) {
        this.purchase_date = purchase_date;
    }



    public int getOrder_category() {
        return order_category;
    }

    public void setOrder_category(int order_category) {
        this.order_category = order_category;
    }

    public List<OrderAndDeliveryData> getPurchase_item() {
        return purchase_items;
    }

    public void setPurchase_item(List<OrderAndDeliveryData> purchase_item) {
        this.purchase_items = purchase_item;
    }
}
