package kr.co.yogibaba.www.yogibaba.search;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentTabHost;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.TextView;

import kr.co.yogibaba.www.yogibaba.CustomRecyclerDivider;
import kr.co.yogibaba.www.yogibaba.R;
import kr.co.yogibaba.www.yogibaba.VrPlayerActivity;
import kr.co.yogibaba.www.yogibaba.data.search.SearchItem;
import kr.co.yogibaba.www.yogibaba.data.search.SearchVideo;
import kr.co.yogibaba.www.yogibaba.manager.TypefaceManager;
import kr.co.yogibaba.www.yogibaba.search.shoppingsearch.ResultDivier;
import kr.co.yogibaba.www.yogibaba.search.shoppingsearch.ShoppingSearchAdpater;
import kr.co.yogibaba.www.yogibaba.search.shoppingsearch.ShoppingSearchViewHolder;
import kr.co.yogibaba.www.yogibaba.search.videosearch.VideoSearchAdapter;
import kr.co.yogibaba.www.yogibaba.search.videosearch.VideoSearchViewHolder;
import kr.co.yogibaba.www.yogibaba.shoppingtab.shoppingdetail.ShoppingDetailActivity;
import kr.co.yogibaba.www.yogibaba.videoTab.videodetail.VideoDetailActivity;

public class SearchActivity extends AppCompatActivity {
    EditText editText;
    //Button button;
    FragmentTabHost tabHost;
    TextView textView;
    TextView textView1;
    ImageView imageView;
    ImageView imageView1;
    AppCompatDelegate mDelegate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //글꼴테스트
        if (Build.VERSION.SDK_INT < 11){
            getLayoutInflater().setFactory(this);
        } else {
            getLayoutInflater().setFactory2(this);
        }
        mDelegate = getDelegate();
        //글꼴테스트
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.searchbtn);
        getSupportActionBar().setTitle(null);
        View v= LayoutInflater.from(this).inflate(R.layout.view_tab_select,null);
        textView=(TextView)v.findViewById(R.id.tab_name);
        imageView=(ImageView)v.findViewById(R.id.img);
        textView.setText("영상");
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tabHost.setCurrentTab(0);
            }
        });
        editText=(EditText)findViewById(R.id.edit_search);

        View v1= LayoutInflater.from(this).inflate(R.layout.view_tab_select_shopping,null);
        textView1=(TextView)v1.findViewById(R.id.tab_name);
        imageView1=(ImageView)v1.findViewById(R.id.img);
        imageView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tabHost.setCurrentTab(1);
            }
        });
        textView1.setText("쇼핑");

        editText.setHint("검색어를 입력해 주세요");
        editText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    if (!editText.getText().toString().equals("") && !editText.getText().equals(null)) {
                        setChangedAdapter();
                    }
                    return true;
                }
                return false;
            }
        });

        tabHost=(FragmentTabHost)findViewById(R.id.tab_Host);
        tabHost.setup(this, getSupportFragmentManager(), android.R.id.tabcontent);

        tabHost.addTab(tabHost.newTabSpec("videosearch").setIndicator(v), VideoSearchFragment.class, null);//);
        tabHost.addTab(tabHost.newTabSpec("shoppingsearch").setIndicator(v1), ShoppingSearchFragment.class, null);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id=item.getItemId();
        if(id==android.R.id.home){
            if(!editText.getText().toString().equals("") && !editText.getText().equals(null)) {
                setChangedAdapter();
            }
        }
        if(id==R.id.menu_search_cancle)
        {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
    void setChangedAdapter(){
        final String keyword=editText.getText().toString();
        editText.setText(null);

        if(tabHost.getCurrentTab()==0){

            final VideoSearchFragment fragment=(VideoSearchFragment)getSupportFragmentManager().findFragmentByTag("videosearch");
            fragment.gridLayoutManager.setOrientation(GridLayout.VERTICAL);
            fragment.gridLayoutManager.setSpanCount(1);
            fragment.relativeLayout.setBackgroundColor(getResources().getColor(android.R.color.transparent));

            fragment.recyclerView.setLayoutManager(fragment.gridLayoutManager);

            fragment.recyclerView.addItemDecoration(new CustomRecyclerDivider(SearchActivity.this));
            if(fragment.mAdapter==null) {
                fragment.mAdapter = new VideoSearchAdapter();
                Log.i("inputChange","Empty");
            }
            else{
                fragment.mAdapter.clear();
                Log.i("inputChange","Change");
            }

            fragment.recyclerView.setAdapter(fragment.mAdapter);
            fragment.spinner.setVisibility(View.VISIBLE);
            fragment.textView.setVisibility(View.VISIBLE);
            fragment.recomandText.setVisibility(View.INVISIBLE);
            fragment.star.setVisibility(View.INVISIBLE);
            fragment.setData(fragment.spinner.getSelectedItem().toString(), keyword);//fragment.spinner.getPrompt().toString(), keyword);


            fragment.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    fragment.setData(parent.getItemAtPosition(position).toString(), keyword);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            fragment.mAdapter.setOnItemClicListener(new VideoSearchViewHolder.OnItemClickListener() {
                @Override
                public void onItemClick(View view, SearchVideo product) {
                    Intent i = new Intent(SearchActivity.this, VideoDetailActivity.class);
                    i.putExtra(VideoDetailActivity.MOVIE_ID, product.getMovie_id());
                    startActivity(i);
                }
            });
            fragment.mAdapter.setOnButtonClickListener(new VideoSearchViewHolder.OnButtonClickListener() {
                @Override
                public void onButtonClick(View view, SearchVideo product) {
                    Intent i = new Intent(SearchActivity.this, VrPlayerActivity.class);
                    Uri uri=Uri.parse(product.getMovie_url());
                    i.setDataAndType(uri, "video/mp4");
                    i.putExtra(VrPlayerActivity.MOVIE_ID, product.getMovie_id());
                    startActivity(i);
                }
            });


        }
        else{

            final ShoppingSearchFragment fragment=(ShoppingSearchFragment)getSupportFragmentManager().findFragmentByTag("shoppingsearch");
            fragment.gridLayoutManager.setOrientation(GridLayout.VERTICAL);
            fragment.gridLayoutManager.setSpanCount(2);
            fragment.relativeLayout.setBackgroundColor(getResources().getColor(android.R.color.transparent));
            fragment.recyclerView.setLayoutManager(fragment.gridLayoutManager);
            fragment.recyclerView.addItemDecoration(new ResultDivier(SearchActivity.this));
            if(fragment.mAdapter==null){
                fragment.mAdapter=new ShoppingSearchAdpater();
            }
            else{
                fragment.mAdapter.clear();
            }

            fragment.mAdapter.setOnItemClicListener(new ShoppingSearchViewHolder.OnItemClickListener() {
                @Override
                public void onItemClick(View view, SearchItem product) {
                    Intent i = new Intent(SearchActivity.this, ShoppingDetailActivity.class);
                    i.putExtra(ShoppingDetailActivity.ITEM_ID, product.getItem_id());
                    startActivity(i);
                }
            });

            fragment.mAdapter.setOnLikeButtonClickListener(new ShoppingSearchViewHolder.OnLikeButtonClickListener() {
                @Override
                public void onButtonClick(View view, SearchItem product) {
                    /*if (!product.is_like()) {
                        NetworkManagerOh.getInstance().onClickLikeButton(VideoDetailActivity.this
                                , product.getItem_id()
                                , user_id
                                , new NetworkManagerOh.OnResultListener<Click>() {
                            @Override
                            public void onSuccess(Request request, Click result) {
                                Toast.makeText(VideoDetailActivity.this, result.result.getMessage(), Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onFail(Request request, IOException exception) {
                                Toast.makeText(VideoDetailActivity.this, "네트워크 에러", Toast.LENGTH_SHORT).show();
                            }
                        });
                    } else {
                        NetworkManagerOh.getInstance().onClickUnLikeButton(VideoDetailActivity.this
                                , product.getItem_id()
                                , user_id
                                , new NetworkManagerOh.OnResultListener<Click>() {
                            @Override
                            public void onSuccess(Request request, Click result) {
                                Toast.makeText(VideoDetailActivity.this, result.result.getMessage(), Toast.LENGTH_SHORT).show();

                            }

                            @Override
                            public void onFail(Request request, IOException exception) {
                                Toast.makeText(VideoDetailActivity.this, "네트워크 에러", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }*/
                }
            });

            fragment.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    fragment.setData(parent.getItemAtPosition(position).toString(), keyword);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            fragment.recyclerView.setAdapter(fragment.mAdapter);
            fragment.spinner.setVisibility(View.VISIBLE);
            fragment.textView.setVisibility(View.VISIBLE);
            fragment.recomandText.setVisibility(View.INVISIBLE);
            fragment.star.setVisibility(View.INVISIBLE);
            fragment.setData(fragment.spinner.getSelectedItem().toString(), keyword);



        }
    }
    public View setCustomFont(View view, String name, Context context, AttributeSet attrs){
        if(view == null){
            if(name.equals("TextView")){
                view = new TextView(context,attrs);
            }
        }
        if (view != null && view instanceof TextView){
            TypedArray typedArray = context.obtainStyledAttributes(attrs, new int[]{android.R.attr.fontFamily});
            String fontFamily = typedArray.getString(0);
            typedArray.recycle();
            Typeface typeface = TypefaceManager.getInstance().getTypeface(context,fontFamily);
            if (typeface != null){
                TextView textView = (TextView)view;
                textView.setTypeface(typeface);
            }
        }
        return view;
    }

    @Override
    public View onCreateView(String name, Context context, AttributeSet attrs) {
        View view = super.onCreateView(name, context, attrs);
        if(Build.VERSION.SDK_INT < 11){
            if (view == null){
                view = mDelegate.createView(null, name, context, attrs);
                view = setCustomFont(view, name, context, attrs);
            }
        }
        return view;
    }

    @Override
    public View onCreateView(View parent, String name, Context context, AttributeSet attrs) {
        View view  = super.onCreateView(parent, name, context, attrs);
        if (view == null){
            view = mDelegate.createView(parent, name, context, attrs);
        }
        view = setCustomFont(view, name, context, attrs);

        return  view;
    }
}
