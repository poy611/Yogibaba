package kr.co.yogibaba.www.yogibaba.data.like;

/**
 * Created by jah on 2016-05-30.
 */
public class LikesVideoList {
    int edit_Type = 0;

    public int getEdit_Type() {
        return edit_Type;
    }

    public void setEdit_Type(int edit_Type) {
        this.edit_Type = edit_Type;
    }

    public int movie_id;
    public String movie_title;
    public String movie_strapline;
    public int movie_hit;
    public String movie_url;
    public String movie_thumbnail;

    public int getMovie_id() {
        return movie_id;
    }

    public void setMovie_id(int movie_id) {
        this.movie_id = movie_id;
    }

    public String getMovie_title() {
        return movie_title;
    }

    public void setMovie_title(String movie_title) {
        this.movie_title = movie_title;
    }

    public String getMovie_strapline() {
        return movie_strapline;
    }

    public void setMovie_strapline(String movie_strapline) {
        this.movie_strapline = movie_strapline;
    }

    public int getMovie_hit() {
        return movie_hit;
    }

    public void setMovie_hit(int movie_hit) {
        this.movie_hit = movie_hit;
    }

    public String getMovie_url() {
        return movie_url;
    }

    public void setMovie_url(String movie_url) {
        this.movie_url = movie_url;
    }

    public String getMovie_thumbnail() {
        return movie_thumbnail;
    }

    public void setMovie_thumbnail(String movie_thumbnail) {
        this.movie_thumbnail = movie_thumbnail;
    }
}
