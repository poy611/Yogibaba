package kr.co.yogibaba.www.yogibaba.data.shoppingdetail.buy;

/**
 * Created by OhDaeKyoung on 2016. 5. 19..
 */
public class BuyProduct {
    String message;
    BuyProductData purchase_item;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
