package kr.co.yogibaba.www.yogibaba.manager;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import kr.co.yogibaba.www.yogibaba.MyApplication;

/**
 * Created by OhDaeKyoung on 2016. 5. 23..
 */
public class PropertyManager {
    private static PropertyManager instance;
    public static PropertyManager getInstance() {
        if (instance == null) {
            instance = new PropertyManager();
        }
        return instance;
    }
    SharedPreferences mPrefs;
    SharedPreferences.Editor mEditor;

    private PropertyManager() {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(MyApplication.getContext());
        mEditor = mPrefs.edit();
    }

    private static final String USER_ID="userid";

    public void setUserId(String userId){
        mEditor.putString(USER_ID, userId);
        mEditor.commit();
    }
    public String getUserId() {

        return mPrefs.getString(USER_ID,"");
    }

    private static final String FIELD_REGISTRATION_ID = "regid";
    public void setRegistrationToken(String token) {
        mEditor.putString(FIELD_REGISTRATION_ID, token);
        mEditor.commit();
    }
    public String getRegistrationToken(){
        return mPrefs.getString(FIELD_REGISTRATION_ID, "");
    }
    private static final String IS_GCM="is_gcm";

    public void setIsGcm(boolean is_gcm){//String userId) {
        mEditor.putBoolean(IS_GCM, is_gcm);
        mEditor.commit();
    }
    public boolean getIsGcm(){
        return mPrefs.getBoolean(IS_GCM,true);
    }


    private static final String IS_LOGIN="is_login";
    public void setLogin(boolean login) {
        mEditor.putBoolean(IS_LOGIN,login);
        mEditor.commit();
    }
    public boolean isLogin() {
        return mPrefs.getBoolean(IS_LOGIN, false);
    }
}
