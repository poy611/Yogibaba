package kr.co.yogibaba.www.yogibaba.shoppingtab.shoppingdetail;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

import kr.co.yogibaba.www.yogibaba.R;
import kr.co.yogibaba.www.yogibaba.data.etc.Click;
import kr.co.yogibaba.www.yogibaba.data.review.ReviewResults;
import kr.co.yogibaba.www.yogibaba.data.review.Reviews;
import kr.co.yogibaba.www.yogibaba.manager.NetworkManagerOh;
import kr.co.yogibaba.www.yogibaba.manager.PropertyManager;
import kr.co.yogibaba.www.yogibaba.manager.TypefaceManager;
import kr.co.yogibaba.www.yogibaba.shoppingtab.shoppingdetail.review.ReviewAdapter;
import kr.co.yogibaba.www.yogibaba.shoppingtab.shoppingdetail.review.ReviewDivider;
import kr.co.yogibaba.www.yogibaba.shoppingtab.shoppingdetail.review.ReviewWriteActivity;
import okhttp3.Request;

public class ShoppingDetailReviewActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    ReviewAdapter mAdapter;
    Button btn;
    int item_id;
    AppCompatDelegate mDelegate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //글꼴테스트
        if (Build.VERSION.SDK_INT < 11){
            getLayoutInflater().setFactory(this);
        } else {
            getLayoutInflater().setFactory2(this);
        }
        mDelegate = getDelegate();
        //글꼴테스트
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_detail_qn);
        item_id=getIntent().getIntExtra(ShoppingDetailActivity.ITEM_ID, 1);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        btn=(Button)findViewById(R.id.btn_review_write);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(PropertyManager.getInstance().isLogin()) {
                    Intent intent = new Intent(ShoppingDetailReviewActivity.this, ReviewWriteActivity.class);
                    intent.putExtra(ShoppingDetailActivity.ITEM_ID, item_id);
                    startActivity(intent);
                }
                else{
                    Toast.makeText(ShoppingDetailReviewActivity.this,"로그인이 필요합니다",Toast.LENGTH_SHORT).show();
                }
            }
        });
        recyclerView=(RecyclerView)findViewById(R.id.rv_review);
        mAdapter=new ReviewAdapter();
        mAdapter.setOnButtonClickListener(new ReviewAdapter.OnButtonClickListener() {
            @Override
            public void onButtonClick(View v, Reviews product) {
                NetworkManagerOh.getInstance().deleteReivew(ShoppingDetailReviewActivity.this
                        , product.getUser_id()
                        , product.getReview_id()
                        , new NetworkManagerOh.OnResultListener<Click>() {
                            @Override
                            public void onSuccess(Request request, Click result) {

                                Toast.makeText(ShoppingDetailReviewActivity.this
                                        ,result.result.getMessage(),Toast.LENGTH_SHORT).show();
                                onResume();
                            }

                            @Override
                            public void onFail(Request request, IOException exception) {

                                Toast.makeText(ShoppingDetailReviewActivity.this
                                        ,"네트워크 에러",Toast.LENGTH_SHORT).show();
                            }
                        });

            }
        });

        recyclerView.setAdapter(mAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new ReviewDivider(ShoppingDetailReviewActivity.this));

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int type=item.getItemId();
        if(type==android.R.id.home)
        {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setData();

    }

    void setData(){
        NetworkManagerOh.getInstance().getReviewList(ShoppingDetailReviewActivity.this
                , item_id
                , new NetworkManagerOh.OnResultListener<ReviewResults>() {
                    @Override
                    public void onSuccess(Request request, ReviewResults result) {
                        if(result.getSuccess()==1)
                        {
                            Toast.makeText(ShoppingDetailReviewActivity.this,
                                    result.result.getMessage()
                                    ,Toast.LENGTH_SHORT).show();
                            mAdapter.clear();
                            mAdapter.addAll(result.result.getReviews());
                        }
                        else{
                            Toast.makeText(ShoppingDetailReviewActivity.this,
                                    result.result.getMessage()
                                    ,Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFail(Request request, IOException exception) {
                        Toast.makeText(ShoppingDetailReviewActivity.this,"네트워크에러"
                                ,Toast.LENGTH_SHORT).show();

                    }
                });
    }
    public View setCustomFont(View view, String name, Context context, AttributeSet attrs){
        if(view == null){
            if(name.equals("TextView")){
                view = new TextView(context,attrs);
            }
        }
        if (view != null && view instanceof TextView){
            TypedArray typedArray = context.obtainStyledAttributes(attrs, new int[]{android.R.attr.fontFamily});
            String fontFamily = typedArray.getString(0);
            typedArray.recycle();
            Typeface typeface = TypefaceManager.getInstance().getTypeface(context,fontFamily);
            if (typeface != null){
                TextView textView = (TextView)view;
                textView.setTypeface(typeface);
            }
        }
        return view;
    }

    @Override
    public View onCreateView(String name, Context context, AttributeSet attrs) {
        View view = super.onCreateView(name, context, attrs);
        if(Build.VERSION.SDK_INT < 11){
            if (view == null){
                view = mDelegate.createView(null, name, context, attrs);
                view = setCustomFont(view, name, context, attrs);
            }
        }
        return view;
    }

    @Override
    public View onCreateView(View parent, String name, Context context, AttributeSet attrs) {
        View view  = super.onCreateView(parent, name, context, attrs);
        if (view == null){
            view = mDelegate.createView(parent, name, context, attrs);
        }
        view = setCustomFont(view, name, context, attrs);

        return  view;
    }
}