package kr.co.yogibaba.www.yogibaba.settingTab.basket;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import kr.co.yogibaba.www.yogibaba.R;

import kr.co.yogibaba.www.yogibaba.data.basket.Basket;

/**
 * Created by jah on 2016-05-19.
 */
public class BasketAdapter extends RecyclerView.Adapter<BasketViewHolder> {

    List<Basket> items = new ArrayList<>();

    public void clear(){
        items.clear();
        notifyDataSetChanged();
    }

    public void add(Basket data){
        items.add(data);
        notifyDataSetChanged();
    }

    public void addall(List<Basket> items){
        this.items.addAll(items);
        notifyDataSetChanged();
    }
    public void remove(int position){
        items.remove(position);
        notifyDataSetChanged();
    }

    @Override
    public BasketViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_basket_list,null);
        return new BasketViewHolder(view);
    }

    @Override
    public void onBindViewHolder(BasketViewHolder holder, final int position) {
        holder.setBasketData(items.get(position));
        holder.btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener!=null){
                    mListener.OnDeleteButtonClick(v,items.get(position));
                    remove(position);
                }
            }
        });
        holder.setOnChangeButtonClickListener(bListener);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }



    interface OnDeleteButtonClickListener {
        void OnDeleteButtonClick(View v,Basket product);
    }
    OnDeleteButtonClickListener mListener;
    public void setOnDeleteButtonClickListener(OnDeleteButtonClickListener listener){
        mListener=listener;
    }

    BasketViewHolder.OnChangeButtonClickListener bListener;
    public void setOnChangeButtonClickListener(BasketViewHolder.OnChangeButtonClickListener listener){
        bListener=listener;
    }

}
