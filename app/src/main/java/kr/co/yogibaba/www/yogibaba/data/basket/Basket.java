package kr.co.yogibaba.www.yogibaba.data.basket;

import org.w3c.dom.ls.LSException;

import java.util.List;

/**
 * Created by OhDaeKyoung on 2016. 5. 27..
 */
public class Basket {
    int item_id;
    String item_name;
    int item_price;
    int option_price;
    String item_image_url;
    String option_name;
    int item_count;
    List<BasketItem> item_option;

    public List<BasketItem> getItem_option() {
        return item_option;
    }

    public int getItem_id() {
        return item_id;
    }

    public String getItem_name() {
        return item_name;
    }

    public int getItem_price() {
        return item_price;
    }

    public int getOption_price() {
        return option_price;
    }

    public String getItem_image_url() {
        return item_image_url;
    }

    public String getOption_name() {
        return option_name;
    }

    public int getItem_count() {
        return item_count;
    }
}
