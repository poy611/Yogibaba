package kr.co.yogibaba.www.yogibaba.settingTab.orderAndDelivery;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import kr.co.yogibaba.www.yogibaba.R;
import kr.co.yogibaba.www.yogibaba.data.order.OrderDetailPurchases;

/**
 * Created by jah on 2016-05-26.
 */
public class OrderDetailDateAdapter extends RecyclerView.Adapter<OrderDetailDateViewHolder> {

    OrderDetailPurchases pData;

    public void setPurchasesData(OrderDetailPurchases pData) {
        this.pData = pData;
    }

    @Override
    public OrderDetailDateViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_order_detail_header_entry,null);
        return new OrderDetailDateViewHolder(view);
    }

    @Override
    public void onBindViewHolder(OrderDetailDateViewHolder holder, int position) {
        holder.setOrderDetailDate(pData);
    }

    @Override
    public int getItemCount() {
        return 0;
    }
}
