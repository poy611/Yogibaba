package kr.co.yogibaba.www.yogibaba.settingTab.orderAndDelivery;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import kr.co.yogibaba.www.yogibaba.R;
import kr.co.yogibaba.www.yogibaba.data.order.OrderDetailPurchases;

/**
 * Created by jah on 2016-05-26.
 */
public class OrderDetailDateViewHolder extends RecyclerView.ViewHolder {
    TextView order_detail_date, order_detail_number;

    OrderDetailPurchases iData;

    public OrderDetailDateViewHolder(View itemView) {
        super(itemView);

        order_detail_date = (TextView)itemView.findViewById(R.id.text_order_detail_date);
        order_detail_number = (TextView)itemView.findViewById(R.id.text_order_detail_number);

    }

    public void setOrderDetailDate(OrderDetailPurchases iData){
        this.iData = iData;
        order_detail_date.setText(iData.getPurchase_date());
        order_detail_number.setText(iData.getPurchase_number());
    }
}
