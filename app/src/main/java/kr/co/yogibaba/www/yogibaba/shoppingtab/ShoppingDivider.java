package kr.co.yogibaba.www.yogibaba.shoppingtab;

import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import kr.co.yogibaba.www.yogibaba.R;

/**
 * Created by OhDaeKyoung on 2016. 5. 31..
 */
public class ShoppingDivider extends RecyclerView.ItemDecoration {
    private  int top;
    private  int bottom;
    private  int left;
    private  int right;
    final Resources resources;
    public static final int VIEW_TYPE_VIEWPAGE = 1;
    public static final int VIEW_TYPE_RECYCLER = 2;

    public ShoppingDivider(Activity activity) {
        resources = activity.getResources();
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        // super.getItemOffsets(outRect, view, parent, state);

        switch (parent.getAdapter().getItemViewType(parent.getChildAdapterPosition(view)))
        {
            case VIEW_TYPE_VIEWPAGE:
                outRect.top=0;
                outRect.bottom=0;
                outRect.left=0;
                outRect.right=0;
                return ;
            case VIEW_TYPE_RECYCLER:
                outRect.top=(int)resources.getDimension(R.dimen.shoppinglistmargin);
                outRect.bottom=0;
                outRect.left=0;
                outRect.right=0;
                return ;

        }



    }
}
