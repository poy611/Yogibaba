package kr.co.yogibaba.www.yogibaba.shoppingtab.furniture;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import kr.co.yogibaba.www.yogibaba.R;
import kr.co.yogibaba.www.yogibaba.data.ShoppingDataByCategory;
import kr.co.yogibaba.www.yogibaba.facebook.FacebookActivity;
import kr.co.yogibaba.www.yogibaba.manager.PropertyManager;

/**
 * Created by Tacademy on 2016-05-12.
 */
public class FurnitureViewHolder extends RecyclerView.ViewHolder {
    ImageView imageView;
    TextView productTitle,productPrice,productManufatoruer,likesNum,reviewNum;
    ImageButton btn_pub,btn_close_publ,btn_clip,btn_kakao_publ,btn_facebook_publ;
    ShoppingDataByCategory mdata;
    ImageView imgbtn_hart;
    Boolean is_like=false;
    FacebookActivity activity;
    public FurnitureViewHolder(View itemView) {
        super(itemView);
        imageView=(ImageView)itemView.findViewById(R.id.img_shopping_item_by_category);
        productTitle=(TextView)itemView.findViewById(R.id.text_shopping_item_by_category_title);
        productPrice=(TextView)itemView.findViewById(R.id.text_shopping_item_by_category_price);
        likesNum=(TextView)itemView.findViewById(R.id.text_hart_count);
        reviewNum=(TextView)itemView.findViewById(R.id.text_review_count);
        imgbtn_hart=(ImageView)itemView.findViewById(R.id.imgbtn_hart);
        imgbtn_hart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(is_like){
                    imgbtn_hart.setImageResource(R.drawable.likebtn_nomal);
                    is_like=false;
                }
                else {
                    imgbtn_hart.setImageResource(R.drawable.likebtn_press);
                    is_like=true;
                }
            }
        });


        final Context context=itemView.getContext();
        btn_pub=(ImageButton)itemView.findViewById(R.id.btn_pub);

        btn_close_publ=(ImageButton)itemView.findViewById(R.id.btn_close_publ);
        btn_clip=(ImageButton)itemView.findViewById(R.id.btn_clip);
        btn_kakao_publ=(ImageButton)itemView.findViewById(R.id.btn_kakao_publ);
        btn_facebook_publ=(ImageButton)itemView.findViewById(R.id.btn_facebook_publ);

        btn_pub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Animation slideUp = AnimationUtils.loadAnimation(context, R.anim.slide_up);
                Animation slideDown = AnimationUtils.loadAnimation(context, R.anim.slide_down);

                btn_pub.startAnimation(slideDown);
                btn_pub.setVisibility(View.GONE);


                btn_close_publ.startAnimation(slideUp);
                btn_close_publ.setVisibility(View.VISIBLE);

                btn_clip.startAnimation(slideUp);
                btn_clip.setVisibility(View.VISIBLE);

                btn_kakao_publ.startAnimation(slideUp);
                btn_kakao_publ.setVisibility(View.VISIBLE);

                btn_facebook_publ.startAnimation(slideUp);
                btn_facebook_publ.setVisibility(View.VISIBLE);
            }
        });
        btn_close_publ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Animation slideUp = AnimationUtils.loadAnimation(context, R.anim.slide_up);
                Animation slideDown = AnimationUtils.loadAnimation(context, R.anim.slide_down);



                btn_close_publ.startAnimation(slideDown);
                btn_close_publ.setVisibility(View.GONE);

                btn_clip.startAnimation(slideDown);
                btn_clip.setVisibility(View.GONE);

                btn_kakao_publ.startAnimation(slideDown);
                btn_kakao_publ.setVisibility(View.GONE);

                btn_facebook_publ.startAnimation(slideDown);
                btn_facebook_publ.setVisibility(View.GONE);
                btn_pub.startAnimation(slideUp);
                btn_pub.setVisibility(View.VISIBLE);

            }
        });
        btn_facebook_publ.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (PropertyManager.getInstance().isLogin()){
                    Toast.makeText(context,"페이스북으로 요기바바 공유하기",Toast.LENGTH_SHORT).show();
                    activity = new FacebookActivity();
                    activity.post();
                }
                else
                {
                    Toast.makeText(context,"페이스북 로그인이 필요합니다",Toast.LENGTH_LONG).show();
                }
            }
        });

        btn_clip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context,"추가 공유 서비스 준비중",Toast.LENGTH_LONG).show();
            }
        });

        btn_kakao_publ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context,"카카오톡 서비스 준비중",Toast.LENGTH_LONG).show();
            }
        });

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onItemClick(v,mdata);
            }
        });
    }
    public void setShoppingDataByCategory(ShoppingDataByCategory data)
    {
        this.mdata=data;

        Glide.with(imageView.getContext()).load(mdata.getItem_image_url()).into(imageView);
        productTitle.setText(data.getItem_name());
        productPrice.setText("가격: "+data.getItem_price()+" won");
        likesNum.setText(""+data.getItem_like_num());
        reviewNum.setText(""+data.getReview_num());
       // productManufatoruer.setText(data.getItem_brand());
    }
    public interface OnItemClickListener {
        public void onItemClick(View view, ShoppingDataByCategory product);
    }

    OnItemClickListener mListener;

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }
}
