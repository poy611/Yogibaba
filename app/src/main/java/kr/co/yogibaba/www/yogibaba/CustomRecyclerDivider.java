package kr.co.yogibaba.www.yogibaba;

import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by OhDaeKyoung on 2016. 5. 30..
 */
public class CustomRecyclerDivider extends RecyclerView.ItemDecoration {
    private  int top;
    private  int bottom;
    private  int left;
    private  int right;


    public CustomRecyclerDivider(Activity activity) {
        final Resources resources = activity.getResources();
        top=(int)resources.getDimension(R.dimen.recycler_margin_top);
        bottom=(int)resources.getDimension(R.dimen.recycler_margin_bottom);
        left=(int)resources.getDimension(R.dimen.recycler_margin_left);
        right=(int)resources.getDimension(R.dimen.recycler_margin_right);
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
       // super.getItemOffsets(outRect, view, parent, state);
        if (parent.getChildAdapterPosition(view) != parent.getAdapter().getItemCount() - 1) {
            outRect.top=top;
            outRect.bottom=bottom;
            outRect.left=left;
            outRect.right=right;

        }


    }
}
