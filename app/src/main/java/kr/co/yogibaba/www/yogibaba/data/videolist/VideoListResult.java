package kr.co.yogibaba.www.yogibaba.data.videolist;

import java.util.List;

/**
 * Created by OhDaeKyoung on 2016. 5. 23..
 */
public class VideoListResult {
    String message;
    public List<VideoListData> movies;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<VideoListData> getMovies() {
        return movies;
    }

    public void setMovies(List<VideoListData> movies) {
        this.movies = movies;
    }
}
