package kr.co.yogibaba.www.yogibaba.data.videolist;

/**
 * Created by OhDaeKyoung on 2016. 5. 23..
 */
public class VideoListResults {
    public int success;
    public VideoListResult result;

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }
}
