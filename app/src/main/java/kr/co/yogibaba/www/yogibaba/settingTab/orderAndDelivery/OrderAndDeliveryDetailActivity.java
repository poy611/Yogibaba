package kr.co.yogibaba.www.yogibaba.settingTab.orderAndDelivery;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import java.io.IOException;

import kr.co.yogibaba.www.yogibaba.R;
import kr.co.yogibaba.www.yogibaba.data.order.OrderDetailPurchases;
import kr.co.yogibaba.www.yogibaba.manager.NetworkManagerJH;
import okhttp3.Request;

public class OrderAndDeliveryDetailActivity extends AppCompatActivity {

    RecyclerView listview;
    OrderDetailListAdapter mAdapter;
    Intent intent;
    public static String purchase_id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAdapter = new OrderDetailListAdapter();
        setContentView(R.layout.activity_order_and_delivery_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        intent = getIntent();

        listview = (RecyclerView)findViewById(R.id.rv_order_detail_list);
        purchase_id = String.valueOf(intent.getExtras().getInt("purchase_id"));
        listview.setAdapter(mAdapter);
        listview.setLayoutManager(new LinearLayoutManager(this));
        setData(purchase_id);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void setData(String purchase_id) {
        NetworkManagerJH.getInstance().getOrderDetailPurchases(this, purchase_id, new NetworkManagerJH.OnResultListener<OrderDetailPurchases>() {
            @Override
            public void onSuccess(Request request, OrderDetailPurchases result) {
                mAdapter.setPurchasesData(result);
                Toast.makeText(getBaseContext(), "전송성공 : ", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFail(Request request, IOException exception) {
                Toast.makeText(getBaseContext(), "exception : " + exception.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

    }

}
