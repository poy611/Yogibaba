package kr.co.yogibaba.www.yogibaba.settingTab.likes;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;

import kr.co.yogibaba.www.yogibaba.R;
import kr.co.yogibaba.www.yogibaba.data.etc.Click;
import kr.co.yogibaba.www.yogibaba.data.like.LikesShoppingList;
import kr.co.yogibaba.www.yogibaba.manager.NetworkManagerJH;
import kr.co.yogibaba.www.yogibaba.manager.PropertyManager;
import kr.co.yogibaba.www.yogibaba.search.shoppingsearch.ResultDivier;
import kr.co.yogibaba.www.yogibaba.shoppingtab.shoppingdetail.ShoppingDetailActivity;
import okhttp3.Request;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link LikesShoppingListFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link LikesShoppingListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LikesShoppingListFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    String USER_ID;
    int is_show=0;
    public LikesShoppingListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment LikesShoppingListFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static LikesShoppingListFragment newInstance(String param1, String param2) {
        LikesShoppingListFragment fragment = new LikesShoppingListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    LikesShoppingListAdapter mAdapter;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        mAdapter = new LikesShoppingListAdapter();
        mAdapter.setOnItemClicListener(new LikesShoppingListViewHolder.OnItemClickListener(){
            @Override
            public void onItemClick(View view, LikesShoppingList product) {
                Intent intent=new Intent(getContext(), ShoppingDetailActivity.class);
                intent.putExtra(ShoppingDetailActivity.ITEM_ID,product.getItem_id());
                startActivity(intent);


            }
        });
        USER_ID= PropertyManager.getInstance().getUserId();
        mAdapter.setOnButtonClickListener(new LikesShoppingListViewHolder.OnButtonClickListener() {

            @Override
            public void onItemClick(View view, LikesShoppingList product) {
                deleteData(product);

            }
        });
    }
    private void deleteData(LikesShoppingList product) {
        NetworkManagerJH.getInstance().setDeleteLikesShopping(this, USER_ID, String.valueOf(product.getItem_id()), new NetworkManagerJH.OnResultListener<Click>() {
            @Override
            public void onSuccess(Request request, Click result) {

              //  editButton.setVisibility(View.VISIBLE);
              //  completeButton.setVisibility(View.GONE);
                is_show=1;
               onResume();
            }

            @Override
            public void onFail(Request request, IOException exception) {
                Toast.makeText(getContext(), "exception : " + exception.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
    @Override
    public void onResume() {
        super.onResume();
        mAdapter.clear();
        setData(is_show);
    }



    private void setData(final int edit_type) {
        NetworkManagerJH.getInstance().getLikesShoopingList(this, USER_ID, new NetworkManagerJH.OnResultListener<List<LikesShoppingList>>() {
            @Override
            public void onSuccess(Request request, List<LikesShoppingList> result) {
                mAdapter.clear();
                if (result != null) {
                    for (int i = 0; i < result.size(); i++) {
                        result.get(i).setEdit_Type(edit_type);
                    }
                    mAdapter.addAll(result);
                    text_likes_total_count.setText("리스트 " + mAdapter.getItemCount() + "개");
                }
                else {
                    text_likes_total_count.setText("리스트가 비어있습니다");
                }
            }
            @Override
            public void onFail(Request request, IOException exception) {
                Toast.makeText(getContext(), "exception : " + exception.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

    }
    RecyclerView recyclerView;
    GridLayoutManager gridLayoutManager;
    TextView text_likes_total_count;
    ImageView editButton;
    ImageView completeButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view=inflater.inflate(R.layout.fragment_likes_shopping_list, container, false);
        text_likes_total_count=(TextView)view.findViewById(R.id.text_likes_total_count);

        recyclerView=(RecyclerView)view.findViewById(R.id.rv_likes_shopping);
        recyclerView.setAdapter(mAdapter);
        gridLayoutManager=new GridLayoutManager(getContext(),2);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.addItemDecoration(new ResultDivier(getActivity()));
        editButton = (ImageView)view.findViewById(R.id.likes_edit_Button);
        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editButton.setVisibility(View.GONE);
                completeButton.setVisibility(View.VISIBLE);
                mAdapter.clear();
                is_show=1;
                onResume();
            }
        });
        completeButton = (ImageView)view.findViewById(R.id.likes_complete_button);
        completeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editButton.setVisibility(View.VISIBLE);
                completeButton.setVisibility(View.GONE);
                mAdapter.clear();
                is_show=0;
                onResume();
            }
        });

        return  view;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}