package kr.co.yogibaba.www.yogibaba.data;

/**
 * Created by OhDaeKyoung on 2016. 5. 16..
 */
public class SearchRecomData {
    String imageUrl;
    String recomTitle;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getRecomTitle() {
        return recomTitle;
    }

    public void setRecomTitle(String recomTitle) {
        this.recomTitle = recomTitle;
    }
}
