package kr.co.yogibaba.www.yogibaba.videoTab.videodetail;

import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import kr.co.yogibaba.www.yogibaba.R;

/**
 * Created by OhDaeKyoung on 2016. 5. 30..
 */
public class CustomMovieDetailDivider extends RecyclerView.ItemDecoration {
    private  int top;
    private  int bottom;
    private  int left;
    private  int right;
    final Resources resources;
    public final static int HEADER_OF_IN_MOVIE_TYPE=0;
    public final static int BODY_OF_ON_MOVIE_TYPE=1;
    public final static int HEADER_OF_RELATED_MOVIE=2;
    public final static int BODY_OF_RELATED_MOVIE=3;
    int size;
    public CustomMovieDetailDivider(Activity activity,int size) {
        resources = activity.getResources();
        this.size=size;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        // super.getItemOffsets(outRect, view, parent, state);
        int position=parent.getChildAdapterPosition(view);
        switch (parent.getAdapter().getItemViewType(position))
        {
            case HEADER_OF_IN_MOVIE_TYPE:
                outRect.top=(int)resources.getDimension(R.dimen.recycler_video_detail_header_movie_top);
                outRect.bottom=(int)resources.getDimension(R.dimen.recycler_video_detail_margin_bottom);
                outRect.left=(int)resources.getDimension(R.dimen.recycler_video_detail_margin_left);
                outRect.right=(int)resources.getDimension(R.dimen.recycler_video_detail_margin_right);
                return ;
            case BODY_OF_ON_MOVIE_TYPE:
                outRect.top=(int)resources.getDimension(R.dimen.recycler_video_detail_header_in_top);
                outRect.bottom=(int)resources.getDimension(R.dimen.recycler_video_detail_margin_bottom);
                outRect.left=(int)resources.getDimension(R.dimen.recycler_video_detail_margin_left);
                outRect.right=(int)resources.getDimension(R.dimen.recycler_video_detail_margin_right);
                return ;
            case HEADER_OF_RELATED_MOVIE:
                outRect.top=(int)resources.getDimension(R.dimen.recycler_video_detail_header_relate_top);
                outRect.bottom=(int)resources.getDimension(R.dimen.recycler_video_detail_header_relate_bottom);
                outRect.left=(int)resources.getDimension(R.dimen.recycler_video_detail_margin_left);
                outRect.right=(int)resources.getDimension(R.dimen.recycler_video_detail_margin_right);
                return ;
            /*case BODY_OF_RELATED_MOVIE:

                if(position-(size+2)==0)
                {
                    outRect.top=(int)resources.getDimension(R.dimen.recycler_video_detail_header_relate_top);
                    outRect.bottom=(int)resources.getDimension(R.dimen.recycler_video_detail_margin_bottom);
                    outRect.left=(int)resources.getDimension(R.dimen.littleleft);
                    outRect.right=(int)resources.getDimension(R.dimen.right);

                }
                else if((position-(size+2))%2==0){
                    outRect.top=(int)resources.getDimension(R.dimen.recycler_video_detail_header_relate_top);
                    outRect.bottom=(int)resources.getDimension(R.dimen.recycler_video_detail_margin_bottom);
                    outRect.left=(int)resources.getDimension(R.dimen.littleleft);
                    outRect.right=(int)resources.getDimension(R.dimen.right);

                }
                else{
                    outRect.top=(int)resources.getDimension(R.dimen.recycler_video_detail_header_relate_top);
                    outRect.bottom=(int)resources.getDimension(R.dimen.recycler_video_detail_margin_bottom);
                    outRect.left=(int)resources.getDimension(R.dimen.left);
                    outRect.right=(int)resources.getDimension(R.dimen.littleright);
                }*/
               /* outRect.top=(int)resources.getDimension(R.dimen.recycler_video_detail_margin_top);
                outRect.bottom=(int)resources.getDimension(R.dimen.recycler_video_detail_margin_bottom);
                outRect.left=(int)resources.getDimension(R.dimen.recycler_video_detail_margin_left);
                outRect.right=(int)resources.getDimension(R.dimen.recycler_video_detail_margin_right);*/
               // return ;
        }



    }
}
