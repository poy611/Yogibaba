package kr.co.yogibaba.www.yogibaba.shoppingtab;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;

import kr.co.yogibaba.www.yogibaba.R;
import kr.co.yogibaba.www.yogibaba.data.ShoppingDataByCategory;
import kr.co.yogibaba.www.yogibaba.data.ShoppingViewPageData;
import kr.co.yogibaba.www.yogibaba.manager.NetworkManagerJH;
import kr.co.yogibaba.www.yogibaba.shoppingtab.newproducts.NewProductAdapter;
import kr.co.yogibaba.www.yogibaba.shoppingtab.newproducts.NewProductPagerViewHolder;
import kr.co.yogibaba.www.yogibaba.shoppingtab.newproducts.NewProductViewHolder;
import kr.co.yogibaba.www.yogibaba.shoppingtab.shoppingdetail.ShoppingDetailActivity;
import okhttp3.Request;

public class NewFragmentFragment extends Fragment {

    public static final String TYPE_NEW="0";
    public NewFragmentFragment() {
        // Required empty public constructor
    }

    NewProductAdapter mAdapter;
    RecyclerView recyclerView;
    GridLayoutManager gridLayoutManager;


    // TODO: Rename method, update argument and hook method into UI event


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAdapter=new NewProductAdapter();
        mAdapter.setOnItemClicListener(new NewProductViewHolder.OnItemClickListener(){
            @Override
            public void onItemClick(View view, ShoppingDataByCategory product) {
                Intent intent=new Intent(getContext(), ShoppingDetailActivity.class);
                intent.putExtra(ShoppingDetailActivity.ITEM_ID,product.getItem_id());
                startActivity(intent);
            }
        });
        mAdapter.setOnItemClickListener(new ViewpagerShopping.OnViewPagerClickListener() {
            @Override
            public void onViewPagerClick(View view, ShoppingDataByCategory product) {
                Intent intent=new Intent(getContext(),ShoppingDetailActivity.class);
                intent.putExtra(ShoppingDetailActivity.ITEM_ID,product.getItem_id());
                startActivity(intent);
            }
        });
        //setData();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view=inflater.inflate(R.layout.fragment_new, container, false);
        recyclerView=(RecyclerView)view.findViewById(R.id.rv_shopping_new);
        recyclerView.setAdapter(mAdapter);
        gridLayoutManager=new GridLayoutManager(getContext(),1);


        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.addItemDecoration(new ShoppingDivider(getActivity()));
        return  view;
    }

    @Override
    public void onResume() {
        super.onResume();
        setData();
    }

    private void setData(){
        NetworkManagerJH.getInstance().getYogibabaShoppingList(getContext()
                , TYPE_NEW
                , new NetworkManagerJH.OnResultListener<List<ShoppingDataByCategory>>() {
            @Override
            public void onSuccess(Request request, List<ShoppingDataByCategory> result) {
                mAdapter.clear();
                mAdapter.addAll(result);
            }

            @Override
            public void onFail(Request request, IOException exception) {
                Toast.makeText(getContext(),"네트우커ㅡ에러",Toast.LENGTH_SHORT).show();
            }
        });

    }
}
