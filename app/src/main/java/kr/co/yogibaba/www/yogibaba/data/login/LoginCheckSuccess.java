package kr.co.yogibaba.www.yogibaba.data.login;

/**
 * Created by jah on 2016-06-07.
 */
public class LoginCheckSuccess {
    public int success;
    public LoginCheckResult result;

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public LoginCheckResult getResult() {
        return result;
    }

    public void setResult(LoginCheckResult result) {
        this.result = result;
    }
}
