package kr.co.yogibaba.www.yogibaba.search.shoppingsearch;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import kr.co.yogibaba.www.yogibaba.R;
import kr.co.yogibaba.www.yogibaba.data.SearchRecomData;
import kr.co.yogibaba.www.yogibaba.data.search.SearchItemRecom;

/**
 * Created by OhDaeKyoung on 2016. 5. 16..
 */
public class ShoppingSearchRecomAdapter extends RecyclerView.Adapter<ShoppingSearchRecomViewHolder> {
    List<SearchItemRecom> items=new ArrayList<>();
    public void add(SearchItemRecom item)
    {
        items.add(item);
        notifyDataSetChanged();
    }
    public void addAll(List<SearchItemRecom> items)
    {
        this.items.addAll(items);
        notifyDataSetChanged();
    }
    public void clear(){
        items.clear();
        notifyDataSetChanged();
    }

    @Override
    public ShoppingSearchRecomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.view_recomand_search,null);
        return new ShoppingSearchRecomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ShoppingSearchRecomViewHolder holder, int position) {
        holder.setSearchRecomData(items.get(position));
        holder.setOnItemClickListener(mListener);

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    ShoppingSearchRecomViewHolder.OnItemClickListener mListener;
    public void setOnItemClickListener(ShoppingSearchRecomViewHolder.OnItemClickListener listener){
        mListener=listener;
    }
}
