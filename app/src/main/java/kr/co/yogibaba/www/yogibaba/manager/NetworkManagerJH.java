package kr.co.yogibaba.www.yogibaba.manager;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.List;
import java.util.concurrent.TimeUnit;

import kr.co.yogibaba.www.yogibaba.MyApplication;
import kr.co.yogibaba.www.yogibaba.data.ShoppingDataByCategory;
import kr.co.yogibaba.www.yogibaba.data.ShoppingDataByCategorySuccess;
import kr.co.yogibaba.www.yogibaba.data.etc.Click;
import kr.co.yogibaba.www.yogibaba.data.facebook.FacebookIdResult;
import kr.co.yogibaba.www.yogibaba.data.like.LikesShoppingList;
import kr.co.yogibaba.www.yogibaba.data.like.LikesShoppingSuccess;
import kr.co.yogibaba.www.yogibaba.data.like.LikesVideoList;
import kr.co.yogibaba.www.yogibaba.data.like.LikesVideoSuccess;
import kr.co.yogibaba.www.yogibaba.data.login.LoginCheckSuccess;
import kr.co.yogibaba.www.yogibaba.data.order.OrderAndDeliveryData;
import kr.co.yogibaba.www.yogibaba.data.order.OrderAndDeliveryPurchases;
import kr.co.yogibaba.www.yogibaba.data.order.OrderAndDeliveryResult;
import kr.co.yogibaba.www.yogibaba.data.order.OrderAndDeliverySuccess;
import kr.co.yogibaba.www.yogibaba.data.order.OrderDetailPurchaseItem;
import kr.co.yogibaba.www.yogibaba.data.order.OrderDetailPurchases;
import kr.co.yogibaba.www.yogibaba.data.order.OrderDetailSuccess;
import okhttp3.Cache;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.JavaNetCookieJar;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by JH on 2016-05-09.
 */
public class NetworkManagerJH {
    private static NetworkManagerJH instance;

    public static NetworkManagerJH getInstance() {
        if (instance == null) {
            instance = new NetworkManagerJH();
        }
        return instance;
    }

    private static final int DEFAULT_CACHE_SIZE = 50 * 1024 * 1024;
    private static final String DEFAULT_CACHE_DIR = "miniapp";
    OkHttpClient mClient;
    private NetworkManagerJH() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        Context context = MyApplication.getContext();
        CookieManager cookieManager = new CookieManager(new PersistentCookieStore(context), CookiePolicy.ACCEPT_ALL);
        builder.cookieJar(new JavaNetCookieJar(cookieManager));

        File dir = new File(context.getExternalCacheDir(), DEFAULT_CACHE_DIR);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        builder.cache(new Cache(dir, DEFAULT_CACHE_SIZE));

        builder.connectTimeout(30, TimeUnit.SECONDS);
        builder.readTimeout(30, TimeUnit.SECONDS);
        builder.writeTimeout(30, TimeUnit.SECONDS);

        mClient = builder.build();
    }

    public interface OnResultListener<T> {
        public void onSuccess(Request request, T result);
        public void onFail(Request request, IOException exception);
    }

    private static final int MESSAGE_SUCCESS = 1;
    private static final int MESSAGE_FAIL = 0;

    class NetworkHandler extends Handler {
        public NetworkHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            NetworkResult result = (NetworkResult)msg.obj;
            switch (msg.what) {
                case MESSAGE_SUCCESS :
                    result.listener.onSuccess(result.request, result.result);
                    break;
                case MESSAGE_FAIL :
                    result.listener.onFail(result.request, result.excpetion);
                    break;
            }
        }
    }

    NetworkHandler mHandler = new NetworkHandler(Looper.getMainLooper());

    static class NetworkResult<T> {
        Request request;
        OnResultListener<T> listener;
        IOException excpetion;
        T result;
    }

    Gson gson = new Gson();



    public static final String TYPE_NEW_PRODUCT = "0";
    public static final String TYPE_FURNITURE = "1";
    public static final String TYPE_PROPS = "2";
    public static final String TYPE_ELECTRONICGOODS = "3";
    public static final String TYPE_LIGHTING = "4";

    private static final String YOGIBABA_SERVER = "http://52.79.169.4:3000";
    private static final String YOGIBABA_SHOPPING_LIST_URL = YOGIBABA_SERVER + "/home/shopping/%s";
    //
//    public  Request getYogibabaShoppingDetailList(Object tag, String type, OnResultListener<List<ShoppingDataByCategory>> listener) {
////        [쇼핑] 홈 -> 쇼핑페이지 홈 리스트 조회
////                /home/shopping/:type (GET)
//
//        //최초 슬라이드바에서 선택한 최신제품 / 가구 / 조명 / etc  클릭 시 이동화면
//        //연관 데이터 ShoppingDataByCategorySuccess -> ShoppingDataByCategoryResult -> ShoppingDataByCategory
//        String url = String.format(YOGIBABA_SHOPPING_LIST_URL,type);
//        Request request = new Request.Builder()
//                .url(url)
////                .header("Accept","application/json")
////                .header("appKey","458a10f5-c07e-34b5-b2bd-4a891e024c2a")
//                .build();
//        final NetworkResult<List<ShoppingDataByCategory>> result = new NetworkResult<>();
//        result.request = request;
//        result.listener = listener;
//        mClient.newCall(request).enqueue(new Callback() {
//            @Override
//            public void onFailure(Call call, IOException e) {
//                result.excpetion = e;
//                mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_FAIL, result));
//            }
//
//            @Override
//            public void onResponse(Call call, Response response) throws IOException {
//                if (response.isSuccessful()) {
//                    ShoppingDataByCategorySuccess data = gson.fromJson(response.body().charStream(), ShoppingDataByCategorySuccess.class);
//                    result.result = data.result.item;
//                    mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_SUCCESS, result));
//                }
//            }
//        });
//        return request;
//    }
    public  Request getYogibabaShoppingList(Object tag, String type, OnResultListener<List<ShoppingDataByCategory>> listener) {
//      [쇼핑] 쇼핑 상세 페이지 조회(로그인X)
//        /shopping/info/purchase_item/:item_id

        //상품 화면에서 상품상세화면으로 이동
        //연관 데이터 ShoppingDetailResult -> ShoppingDetailResultData -> ShoppingDetailResultOptionData / ShoppingDetailRelateData
        String url = String.format(YOGIBABA_SHOPPING_LIST_URL,type);
        Request request = new Request.Builder()
                .url(url)
//                .header("Accept","application/json")
//                .header("appKey","458a10f5-c07e-34b5-b2bd-4a891e024c2a")
                .build();
        final NetworkResult<List<ShoppingDataByCategory>> result = new NetworkResult<>();
        result.request = request;
        result.listener = listener;
        mClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                result.excpetion = e;
                mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_FAIL, result));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    ShoppingDataByCategorySuccess data = gson.fromJson(response.body().charStream(), ShoppingDataByCategorySuccess.class);
                    result.result = data.result.item;
                    mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_SUCCESS, result));
                }
            }
        });
        return request;
    }


    private static final String YOGIBABA_ORDER_AND_DELIVERY_LIST_URL = YOGIBABA_SERVER + "/home/order";

    public  Request getOrderAndDeliveryList(Object tag, String user_id, OnResultListener<List<OrderAndDeliveryPurchases>> listener) {

        RequestBody fromBody = new FormBody.Builder()
                .add("user_id",user_id)
                .build();

        Request request = new Request.Builder()
                .url(YOGIBABA_ORDER_AND_DELIVERY_LIST_URL)
                .header("Accept", "application/json")
                .post(fromBody)
                .build();
        final NetworkResult<List<OrderAndDeliveryPurchases>> result = new NetworkResult<>();
        result.request = request;
        result.listener = listener;
        mClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                result.excpetion = e;
                mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_FAIL, result));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    OrderAndDeliverySuccess data = gson.fromJson(response.body().charStream(), OrderAndDeliverySuccess.class);
                    result.result = data.result.purchases;                   ;
                    mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_SUCCESS, result));
                }
            }
        });
        return request;
    }

    public  Request getOrderAndDeliverySuccess(Object tag, String user_id, OnResultListener<OrderAndDeliverySuccess> listener) {
//      [주문&배송] 홈 -> 주문내역/배송조회 클릭 이벤트 처리
//       URL  /home/order
        //POST
        //user_id : 사용자 아이디
        //SIGN UP 기능이 미구현이므로 user_id는 가라로 만든다
        //연관 데이터 OrderAndDeliverySuccess -> OrderAndDeliveryPurchases -> OrderAndDeliveryData
        //POST방식으로 request 하기
        RequestBody fromBody = new FormBody.Builder()
                .add("user_id",user_id)
                .build();

//        RequestBody fromBody=new MultipartBody.Builder()
//                .setType(MultipartBody.FORM)
//                .addFormDataPart("user_id",user_id)
//                .build();
        Request request = new Request.Builder()
                .url(YOGIBABA_ORDER_AND_DELIVERY_LIST_URL)
                .header("Accept", "application/json")
                .post(fromBody)
                .build();
        final NetworkResult<OrderAndDeliverySuccess> result = new NetworkResult<>();
        result.request = request;
        result.listener = listener;
        mClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                result.excpetion = e;
                mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_FAIL, result));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    OrderAndDeliverySuccess data = gson.fromJson(response.body().charStream(), OrderAndDeliverySuccess.class);
                    result.result = data;
                    mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_SUCCESS, result));
                }
            }
        });
        return request;
    }
//
//    public  Request getOrderAndDelivery(Object tag, String user_id, OnResultListener<OrderAndDeliveryPurchases> listener) {
////      [주문&배송] 홈 -> 주문내역/배송조회 클릭 이벤트 처리
////       URL  /home/order
//        //POST
//        //user_id : 사용자 아이디
//        //SIGN UP 기능이 미구현이므로 user_id는 가라로 만든다
//        //연관 데이터 OrderAndDeliverySuccess -> OrderAndDeliveryPurchases -> OrderAndDeliveryData
//        //POST방식으로 request 하기
//        RequestBody fromBody=new MultipartBody.Builder()
//                .setType(MultipartBody.FORM)
//                .addFormDataPart("user_id",user_id)
//                .build();
//        Request request = new Request.Builder()
//                .url(YOGIBABA_ORDER_AND_DELIVERY_LIST_URL)
//                .header("Accept", "application/json")
//                .post(fromBody)
//                .build();
//        final NetworkResult<OrderAndDeliveryPurchases> result = new NetworkResult<>();
//        result.request = request;
//        result.listener = listener;
//        mClient.newCall(request).enqueue(new Callback() {
//            @Override
//            public void onFailure(Call call, IOException e) {
//                result.excpetion = e;
//                mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_FAIL, result));
//            }
//
//            @Override
//            public void onResponse(Call call, Response response) throws IOException {
//                if (response.isSuccessful()) {
//                    OrderAndDeliverySuccess data = gson.fromJson(response.body().charStream(), OrderAndDeliverySuccess.class);
//                    result.result = data.result;
//                    mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_SUCCESS, result));
//                }
//            }
//        });
//        return request;
//    }

    private static final String YOGIBABA_ORDER_DETAIL_URL = YOGIBABA_SERVER + "/order/detail";
    public  Request getOrderDetailPurchaseList(Object tag, String purchase_id, OnResultListener<List<OrderDetailPurchaseItem>> listener) {
        //[주문&배송] 주문내역/배송조회 -> 주문상세내역 조회 이벤트 처리
        ///order/detail/:purchase_id
        // 구매 목록 아이템의 정보를 얻어옴
        String url = String.format(YOGIBABA_ORDER_DETAIL_URL,purchase_id);
        Request request = new Request.Builder()
                .url(url)
//                .header("Accept","application/json")
//                .header("appKey","458a10f5-c07e-34b5-b2bd-4a891e024c2a")
                .build();
        final NetworkResult<List<OrderDetailPurchaseItem>> result = new NetworkResult<>();
        result.request = request;
        result.listener = listener;
        mClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                result.excpetion = e;
                mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_FAIL, result));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    OrderDetailSuccess data = gson.fromJson(response.body().charStream(), OrderDetailSuccess.class);
//                    result.result = data.result.getPurchases().getPurchase_item();
                    result.result = data.result.purchases.purchase_item;
                    mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_SUCCESS, result));
                }
            }
        });
        return request;
    }

    public  Request getOrderDetailPurchases(Object tag, String purchase_id, OnResultListener<OrderDetailPurchases> listener) {
        //[주문&배송] 주문내역/배송조회 -> 주문상세내역 조회 이벤트 처리
        ///order/detail/:purchase_id
        // 주문내역 정보를 얻어옴

        RequestBody fromBody = new FormBody.Builder()
                .add("purchase_id",purchase_id)
                .build();
        Request request = new Request.Builder()
                .url(YOGIBABA_ORDER_DETAIL_URL)
                .header("Accept", "application/json")
                .post(fromBody)
                .build();
        final NetworkResult<OrderDetailPurchases> result = new NetworkResult<>();
        result.request = request;
        result.listener = listener;
        mClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                result.excpetion = e;
                mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_FAIL, result));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    OrderDetailSuccess data = gson.fromJson(response.body().charStream(), OrderDetailSuccess.class);
                    result.result = data.result.purchases;
                    mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_SUCCESS, result));
                }
            }
        });
        return request;
    }

    private static final String YOGIBABA_LIKES_VIDEO_URL = YOGIBABA_SERVER + "/home/mypage/like/movie";
    public  Request getLikesVideoList(Object tag, String user_id, OnResultListener<List<LikesVideoList>> listener) {
//      [마이페이지] 홈 -> 마이페이지 좋아요 버튼 클릭 이벤트 처리(영상)
//        로그인 되어있을 때 마이페이지 조회        //POST
//        /home/mypage/like/movie
        RequestBody fromBody = new FormBody.Builder()
                .add("user_id",user_id)
                .build();
//        RequestBody fromBody=new MultipartBody.Builder()
//                .setType(MultipartBody.FORM)
//                .addFormDataPart("user_id",user_id)
//                .build();
        Request request = new Request.Builder()
                .url(YOGIBABA_LIKES_VIDEO_URL)
                .header("Accept", "application/json")
                .post(fromBody)
                .build();
        final NetworkResult<List<LikesVideoList>> result = new NetworkResult<>();
        result.request = request;
        result.listener = listener;
        mClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                result.excpetion = e;
                mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_FAIL, result));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    LikesVideoSuccess data = gson.fromJson(response.body().charStream(), LikesVideoSuccess.class);
                    result.result = data.result.user_movie_likes;                   ;
                    mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_SUCCESS, result));
                }
            }
        });
        return request;
    }

    private static final String YOGIBABA_LIKES_SHOPPING_URL = YOGIBABA_SERVER + "/home/mypage/like/shopping";
    public  Request getLikesShoopingList(Object tag, String user_id, OnResultListener<List<LikesShoppingList>> listener) {
//      [마이페이지] 홈 -> 마이페이지 좋아요 버튼 클릭 이벤트 처리(영상)
//        로그인 되어있을 때 마이페이지 조회        //POST
//        /home/mypage/like/movie
        RequestBody fromBody = new FormBody.Builder()
                .add("user_id",user_id)
                .build();

        Request request = new Request.Builder()
                .url(YOGIBABA_LIKES_SHOPPING_URL)
                .header("Accept", "application/json")
                .post(fromBody)
                .build();
        final NetworkResult<List<LikesShoppingList>> result = new NetworkResult<>();
        result.request = request;
        result.listener = listener;
        mClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                result.excpetion = e;
                mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_FAIL, result));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    LikesShoppingSuccess data = gson.fromJson(response.body().charStream(), LikesShoppingSuccess.class);
                    result.result = data.result.user_shopping_likes;                   ;
                    mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_SUCCESS, result));
                }
            }
        });
        return request;
    }
    private static final String YOGIBABA_DELETELIKES_VIDEO_URL = YOGIBABA_SERVER + "/home/mypage/like/movie/delete";
    public  Request setDeleteLikesVideo(Object tag, String user_id, String movie_id, OnResultListener<Click> listener) {
//     [주문&배송] 좋아요 삭제 (영상)
//       /home/mypage/like/movie/delete
        RequestBody fromBody = new FormBody.Builder()
                .add("user_id",user_id)
                .add("movie_id", movie_id)
                .build();

        Request request = new Request.Builder()
                .url(YOGIBABA_DELETELIKES_VIDEO_URL)
                .header("Accept", "application/json")
                .post(fromBody)
                .build();
        final NetworkResult<Click> result = new NetworkResult<>();
        result.request = request;
        result.listener = listener;
        mClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                result.excpetion = e;
                mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_FAIL, result));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    Click data = gson.fromJson(response.body().charStream(), Click.class);
                    result.result = data;                   ;
                    mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_SUCCESS, result));
                }
            }
        });
        return request;
    }
    private static final String YOGIBABA_DELETELIKES_SHOPPING_URL = YOGIBABA_SERVER + "/home/mypage/like/shopping/delete";
    public  Request setDeleteLikesShopping(Object tag, String user_id, String item_id, OnResultListener<Click> listener) {
//    [주문&배송] 좋아요 삭제 (아이탬)
//       /home/mypage/like/shopping/delete
//        user_id : 사용자 id
//        item_id : 아이탬 id
        RequestBody fromBody = new FormBody.Builder()
                .add("user_id",user_id)
                .add("item_id", item_id)
                .build();

        Request request = new Request.Builder()
                .url(YOGIBABA_DELETELIKES_SHOPPING_URL)
                .header("Accept", "application/json")
                .post(fromBody)
                .build();
        final NetworkResult<Click> result = new NetworkResult<>();
        result.request = request;
        result.listener = listener;
        mClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                result.excpetion = e;
                mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_FAIL, result));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    Click data = gson.fromJson(response.body().charStream(), Click.class);
                    result.result = data;                   ;
                    mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_SUCCESS, result));
                }
            }
        });
        return request;
    }

    private static final String YOGIBABA_LOGIN_URL = YOGIBABA_SERVER + "/user/login/check";
    public  Request setLoginCheck(Object tag, String facebook_token, OnResultListener<LoginCheckSuccess> listener) {
//   [Facebook] Login Check
//
//         success가 1이면 기존회원이 아니니까 새 회원 등록하는 Activity로 이동하고
//          0이면 기존회원이니까 user_id를 주어서 그 값을shared Reference에 집어넣으
        RequestBody fromBody = new FormBody.Builder()
                .add("facebook_token", facebook_token)
                .build();

        Request request = new Request.Builder()
                .url(YOGIBABA_LOGIN_URL)
                .header("Accept", "application/json")
                .post(fromBody)
                .build();
        final NetworkResult<LoginCheckSuccess> result = new NetworkResult<>();
        result.request = request;
        result.listener = listener;
        mClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                result.excpetion = e;
                mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_FAIL, result));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    LoginCheckSuccess data = gson.fromJson(response.body().charStream(), LoginCheckSuccess.class);
                    result.result = data;                   ;
                    mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_SUCCESS, result));
                }
            }
        });
        return request;
    }

    private static final String YOGIBABA_JOIN_URL = YOGIBABA_SERVER + "/user/login";
    public  Request setMembershipJoin(Object tag, String facebook_token, String email, String phone_number, String address, OnResultListener<LoginCheckSuccess> listener) {
//   [Facebook] Login Check
//
//         success가 1이면 기존회원이 아니니까 새 회원 등록하는 Activity로 이동하고
//          0이면 기존회원이니까 user_id를 주어서 그 값을shared Reference에 집어넣으
        RequestBody fromBody = new FormBody.Builder()
                .add("facebook_token", facebook_token)
                .add("email", email)
                .add("phone_number", phone_number)
                .add("address", address)
                .build();

        Request request = new Request.Builder()
                .url(YOGIBABA_JOIN_URL)
                .header("Accept", "application/json")
                .post(fromBody)
                .build();
        final NetworkResult<LoginCheckSuccess> result = new NetworkResult<>();
        result.request = request;
        result.listener = listener;
        mClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                result.excpetion = e;
                mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_FAIL, result));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    LoginCheckSuccess data = gson.fromJson(response.body().charStream(), LoginCheckSuccess.class);
                    result.result = data;                   ;
                    mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_SUCCESS, result));
                }
            }
        });
        return request;
    }

    private static final String FACEBOOK_SERVER = "https://graph.facebook.com";
    private static final String FACEBOOK_POST = FACEBOOK_SERVER + "/v2.6/me/feed?access_token=%s";

    public Request getFacebookPost(Object tag, String token,
                                   String message,
                                   String caption,
                                   String link,
                                   String picture,
                                   String name,
                                   String description,
                                   OnResultListener<String> listener) {
        String url = String.format(FACEBOOK_POST, token);

        RequestBody body = new FormBody.Builder()
                .add("message", message)
                .add("link", link)
                .add("caption", caption)
                .add("picture", picture)
                .add("name", name)
                .add("description", description)
                .build();

        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();

        final NetworkResult<String> result = new NetworkResult<>();
        result.request = request;
        result.listener = listener;
        mClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                result.excpetion = e;
                mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_FAIL, result));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    String text = response.body().string();
                    FacebookIdResult data = gson.fromJson(text, FacebookIdResult.class);
                    result.result = data.id;
                    mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_SUCCESS, result));
                } else {
                    result.excpetion = new IOException(response.message());
                    mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_FAIL, result));
                }
            }
        });
        return request;
    }

}