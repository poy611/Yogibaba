package kr.co.yogibaba.www.yogibaba.data.order;

/**
 * Created by jah on 2016-05-25.
 */
public class OrderDetailResult {
    //OrderDetailSuccess -> OrderDetailResult -> OrderDetailPurchases -> OrderDetailPurchaseItem
    public String message;
    public OrderDetailPurchases purchases;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public OrderDetailPurchases getPurchases() {
        return purchases;
    }

    public void setPurchases(OrderDetailPurchases purchases) {
        this.purchases = purchases;
    }
}
