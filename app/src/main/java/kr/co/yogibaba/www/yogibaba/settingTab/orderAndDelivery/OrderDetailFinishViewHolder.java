package kr.co.yogibaba.www.yogibaba.settingTab.orderAndDelivery;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import kr.co.yogibaba.www.yogibaba.R;
import kr.co.yogibaba.www.yogibaba.data.order.OrderDetailPurchases;

/**
 * Created by jah on 2016-05-25.
 */
public class OrderDetailFinishViewHolder extends RecyclerView.ViewHolder {

    public static final String CONDITON_0 = "배송준비";
    public static final String CONDITON_1 = "배송중";
    public static final String CONDITON_2 = "구매완료";
    public static final String CONDITON_3 = "환불완료";
    public static final int deliveryfee = 2500;

    TextView order_condition;
    TextView recipient_name,    recipient_phone_number, recipient_address, product_price, total_price, delivery_fee;

    OrderDetailPurchases purchasesData;

    public OrderDetailFinishViewHolder(View itemView) {
        super(itemView);
        order_condition = (TextView)itemView.findViewById(R.id.text_purchase_detail_condition);

        recipient_name = (TextView)itemView.findViewById(R.id.text_recipient_name);
        recipient_phone_number = (TextView)itemView.findViewById(R.id.text_phone_num);
        recipient_address = (TextView)itemView.findViewById(R.id.text_recipient_address);

        product_price = (TextView)itemView.findViewById(R.id.text_product_price);
        delivery_fee = (TextView)itemView.findViewById(R.id.text_delivery_fee) ;
        total_price = (TextView)itemView.findViewById(R.id.text_total_price);


    }

    public void setOrderConditionData(OrderDetailPurchases purchasesData){
        this.purchasesData = purchasesData;
        int i;
        int totalprice = 0;


        recipient_name.setText(purchasesData.getRecipient_name());
        recipient_address.setText(purchasesData.getRecipient_address());
        recipient_phone_number.setText(purchasesData.getRecipient_phone_number());

        for(i=0;i<purchasesData.getPurchase_item().size();i++){
            totalprice = totalprice
                    + purchasesData.getPurchase_item().get(i).getOption_price()
                    + purchasesData.getPurchase_item().get(i).getItem_price();
        }
        product_price.setText(String.valueOf(totalprice));
        delivery_fee.setText(String.valueOf(deliveryfee));
        total_price.setText(String.valueOf(totalprice+ deliveryfee));

        switch (purchasesData.getOrder_category()){
            case 0 :
                order_condition.setText(CONDITON_0);
                break;
            case 1:
                order_condition.setText(CONDITON_1);
                break;
            case 2:
                order_condition.setText(CONDITON_2);
                break;
            case 3:
                order_condition.setText(CONDITON_3);
                break;
        }

    }

}
