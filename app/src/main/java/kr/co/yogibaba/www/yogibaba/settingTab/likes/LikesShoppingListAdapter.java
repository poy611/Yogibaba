package kr.co.yogibaba.www.yogibaba.settingTab.likes;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import kr.co.yogibaba.www.yogibaba.R;
import kr.co.yogibaba.www.yogibaba.data.like.LikesShoppingList;

/**
 * Created by jah on 2016-05-22.
 */
public class LikesShoppingListAdapter extends RecyclerView.Adapter<LikesShoppingListViewHolder> {

    List<LikesShoppingList> items=new ArrayList<>();
    public void add(LikesShoppingList item)
    {
        items.add(item);
        notifyDataSetChanged();
    }
    public void addAll(List<LikesShoppingList> items){
        this.items.addAll(items);
        notifyDataSetChanged();
    }
    public void clear(){
        items.clear();
        notifyDataSetChanged();
    }

    @Override
    public LikesShoppingListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.view_likes_shopping_list
                ,null);
        return new LikesShoppingListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(LikesShoppingListViewHolder holder, int position) {
        holder.setShoppingDataByCategory(items.get(position));
        holder.setOnItemClickListener(mListener);
        holder.setOnButtonClickListener(bListener);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    LikesShoppingListViewHolder.OnItemClickListener mListener;
    LikesShoppingListViewHolder.OnButtonClickListener bListener;
    public void setOnItemClicListener(LikesShoppingListViewHolder.OnItemClickListener listener){
        mListener=listener;
    }
    public void setOnButtonClickListener(LikesShoppingListViewHolder.OnButtonClickListener listener){
        bListener=listener;
    }
}