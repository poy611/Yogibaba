package kr.co.yogibaba.www.yogibaba.videoTab.vrplayer;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import kr.co.yogibaba.www.yogibaba.R;
import kr.co.yogibaba.www.yogibaba.data.vrplayer.VrPlayerRelatedData;

/**
 * Created by OhDaeKyoung on 2016. 5. 23..
 */
public class VrPlayerViewHolder extends RecyclerView.ViewHolder {
    ImageView imageView;
    TextView itemName,itemPrice;
    VrPlayerRelatedData data;
    TextView btn_close, btn_go_detail;
    public interface OnItemClickListener {
         void onItemClick(View view, VrPlayerRelatedData product);
    }

    OnItemClickListener mListener;
    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }


    public interface OnCancelClickListener{
        void onCancleClick(View view);
    }
    OnCancelClickListener bListener;
    public void setOnCancleClickListener(OnCancelClickListener listener){
        bListener=listener;
    }
   // ViewFlipper viewFlipper;
    public VrPlayerViewHolder(View itemView) {
        super(itemView);
        //viewFlipper=(ViewFlipper)itemView.findViewById(R.id.viewflipper);
        imageView=(ImageView)itemView.findViewById(R.id.img_vr_related);
        itemName=(TextView)itemView.findViewById(R.id.text_vr_related_name);
        itemPrice=(TextView)itemView.findViewById(R.id.text_vr_related_price);
        btn_close=(TextView)itemView.findViewById(R.id.btn_close);
        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(bListener!=null)
                {
                    bListener.onCancleClick(v);
                }
            }
        });
        btn_go_detail=(TextView)itemView.findViewById(R.id.btn_go_detail);
        btn_go_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener!=null)
                {
                    mListener.onItemClick(v,data);
                }
            }
        });


    }
    void setVrPlayerRelatedData(VrPlayerRelatedData mData)
    {
        data=mData;
        Glide.with(imageView.getContext()).load(data.getItem_image_url()).into(imageView);
        itemName.setText(data.getItem_name());
        itemPrice.setText(data.getItem_price()+"won");

    }

}
