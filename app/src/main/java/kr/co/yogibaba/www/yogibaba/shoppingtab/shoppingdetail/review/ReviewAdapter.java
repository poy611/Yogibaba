package kr.co.yogibaba.www.yogibaba.shoppingtab.shoppingdetail.review;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import kr.co.yogibaba.www.yogibaba.R;
import kr.co.yogibaba.www.yogibaba.data.review.Reviews;

import kr.co.yogibaba.www.yogibaba.manager.PropertyManager;


/**
 * Created by OhDaeKyoung on 2016. 5. 19..
 */
public class ReviewAdapter extends RecyclerView.Adapter<ReviewViewHolder> {
    List<Reviews> items=new ArrayList<>();
    String user_id;
    public void add(Reviews item)
    {
        items.add(item);
        notifyDataSetChanged();
    }
    public void addAll(List<Reviews> items)
    {

        this.items.addAll(items);
        notifyDataSetChanged();
    }
    public void clear(){
        items.clear();
        notifyDataSetChanged();
    }
    public void remove(int position){
        items.remove(position);
        notifyDataSetChanged();
    }
    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public void onBindViewHolder(ReviewViewHolder holder, final int position) {
        holder.setReview(items.get(position));
        user_id= PropertyManager.getInstance().getUserId();
        //사용자 아이디와 내 아이디가 같으면 휴지통 보여라
        if(user_id==items.get(position).getUser_id()) {
            holder.btn.setVisibility(View.VISIBLE);
        }
        else{
            holder.btn.setVisibility(View.GONE);
        }
        holder.btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener!=null)
                {
                    mListener.onButtonClick(v,items.get(position));
                    remove(position);
                }
            }
        });
    }

    @Override
    public ReviewViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.view_review,null);
        return new ReviewViewHolder(view);
    }

    public interface OnButtonClickListener{
        public void onButtonClick(View v,Reviews product);
    }
    public OnButtonClickListener mListener;
    public void setOnButtonClickListener(OnButtonClickListener listener){
        mListener=listener;
    }
}
