package kr.co.yogibaba.www.yogibaba.settingTab.orderAndDelivery;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;

import kr.co.yogibaba.www.yogibaba.R;
import kr.co.yogibaba.www.yogibaba.data.order.OrderAndDeliveryPurchases;
import kr.co.yogibaba.www.yogibaba.data.order.OrderAndDeliveryResult;
import kr.co.yogibaba.www.yogibaba.data.order.OrderAndDeliverySuccess;
import kr.co.yogibaba.www.yogibaba.manager.NetworkManagerJH;
import kr.co.yogibaba.www.yogibaba.manager.PropertyManager;
import okhttp3.Request;

public class OrderAndDeliveryMainActivity extends AppCompatActivity {

    RecyclerView listview;
    OrderListAdapter mAdapter;
    String user_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_and_delivery_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        user_id= PropertyManager.getInstance().getUserId();
        listview = (RecyclerView)findViewById(R.id.rv_order_list);
        mAdapter = new OrderListAdapter();

        mAdapter.setOnButtonClickListner(new OrderListAdapter.OnButtonClickListenr() {
            @Override
            public void onButtonClick(OrderAndDeliveryPurchases product) {
                Intent intent = new Intent(OrderAndDeliveryMainActivity.this, OrderAndDeliveryDetailActivity.class);
                intent.putExtra("purchase_id",product.getPurchase_id());
                Toast.makeText(OrderAndDeliveryMainActivity.this,product.getPurchase_id()+"",Toast.LENGTH_SHORT).show();
                startActivity(intent);
            }
        });
        listview.setAdapter(mAdapter);
        listview.setLayoutManager(new LinearLayoutManager(this));

        setData();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void setData() {

//        NetworkManagerJH.getInstance().getOrderAndDelivery(this,user_id, new NetworkManagerJH.OnResultListener<OrderAndDeliveryResult>() {
//            @Override
//            public void onSuccess(Request request, OrderAndDeliveryResult result) {
//                mAdapter.clear();
//                if (result ==  null ){
//
//                    Toast.makeText(OrderAndDeliveryMainActivity.this, "물건좀사라", Toast.LENGTH_LONG).show();
//                    finish();
//                }
//                mAdapter.addAll(result);
//            }
//            @Override
//            public void onFail(Request request, IOException exception) {
//                Toast.makeText(getBaseContext(), "exception : " + exception.getMessage(), Toast.LENGTH_SHORT).show();
//            }
//        });

        NetworkManagerJH.getInstance().getOrderAndDeliverySuccess(this, user_id, new NetworkManagerJH.OnResultListener<OrderAndDeliverySuccess>() {
            @Override
            public void onSuccess(Request request, OrderAndDeliverySuccess result) {
               mAdapter.setData(result);
                if (mAdapter.getSuccess().getSuccess() == 1){
                    NetworkManagerJH.getInstance().getOrderAndDeliveryList(this, user_id ,new NetworkManagerJH.OnResultListener<List<OrderAndDeliveryPurchases>>() {
                        @Override
                        public void onSuccess(Request request, List<OrderAndDeliveryPurchases> result) {
                            mAdapter.clear();
                            mAdapter.addAll(result);
                        }
                        @Override
                        public void onFail(Request request, IOException exception) {
                            Toast.makeText(getBaseContext(), "exception : " + exception.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
                }
                else{
                    Toast.makeText(getBaseContext(), "구매내역이 없습니다" , Toast.LENGTH_LONG).show();
                    OrderAndDeliveryMainActivity.this.finish();
                }

            }

            @Override
            public void onFail(Request request, IOException exception) {
                Toast.makeText(getBaseContext(), "exception : " + exception.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

//        NetworkManagerJH.getInstance().getOrderAndDeliveryList(this, user_id ,new NetworkManagerJH.OnResultListener<List<OrderAndDeliveryPurchases>>() {
//            @Override
//            public void onSuccess(Request request, List<OrderAndDeliveryPurchases> result) {
//                mAdapter.clear();
//                mAdapter.addAll(result);
//            }
//            @Override
//            public void onFail(Request request, IOException exception) {
//                Toast.makeText(getBaseContext(), "exception : " + exception.getMessage(), Toast.LENGTH_SHORT).show();
//            }
//        });


    }
}
