package kr.co.yogibaba.www.yogibaba.data.search;

import java.util.List;

/**
 * Created by OhDaeKyoung on 2016. 5. 26..
 */
public class SearchItemRecomResult {
    String message;
    List<SearchItemRecom> items;

    public String getMessage() {
        return message;
    }

    public List<SearchItemRecom> getItems() {
        return items;
    }
}
