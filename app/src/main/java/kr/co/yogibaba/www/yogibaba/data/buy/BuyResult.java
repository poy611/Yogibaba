package kr.co.yogibaba.www.yogibaba.data.buy;

import java.util.List;

/**
 * Created by OhDaeKyoung on 2016. 5. 25..
 */
public class BuyResult {
    String message;
    List<BuyItem> purchase_item;
    String orderer_name;
    String orderer_email;
    String orderer_phone_number;

    public List<BuyItem> getPurchase_item() {
        return purchase_item;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public String getOrderer_name() {
        return orderer_name;
    }

    public void setOrderer_name(String orderer_name) {
        this.orderer_name = orderer_name;
    }

    public String getOrderer_email() {
        return orderer_email;
    }

    public void setOrderer_email(String orderer_email) {
        this.orderer_email = orderer_email;
    }

    public String getOrderer_phone_number() {
        return orderer_phone_number;
    }

    public void setOrderer_phone_number(String orderer_phone_number) {
        this.orderer_phone_number = orderer_phone_number;
    }
}
