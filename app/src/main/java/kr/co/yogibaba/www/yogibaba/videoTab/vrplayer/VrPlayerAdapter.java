package kr.co.yogibaba.www.yogibaba.videoTab.vrplayer;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import kr.co.yogibaba.www.yogibaba.R;

import kr.co.yogibaba.www.yogibaba.data.vrplayer.VrPlayerRelatedData;

/**
 * Created by OhDaeKyoung on 2016. 5. 20..
 */
public class VrPlayerAdapter extends RecyclerView.Adapter<VrPlayerViewHolder> {
    List<VrPlayerRelatedData> items=new ArrayList<>();
    public void add(VrPlayerRelatedData item)
    {
        items.add(item);
        notifyDataSetChanged();
    }
    public void addAll(List<VrPlayerRelatedData> items)
    {
        this.items.addAll(items);
        notifyDataSetChanged();
    }
    public void clear(){
        items.clear();
        notifyDataSetChanged();
    }

    @Override
    public VrPlayerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=LayoutInflater.from(parent.getContext()).inflate(R.layout.view_vr_player,null);
        return new VrPlayerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(VrPlayerViewHolder holder, int position) {
        holder.setVrPlayerRelatedData(items.get(position));
        holder.setOnItemClickListener(mListener);
        holder.setOnCancleClickListener(bListener);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
    VrPlayerViewHolder.OnItemClickListener mListener;

    public void setOnItemClicListener(VrPlayerViewHolder.OnItemClickListener listener) {
        mListener = listener;
    }

    VrPlayerViewHolder.OnCancelClickListener bListener;

    public void setOnCanccelClickListener(VrPlayerViewHolder.OnCancelClickListener listener)
    {
        bListener=listener;
    }
}
