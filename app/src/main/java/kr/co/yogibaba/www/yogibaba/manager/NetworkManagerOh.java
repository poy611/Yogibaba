package kr.co.yogibaba.www.yogibaba.manager;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.concurrent.TimeUnit;

import kr.co.yogibaba.www.yogibaba.MyApplication;
import kr.co.yogibaba.www.yogibaba.data.ClickLike;
import kr.co.yogibaba.www.yogibaba.data.ClickUnLike;
import kr.co.yogibaba.www.yogibaba.data.basket.BasketResults;
import kr.co.yogibaba.www.yogibaba.data.buy.BuyResults;
import kr.co.yogibaba.www.yogibaba.data.etc.Click;
import kr.co.yogibaba.www.yogibaba.data.review.ReviewResults;
import kr.co.yogibaba.www.yogibaba.data.search.SearchItemRecomResults;
import kr.co.yogibaba.www.yogibaba.data.search.SearchItemResults;
import kr.co.yogibaba.www.yogibaba.data.search.SearchVideoRecomResults;
import kr.co.yogibaba.www.yogibaba.data.search.SearchVideoResults;
import kr.co.yogibaba.www.yogibaba.data.shoppingdetail.ShoppingDetailResult;
import kr.co.yogibaba.www.yogibaba.data.videodetail.VideoDetailResults;
import kr.co.yogibaba.www.yogibaba.data.videolist.VideoListResults;
import kr.co.yogibaba.www.yogibaba.data.vrplayer.VrPlayerResult;
import okhttp3.Cache;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.JavaNetCookieJar;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by dongja94 on 2016-05-09.
 */
public class NetworkManagerOh {
    private static NetworkManagerOh instance;
    public static NetworkManagerOh getInstance() {
        if (instance == null) {
            instance = new NetworkManagerOh();
        }
        return instance;
    }

    private static final int DEFAULT_CACHE_SIZE = 50 * 1024 * 1024;
    private static final String DEFAULT_CACHE_DIR = "miniapp";
    OkHttpClient mClient;
    private NetworkManagerOh() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        Context context = MyApplication.getContext();
        CookieManager cookieManager = new CookieManager(new PersistentCookieStore(context), CookiePolicy.ACCEPT_ALL);
        builder.cookieJar(new JavaNetCookieJar(cookieManager));

        File dir = new File(context.getExternalCacheDir(), DEFAULT_CACHE_DIR);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        builder.cache(new Cache(dir, DEFAULT_CACHE_SIZE));

        builder.connectTimeout(30, TimeUnit.SECONDS);
        builder.readTimeout(30, TimeUnit.SECONDS);
        builder.writeTimeout(30, TimeUnit.SECONDS);

        mClient = builder.build();
    }

    public interface OnResultListener<T> {
        public void onSuccess(Request request, T result);
        public void onFail(Request request, IOException exception);
    }

    private static final int MESSAGE_SUCCESS = 1;
    private static final int MESSAGE_FAIL = 2;

    class NetworkHandler extends Handler {
        public NetworkHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            NetworkResult result = (NetworkResult)msg.obj;
            switch (msg.what) {
                case MESSAGE_SUCCESS :
                    result.listener.onSuccess(result.request, result.result);
                    break;
                case MESSAGE_FAIL :
                    result.listener.onFail(result.request, result.excpetion);
                    break;
            }
        }
    }

    NetworkHandler mHandler = new NetworkHandler(Looper.getMainLooper());

    static class NetworkResult<T> {
        Request request;
        OnResultListener<T> listener;
        IOException excpetion;
        T result;
    }

    Gson gson = new Gson();

    private static final String URL_TYPE="http://52.79.169.4:3000";
    private static final String VIDEOLIST_URL = URL_TYPE+"/home/movies";
    public Request getVideoList(Object tag, OnResultListener<VideoListResults> listener) {
        Request request = new Request.Builder()
                .url(VIDEOLIST_URL)
                .header("Accept", "application/json")
                .build();
        final NetworkResult<VideoListResults> result = new NetworkResult<>();
        result.request = request;
        result.listener = listener;
        mClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                result.excpetion = e;
                mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_FAIL, result));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    VideoListResults data = gson.fromJson(response.body().charStream(), VideoListResults.class);
                    result.result = data;
                    mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_SUCCESS, result));
                } else {
                    throw new IOException(response.message());
                }
            }
        });
        return request;
    }
    private static final String VIDEO_DETAIL_PAGE = URL_TYPE+"/movie/info/%s";

    public Request getVideoDetailList(Object tag, int movieId,OnResultListener<VideoDetailResults> listener) {
        String url = String.format(VIDEO_DETAIL_PAGE,movieId);
        Request request = new Request.Builder()
                .url(url)
                .header("Accept", "application/json")
                .build();
        final NetworkResult<VideoDetailResults> result = new NetworkResult<>();
        result.request = request;
        result.listener = listener;
        mClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                result.excpetion = e;
                mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_FAIL, result));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    VideoDetailResults data = gson.fromJson(response.body().charStream(), VideoDetailResults.class);
                    result.result = data;
                    mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_SUCCESS, result));
                } else {
                    throw new IOException(response.message());
                }
            }
        });
        return request;
    }
    private static final String VIDEO_DETAIL_PAGE_LOGIN = URL_TYPE+"/movie/info";

    public Request getVideoDetailListLogin(Object tag, int movieId, String userId ,OnResultListener<VideoDetailResults> listener) {
        RequestBody body = new FormBody.Builder()
                .add("movie_id", "" + movieId)
                .add("user_id", userId)
                .build();

        Request request = new Request.Builder()
                .url(VIDEO_DETAIL_PAGE_LOGIN)
                .header("Accept", "application/json")
                .post(body)
                .build();
        final NetworkResult<VideoDetailResults> result = new NetworkResult<>();
        result.request = request;
        result.listener = listener;
        mClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                result.excpetion = e;
                mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_FAIL, result));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    VideoDetailResults data = gson.fromJson(response.body().charStream(), VideoDetailResults.class);
                    result.result = data;
                    mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_SUCCESS, result));
                } else {
                    throw new IOException(response.message());
                }
            }
        });
        return request;
    }
    private static final String VIDEO_DETAIL_PAGE_LIKE = URL_TYPE+"/movie/info/like";

    public Request setEventLikeClick(Object tag, int movieId, String userId ,OnResultListener<ClickLike> listener) {
        RequestBody fromBody = new FormBody.Builder()
                .add("movie_id", "" + movieId)
                .add("user_id", userId)
                .build();
        Request request = new Request.Builder()
                .url(VIDEO_DETAIL_PAGE_LIKE)
                .header("Accept", "application/json")
                .post(fromBody)
                .build();
        final NetworkResult<ClickLike> result = new NetworkResult<>();
        result.request = request;
        result.listener = listener;
        mClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                result.excpetion = e;
                mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_FAIL, result));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    ClickLike data = gson.fromJson(response.body().charStream(), ClickLike.class);
                    result.result = data;
                    mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_SUCCESS, result));
                } else {
                    throw new IOException(response.message());
                }
            }
        });
        return request;
    }
    private static final String VIDEO_DETAIL_PAGE_UNLIKE = URL_TYPE+"/movie/info/unlike";

    public Request setEventUNLikeClick(Object tag, int movieId, String userId ,OnResultListener<ClickUnLike> listener) {
        RequestBody fromBody = new FormBody.Builder()
                .add("movie_id", "" + movieId)
                .add("user_id", userId)
                .build();
        Request request = new Request.Builder()
                .url(VIDEO_DETAIL_PAGE_UNLIKE)
                .header("Accept", "application/json")
                .post(fromBody)
                .build();
        final NetworkResult<ClickUnLike> result = new NetworkResult<>();
        result.request = request;
        result.listener = listener;
        mClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                result.excpetion = e;
                mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_FAIL, result));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    ClickUnLike data = gson.fromJson(response.body().charStream(), ClickUnLike.class);
                    result.result = data;
                    mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_SUCCESS, result));
                } else {
                    throw new IOException(response.message());
                }
            }
        });
        return request;
    }
    private static final String VIDEO_CLICK_URL = URL_TYPE+"/movie/player/%s";

    public Request setVideoClick(Object tag, int movieId,OnResultListener<VrPlayerResult> listener) {
        String url = String.format(VIDEO_CLICK_URL, movieId);
        Request request = new Request.Builder()
                .url(url)
                .header("Accept", "application/json")
                .build();
        final NetworkResult<VrPlayerResult> result = new NetworkResult<>();
        result.request = request;
        result.listener = listener;
        mClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                result.excpetion = e;
                mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_FAIL, result));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    VrPlayerResult data = gson.fromJson(response.body().charStream(), VrPlayerResult.class);
                    result.result = data;
                    mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_SUCCESS, result));
                } else {
                    throw new IOException(response.message());
                }
            }
        });
        return request;
    }
    private static final String SHOPPING_DETAIL = URL_TYPE+"/shopping/info/items/%s";

    public Request getShoppingDetail(Object tag, int itemId,OnResultListener<ShoppingDetailResult> listener) {
        String url = String.format(SHOPPING_DETAIL, itemId);
        Request request = new Request.Builder()
                .url(url)
                .header("Accept", "application/json")
                .build();
        final NetworkResult<ShoppingDetailResult> result = new NetworkResult<>();
        result.request = request;
        result.listener = listener;
        mClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                result.excpetion = e;
                mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_FAIL, result));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    ShoppingDetailResult data = gson.fromJson(response.body().charStream(), ShoppingDetailResult.class);
                    result.result = data;
                    mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_SUCCESS, result));
                } else {
                    throw new IOException(response.message());
                }
            }
        });
        return request;
    }
    private static final String SHOPPING_DETAIL_LOGIN = URL_TYPE+"/shopping/info/items";

    public Request getShoppingDetailLogin(Object tag, int itemId,String userId,OnResultListener<ShoppingDetailResult> listener) {
        RequestBody fromBody = new FormBody.Builder()
                .add("item_id", "" + itemId)
                .add("user_id", userId)
                .build();
        Request request = new Request.Builder()
                .url(SHOPPING_DETAIL_LOGIN)
                .header("Accept", "application/json")
                .post(fromBody)
                .build();
        final NetworkResult<ShoppingDetailResult> result = new NetworkResult<>();
        result.request = request;
        result.listener = listener;
        mClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                result.excpetion = e;
                mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_FAIL, result));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    ShoppingDetailResult data = gson.fromJson(response.body().charStream(), ShoppingDetailResult.class);
                    result.result = data;
                    mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_SUCCESS, result));
                } else {
                    throw new IOException(response.message());
                }
            }
        });
        return request;
    }
    private static final String SHOPPING_CLICK_BUCKET= URL_TYPE+"/shopping/info/basket";

    public Request onClickBucketButton(Object tag, int itemId, String userId, int itemCount, String item_option,int optionPrice,OnResultListener<Click> listener) {

        RequestBody fromBody = new FormBody.Builder()
                .add("item_id",""+itemId)
                .add("user_id", userId)
                .add("item_count",""+itemCount)
                .add("option_name",item_option)
                .add("option_price",""+optionPrice)
                .build();
        Request request = new Request.Builder()
                .url(SHOPPING_CLICK_BUCKET)
                .header("Accept", "application/json")
                .post(fromBody)
                .build();
        final NetworkResult<Click> result = new NetworkResult<>();
        result.request = request;
        result.listener = listener;
        mClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                result.excpetion = e;
                mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_FAIL, result));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    Click data = gson.fromJson(response.body().charStream(), Click.class);
                    result.result = data;
                    mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_SUCCESS, result));
                } else {
                    throw new IOException(response.message());
                }
            }
        });
        return request;
    }
    private static final String SHOPPING_LIKE_BUTTON_CLICK= URL_TYPE+"/shopping/info/like";

    public Request onClickLikeButton(Object tag, int itemId, String userId, OnResultListener<Click> listener) {
        RequestBody fromBody = new FormBody.Builder()
                .add("item_id", ""+itemId)
                .add("user_id", userId)
                .build();

        Request request = new Request.Builder()
                .url(SHOPPING_LIKE_BUTTON_CLICK)
                .header("Accept", "application/json")
                .post(fromBody)
                .build();
        final NetworkResult<Click> result = new NetworkResult<>();
        result.request = request;
        result.listener = listener;
        mClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                result.excpetion = e;
                mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_FAIL, result));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    Click data = gson.fromJson(response.body().charStream(), Click.class);
                    result.result = data;
                    mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_SUCCESS, result));
                } else {
                    throw new IOException(response.message());
                }
            }
        });
        return request;
    }
    private static final String SHOPPING_UNLIKE_BUTTON_CLICK= URL_TYPE+"/shopping/info/unlike";

    public Request onClickUnLikeButton(Object tag, int itemId, String userId, OnResultListener<Click> listener) {
        RequestBody fromBody = new FormBody.Builder()
                .add("item_id", "" + itemId)
                .add("user_id", userId)
                .build();
        Request request = new Request.Builder()
                .url(SHOPPING_UNLIKE_BUTTON_CLICK)
                .header("Accept", "application/json")
                .post(fromBody)
                .build();
        final NetworkResult<Click> result = new NetworkResult<>();
        result.request = request;
        result.listener = listener;
        mClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                result.excpetion = e;
                mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_FAIL, result));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    Click data = gson.fromJson(response.body().charStream(), Click.class);
                    result.result = data;
                    mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_SUCCESS, result));
                } else {
                    throw new IOException(response.message());
                }
            }
        });
        return request;
    }
    private static final String REVIEW_LIST= URL_TYPE+"/shopping/info/review/%s";

    public Request getReviewList(Object tag, int itemId, OnResultListener<ReviewResults> listener) {
        String url = String.format(REVIEW_LIST, itemId);

        Request request = new Request.Builder()
                .url(url)
                .header("Accept", "application/json")
                .build();
        final NetworkResult<ReviewResults> result = new NetworkResult<>();
        result.request = request;
        result.listener = listener;
        mClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                result.excpetion = e;
                mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_FAIL, result));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    ReviewResults data = gson.fromJson(response.body().charStream(), ReviewResults.class);
                    result.result = data;
                    mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_SUCCESS, result));
                } else {
                    throw new IOException(response.message());
                }
            }
        });
        return request;
    }
    private static final String REVIEW_WRITE= URL_TYPE+"/shopping/info/review/write";

    public Request writeReivew(Object tag, int itemId, String userId,
                               String content, OnResultListener<Click> listener) {
        RequestBody fromBody = new FormBody.Builder()
                .add("item_id", "" + itemId)
                .add("user_id", userId)
                .add("review_content", content)
                .build();
        Request request = new Request.Builder()
                .url(REVIEW_WRITE)
                .header("Accept", "application/json")
                .post(fromBody)
                .build();
        final NetworkResult<Click> result = new NetworkResult<>();
        result.request = request;
        result.listener = listener;
        mClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                result.excpetion = e;
                mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_FAIL, result));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    Click data = gson.fromJson(response.body().charStream(), Click.class);
                    result.result = data;
                    mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_SUCCESS, result));
                } else {
                    throw new IOException(response.message());
                }
            }
        });
        return request;
    }
    private static final String REVIEW_DELETE= URL_TYPE+"/review/delete";

    public Request deleteReivew(Object tag, String userId,
                               int reviewId, OnResultListener<Click> listener) {
        RequestBody fromBody = new FormBody.Builder()
                .add("user_id", userId)
                .add("review_id",""+reviewId)
                .build();

        Request request = new Request.Builder()
                .url(REVIEW_DELETE)
                .header("Accept", "application/json")
                .post(fromBody)
                .build();
        final NetworkResult<Click> result = new NetworkResult<>();
        result.request = request;
        result.listener = listener;
        mClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                result.excpetion = e;
                mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_FAIL, result));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    Click data = gson.fromJson(response.body().charStream(), Click.class);
                    result.result = data;
                    mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_SUCCESS, result));
                } else {
                    throw new IOException(response.message());
                }
            }
        });
        return request;
    }
    private static final String BUY_ITEM= URL_TYPE+"/shopping/info/purchase";

    public Request getBuyItem(Object tag, String userId, int itemId, String optionName
                                ,int optionPrice,int itemCount, OnResultListener<BuyResults> listener) {
        RequestBody fromBody = new FormBody.Builder()
                .add("user_id", userId)
                .add("item_id", "" + itemId)
                .add("option_name", optionName)
                .add("option_price", "" + optionPrice)
                .add("item_count", "" + itemCount)
                .build();

        Request request = new Request.Builder()
                .url(BUY_ITEM)
                .header("Accept", "application/json")
                .post(fromBody)
                .build();
        final NetworkResult<BuyResults> result = new NetworkResult<>();
        result.request = request;
        result.listener = listener;
        mClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                result.excpetion = e;
                mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_FAIL, result));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    BuyResults data = gson.fromJson(response.body().charStream(), BuyResults.class);
                    result.result = data;
                    mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_SUCCESS, result));
                } else {
                    throw new IOException(response.message());
                }
            }
        });
        return request;
    }
    private static final String BUY_ITEM_BASKET= URL_TYPE+"/home/mypage/basket/purchase";

    public Request getBuyItemBaskey(Object tag, String userId,OnResultListener<BuyResults> listener) {
        RequestBody fromBody = new FormBody.Builder()
                .add("user_id", userId)
                .build();

        Request request = new Request.Builder()
                .url(BUY_ITEM_BASKET)
                .header("Accept", "application/json")
                .post(fromBody)
                .build();
        final NetworkResult<BuyResults> result = new NetworkResult<>();
        result.request = request;
        result.listener = listener;
        mClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                result.excpetion = e;
                mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_FAIL, result));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    BuyResults data = gson.fromJson(response.body().charStream(), BuyResults.class);
                    result.result = data;
                    mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_SUCCESS, result));
                } else {
                    throw new IOException(response.message());
                }
            }
        });
        return request;
    }
    private static final String BUY_ITEM_COMPLETE= URL_TYPE+"/purchase/finalpay";

    public Request getBuyItemComplete(Object tag
            , String userId
            ,String reciName
            ,String reciAddress
            ,String reciPhoneNumber
            ,String orderName
            ,String orderPhoneNumber
            ,OnResultListener<Click> listener) {
        RequestBody fromBody = new FormBody.Builder()
                .add("user_id", userId)
                .add("recipient_name", reciName)
                .add("recipient_address", reciAddress)
                .add("recipient_phone_number", reciPhoneNumber)
                .add("orderer_name", orderName)
                .add("orderer_phone_number", orderPhoneNumber)
                .build();

        Request request = new Request.Builder()
                .url(BUY_ITEM_COMPLETE)
                .header("Accept", "application/json")
                .post(fromBody)
                .build();
        final NetworkResult<Click> result = new NetworkResult<>();
        result.request = request;
        result.listener = listener;
        mClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                result.excpetion = e;
                mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_FAIL, result));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    Click data = gson.fromJson(response.body().charStream(), Click.class);
                    result.result = data;
                    mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_SUCCESS, result));
                } else {
                    throw new IOException(response.message());
                }
            }
        });
        return request;
    }
    private static final String SEARCH_VIDEO_VIEW_COUNT= URL_TYPE+"/home/search/movies/search/hit";

    public Request searchVideoViewCount(Object tag
            ,String searchWord
            ,OnResultListener<SearchVideoResults> listener) {
        RequestBody fromBody = new FormBody.Builder()
                .add("search_word", searchWord)
                .build();


        Request request = new Request.Builder()
                .url(SEARCH_VIDEO_VIEW_COUNT)
                .header("Accept", "application/json")
                .post(fromBody)
                .build();
        final NetworkResult<SearchVideoResults> result = new NetworkResult<>();
        result.request = request;
        result.listener = listener;
        mClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                result.excpetion = e;
                mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_FAIL, result));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    SearchVideoResults data = gson.fromJson(response.body().charStream(), SearchVideoResults.class);
                    result.result = data;
                    mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_SUCCESS, result));
                } else {
                    throw new IOException(response.message());
                }
            }
        });
        return request;
    }
    private static final String SEARCH_VIDEO_LIKE_COUNT= URL_TYPE+"/home/search/movies/search/like";

    public Request searchVideoLikeCount(Object tag
            ,String searchWord
            ,OnResultListener<SearchVideoResults> listener) {
        RequestBody fromBody = new FormBody.Builder()
                .add("search_word", searchWord)
                .build();

        Request request = new Request.Builder()
                .url(SEARCH_VIDEO_LIKE_COUNT)
                .header("Accept", "application/json")
                .post(fromBody)
                .build();
        final NetworkResult<SearchVideoResults> result = new NetworkResult<>();
        result.request = request;
        result.listener = listener;
        mClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                result.excpetion = e;
                mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_FAIL, result));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    SearchVideoResults data = gson.fromJson(response.body().charStream(), SearchVideoResults.class);
                    result.result = data;
                    mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_SUCCESS, result));
                } else {
                    throw new IOException(response.message());
                }
            }
        });
        return request;
    }
    private static final String SEARCH_ITEM_LIKE_COUNT= URL_TYPE+"/home/search/items/search/like";

    public Request searchItemLikeCount(Object tag
            ,String searchWord
            ,OnResultListener<SearchItemResults> listener) {
        RequestBody fromBody = new FormBody.Builder()
                .add("search_word", searchWord)
                .build();

        Request request = new Request.Builder()
                .url(SEARCH_ITEM_LIKE_COUNT)
                .header("Accept", "application/json")
                .post(fromBody)
                .build();
        final NetworkResult<SearchItemResults> result = new NetworkResult<>();
        result.request = request;
        result.listener = listener;
        mClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                result.excpetion = e;
                mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_FAIL, result));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    SearchItemResults data = gson.fromJson(response.body().charStream(), SearchItemResults.class);
                    result.result = data;
                    mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_SUCCESS, result));
                } else {
                    throw new IOException(response.message());
                }
            }
        });
        return request;
    }
    private static final String SEARCH_ITEM_VIEW_COUNT= URL_TYPE+"/home/search/items/search/hit";

    public Request searchItemViewCount(Object tag
            ,String searchWord
            ,OnResultListener<SearchItemResults> listener) {
        RequestBody fromBody = new FormBody.Builder()
                .add("search_word", searchWord)
                .build();

        Request request = new Request.Builder()
                .url(SEARCH_ITEM_VIEW_COUNT)
                .header("Accept", "application/json")
                .post(fromBody)
                .build();
        final NetworkResult<SearchItemResults> result = new NetworkResult<>();
        result.request = request;
        result.listener = listener;
        mClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                result.excpetion = e;
                mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_FAIL, result));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    SearchItemResults data = gson.fromJson(response.body().charStream(), SearchItemResults.class);
                    result.result = data;
                    mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_SUCCESS, result));
                } else {
                    throw new IOException(response.message());
                }
            }
        });
        return request;
    }
    private static final String SEARCH_VIDEO_RECOM= URL_TYPE+"/home/search/first/movie";

    public Request searchVideoRecom(Object tag
            ,OnResultListener<SearchVideoRecomResults> listener) {

        Request request = new Request.Builder()
                .url(SEARCH_VIDEO_RECOM)
                .header("Accept", "application/json")
                .build();
        final NetworkResult<SearchVideoRecomResults> result = new NetworkResult<>();
        result.request = request;
        result.listener = listener;
        mClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                result.excpetion = e;
                mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_FAIL, result));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    SearchVideoRecomResults data = gson.fromJson(response.body().charStream(), SearchVideoRecomResults.class);
                    result.result = data;
                    mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_SUCCESS, result));
                } else {
                    throw new IOException(response.message());
                }
            }
        });
        return request;
    }
    private static final String SEARCH_ITEM_RECOM= URL_TYPE+"/home/search/first/shopping";

    public Request searchItemRecom(Object tag
            ,OnResultListener<SearchItemRecomResults> listener) {

        Request request = new Request.Builder()
                .url(SEARCH_ITEM_RECOM)
                .header("Accept", "application/json")
                .build();
        final NetworkResult<SearchItemRecomResults> result = new NetworkResult<>();
        result.request = request;
        result.listener = listener;
        mClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                result.excpetion = e;
                mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_FAIL, result));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    SearchItemRecomResults data = gson.fromJson(response.body().charStream(), SearchItemRecomResults.class);
                    result.result = data;
                    mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_SUCCESS, result));
                } else {
                    throw new IOException(response.message());
                }
            }
        });
        return request;
    }
    private static final String BASKET_LIST= URL_TYPE+"/home/mypage/basket";

    public Request getBasketList(Object tag
            ,String userId
            ,OnResultListener<BasketResults> listener) {
        RequestBody fromBody = new FormBody.Builder()
                .add("user_id", userId)
                .build();

        Request request = new Request.Builder()
                .url(BASKET_LIST)
                .header("Accept", "application/json")
                .post(fromBody)
                .build();
        final NetworkResult<BasketResults> result = new NetworkResult<>();
        result.request = request;
        result.listener = listener;
        mClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                result.excpetion = e;
                mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_FAIL, result));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    BasketResults data = gson.fromJson(response.body().charStream(), BasketResults.class);
                    result.result = data;
                    mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_SUCCESS, result));
                } else {
                    throw new IOException(response.message());
                }
            }
        });
        return request;
    }

    private static final String BASKET_DELETE= URL_TYPE+"/home/mypage/basket/delete";

    public Request deleteBasket(Object tag
            ,String userId , int item_id, String option_name, int option_price, int item_count
            ,OnResultListener<Click> listener) {
        RequestBody fromBody = new FormBody.Builder()
                .add("user_id", userId)
                .add("item_id", "" + item_id)
                .add("option_name", option_name)
                .add("option_price", "" + option_price)
                .add("item_count", "" + item_count)
                .build();

        Request request = new Request.Builder()
                .url(BASKET_DELETE)
                .header("Accept", "application/json")
                .post(fromBody)
                .build();
        final NetworkResult<Click> result = new NetworkResult<>();
        result.request = request;
        result.listener = listener;
        mClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                result.excpetion = e;
                mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_FAIL, result));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    Click data = gson.fromJson(response.body().charStream(), Click.class);
                    result.result = data;
                    mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_SUCCESS, result));
                } else {
                    throw new IOException(response.message());
                }
            }
        });
        return request;
    }
    private static final String BASKET_UPDATE= URL_TYPE+"/home/mypage/basket/update";

    public Request updateBasket(Object tag
            ,String userId , int item_id, String option_name, int option_price, int item_count
            ,OnResultListener<Click> listener) {
        RequestBody fromBody = new FormBody.Builder()
                .add("user_id", userId)
                .add("item_id", "" + item_id)
                .add("option_name",option_name)
                .add("option_price", "" + option_price)
                .add("item_count",""+item_count)
                .build();

        Request request = new Request.Builder()
                .url(BASKET_UPDATE)
                .header("Accept", "application/json")
                .post(fromBody)
                .build();
        final NetworkResult<Click> result = new NetworkResult<>();
        result.request = request;
        result.listener = listener;
        mClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                result.excpetion = e;
                mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_FAIL, result));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    Click data = gson.fromJson(response.body().charStream(), Click.class);
                    result.result = data;
                    mHandler.sendMessage(mHandler.obtainMessage(MESSAGE_SUCCESS, result));
                } else {
                    throw new IOException(response.message());
                }
            }
        });
        return request;
    }
}
