package kr.co.yogibaba.www.yogibaba.shoppingtab.shoppingdetail.review;

import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import kr.co.yogibaba.www.yogibaba.R;

/**
 * Created by OhDaeKyoung on 2016. 6. 3..
 */
public class ReviewDivider extends RecyclerView.ItemDecoration {
    private  int top;
    private  int bottom;
    private  int left;
    private  int right;
    final Resources resources;


    public ReviewDivider(Activity activity) {
        resources = activity.getResources();
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {

        outRect.bottom=(int)resources.getDimension(R.dimen.recycler_video_detail_header_relate_bottom);

    }
}
