package kr.co.yogibaba.www.yogibaba.shoppingtab.newproducts;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import kr.co.yogibaba.www.yogibaba.R;
import kr.co.yogibaba.www.yogibaba.data.ShoppingDataByCategory;
import kr.co.yogibaba.www.yogibaba.data.ShoppingViewPageData;
import kr.co.yogibaba.www.yogibaba.shoppingtab.ViewpagerShopping;

/**
 * Created by OhDaeKyoung on 2016. 5. 18..
 */
public class NewProductAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    List<ShoppingDataByCategory> items = new ArrayList<>();


    public static final int VIEW_TYPE_VIEWPAGE = 1;
    public static final int VIEW_TYPE_RECYCLER = 2;

    public void add(ShoppingDataByCategory item) {
        items.add(item);
        notifyDataSetChanged();
    }

    public void addAll(List<ShoppingDataByCategory> items) {
        this.items.addAll(items);
        notifyDataSetChanged();
    }


    public void clear() {
        items.clear();
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if(items.size()<=3){
            return 1;
        }
        else{
            return 1+(items.size()-3);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int itemCount=-1;
        if (position == 0) {
            NewProductPagerViewHolder h = (NewProductPagerViewHolder) holder;
            List<ShoppingDataByCategory> tempitems=new ArrayList<>();
            h.setOnItemClickListener(bListener);
            if(items.size()<=3){
                for(int i=0; i<items.size(); i++)
                {
                    tempitems.add(items.get(i));
                }
                h.setShoppingViewPagerData(tempitems);
                return;
            }
            if(items.size()>3){
                for(int i=0; i<3; i++){
                    tempitems.add(items.get(i));
                }
                h.setShoppingViewPagerData(tempitems);
                return;
            }


        }
       else {
            NewProductViewHolder h = (NewProductViewHolder) holder;
            h.setShoppingDataByCategory(items.get(position + 2));
            h.setOnItemClickListener(mListener);
            return;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_RECYCLER) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_shopping_item_by_category, null);
            return new NewProductViewHolder(view);
        }
        if (viewType == VIEW_TYPE_VIEWPAGE) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_shopping_viewpager, null);
            return new NewProductPagerViewHolder(view, LayoutInflater.from(parent.getContext()));

        }
        throw new IllegalArgumentException("invalid position");
    }
    @Override
    public int getItemViewType(int position) {
        if(position==0)
        {
            return VIEW_TYPE_VIEWPAGE;
        }
        else {
            return VIEW_TYPE_RECYCLER;
        }
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    NewProductViewHolder.OnItemClickListener mListener;

    public void setOnItemClicListener(NewProductViewHolder.OnItemClickListener listener) {
        mListener = listener;
    }
    ViewpagerShopping.OnViewPagerClickListener bListener;
    public void setOnItemClickListener(ViewpagerShopping.OnViewPagerClickListener listener){
        bListener=listener;
    }
}
