package kr.co.yogibaba.www.yogibaba.settingTab.likes;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import kr.co.yogibaba.www.yogibaba.R;
import kr.co.yogibaba.www.yogibaba.data.like.LikesShoppingList;

/**
 * Created by jah on 2016-05-22.
 */
public class LikesShoppingListViewHolder extends RecyclerView.ViewHolder {
    ImageView imageView;
    TextView productTitle,productPrice;
    LikesShoppingList mdata;
    ImageView removeButton;
    ImageView hart;

    public interface OnItemClickListener {
         void onItemClick(View view, LikesShoppingList product);
    }
    OnItemClickListener mListener;
    public interface OnButtonClickListener {
         void onItemClick(View view, LikesShoppingList product);
    }
    OnButtonClickListener bListener;
    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }
    public void setOnButtonClickListener(OnButtonClickListener listener) {
        bListener = listener;
    }

    public LikesShoppingListViewHolder(View itemView) {
        super(itemView);
        imageView=(ImageView)itemView.findViewById(R.id.img_likes_shopping_item);
        productTitle=(TextView)itemView.findViewById(R.id.text_likes_shopping_item);
        productPrice=(TextView)itemView.findViewById(R.id.text_likes_shopping_item_price);
        hart=(ImageView)itemView.findViewById(R.id.like_hart);
        removeButton = (ImageView)itemView.findViewById(R.id.remove_button_likes_shop);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onItemClick(v,mdata);
            }
        });
        removeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(bListener!=null) {
                    bListener.onItemClick(v, mdata);
                }
            }
        });
    }

    public void setShoppingDataByCategory(LikesShoppingList data)
    {

//        Typeface typeface = Typeface.createFromAsset(getAssets(),"fontfile");
        mdata = data;
        if(data.getEdit_Type() == 1){
            removeButton.setVisibility(View.VISIBLE);
            hart.setVisibility(View.GONE);
        } else {
            removeButton.setVisibility(View.GONE);
            hart.setVisibility(View.VISIBLE);
        }

//        imageView.setImageResource(R.drawable.exampler);
        Glide.with(imageView.getContext()).load(data.getItem_image_url()).into(imageView);
        productTitle.setText(data.getItem_name());
        productPrice.setText(String.valueOf(data.getItem_price() + data.getOption_price()));

    }

}