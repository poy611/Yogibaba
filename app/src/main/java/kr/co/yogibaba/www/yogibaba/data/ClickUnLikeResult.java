package kr.co.yogibaba.www.yogibaba.data;

/**
 * Created by OhDaeKyoung on 2016. 5. 23..
 */
public class ClickUnLikeResult {
    String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
