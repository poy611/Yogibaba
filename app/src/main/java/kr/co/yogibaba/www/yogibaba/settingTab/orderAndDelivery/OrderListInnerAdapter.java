package kr.co.yogibaba.www.yogibaba.settingTab.orderAndDelivery;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import kr.co.yogibaba.www.yogibaba.R;
import kr.co.yogibaba.www.yogibaba.data.order.OrderAndDeliveryData;

/**
 * Created by jah on 2016-05-30.
 */
public class OrderListInnerAdapter extends RecyclerView.Adapter<OrderContentViewHolder> {

    List<OrderAndDeliveryData> items = new ArrayList<>();

    public void add(OrderAndDeliveryData data)
    {
        items.add(data);
        notifyDataSetChanged();
    }
    public void addAll(List<OrderAndDeliveryData> mData)
    {
        this.items.addAll(mData);
        notifyDataSetChanged();
    }
    public void clear(){
        items.clear();
        notifyDataSetChanged();
    }


    @Override
    public OrderContentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_order_item_list, null);
        return new OrderContentViewHolder(view);

    }

    @Override
    public void onBindViewHolder(OrderContentViewHolder holder, int position) {

        holder.setOrderContentData(items.get(position));
        return;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}
