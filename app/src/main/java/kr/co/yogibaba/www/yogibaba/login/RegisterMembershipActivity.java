package kr.co.yogibaba.www.yogibaba.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.Toast;

import com.facebook.AccessToken;

import java.io.IOException;

import kr.co.yogibaba.www.yogibaba.R;
import kr.co.yogibaba.www.yogibaba.StartLogonActivity;
import kr.co.yogibaba.www.yogibaba.data.login.LoginCheckSuccess;
import kr.co.yogibaba.www.yogibaba.manager.NetworkManagerJH;
import kr.co.yogibaba.www.yogibaba.manager.PropertyManager;
import okhttp3.Request;

public class RegisterMembershipActivity extends AppCompatActivity {
    public static final String MEMBER_EMIAL="email";
    public static final String MEMBER_PHONE="phone_number";
    public static final String MEMBER_ADDR="address";

    String email;
    String phone_number;
    String address;

    Button btn_join;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_membership);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final Intent intent=getIntent();
        email=intent.getStringExtra(MEMBER_EMIAL);
        phone_number=intent.getStringExtra(MEMBER_PHONE);
        address=intent.getStringExtra(MEMBER_ADDR);


        AccessToken token = AccessToken.getCurrentAccessToken();
        NetworkManagerJH.getInstance().setMembershipJoin(this, token.getToken(), email, phone_number, address, new NetworkManagerJH.OnResultListener<LoginCheckSuccess>() {
            @Override
            public void onSuccess(Request request, LoginCheckSuccess result) {
                Toast.makeText(RegisterMembershipActivity.this,String.valueOf(result.result.getUser_id()), Toast.LENGTH_SHORT).show();

                //넘어오는 user_id를 sharedpreference에 저장
                PropertyManager.getInstance().setUserId(""+result.result.getUser_id());

                PropertyManager.getInstance().setLogin(true);
                Intent intent = new Intent(RegisterMembershipActivity.this, StartLogonActivity.class);
                startActivity(intent);
                RegisterMembershipActivity.this.finish();

            }

            @Override
            public void onFail(Request request, IOException exception) {
                Toast.makeText(RegisterMembershipActivity.this,"fuck", Toast.LENGTH_SHORT).show();
            }
        });
    }



}


