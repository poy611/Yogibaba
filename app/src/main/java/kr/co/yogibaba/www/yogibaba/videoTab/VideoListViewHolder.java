package kr.co.yogibaba.www.yogibaba.videoTab;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.ShareApi;
import com.facebook.share.model.ShareLinkContent;

import java.util.Arrays;

import kr.co.yogibaba.www.yogibaba.R;
import kr.co.yogibaba.www.yogibaba.VrPlayerActivity;
import kr.co.yogibaba.www.yogibaba.data.videodetail.ItemRelatedInMovie;
import kr.co.yogibaba.www.yogibaba.data.videolist.VideoListData;
import kr.co.yogibaba.www.yogibaba.facebook.FacebookActivity;
import kr.co.yogibaba.www.yogibaba.manager.PropertyManager;

/**
 * Created by Tacademy on 2016-05-12.
 */
public class VideoListViewHolder extends RecyclerView.ViewHolder {
    ImageView imageView;
    TextView videoTitle,videoViewCount,videoLikes;
    ImageView imgbtn_hart;
    boolean is_like=false;
    VideoListData mData;
    ImageView btn_pub,btn_close_publ,btn_clip,btn_kakao_publ,btn_facebook_publ;

    FacebookActivity activity;



    public interface OnItemClickListener {
        public void onItemClick(View view, VideoListData product);
    }

    OnItemClickListener mListener;
    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    /* 테스트*/
    public interface OnButtonClickListener{
        void onButtonClick(View view, VideoListData product,ImageView btn_like);
    }
    OnButtonClickListener bListener;
    public void setOnButtonClikcListenter(OnButtonClickListener listenter)
    {
        bListener=listenter;
    }

//    ----------------------------- 테스트 ---------------------------------------------------

    public VideoListViewHolder(final View itemView, final Context context) {
        super(itemView);
        btn_pub=(ImageView)itemView.findViewById(R.id.btn_pub);

        btn_close_publ=(ImageView)itemView.findViewById(R.id.btn_close_publ);
        btn_clip=(ImageView)itemView.findViewById(R.id.btn_clip);
        btn_kakao_publ=(ImageView)itemView.findViewById(R.id.btn_kakao_publ);
        btn_facebook_publ=(ImageView)itemView.findViewById(R.id.btn_facebook_publ);

        btn_pub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Animation slideUp = AnimationUtils.loadAnimation(itemView.getContext(), R.anim.slide_up);
                Animation slideDown = AnimationUtils.loadAnimation(itemView.getContext(), R.anim.slide_down);

                btn_pub.startAnimation(slideDown);
                btn_pub.setVisibility(View.GONE);

                btn_close_publ.startAnimation(slideUp);
                btn_close_publ.setVisibility(View.VISIBLE);

                btn_clip.startAnimation(slideUp);
                btn_clip.setVisibility(View.VISIBLE);

                btn_kakao_publ.startAnimation(slideUp);
                btn_kakao_publ.setVisibility(View.VISIBLE);

                btn_facebook_publ.startAnimation(slideUp);
                btn_facebook_publ.setVisibility(View.VISIBLE);
            }
        });
        btn_close_publ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Animation slideUp = AnimationUtils.loadAnimation(itemView.getContext(), R.anim.slide_up);
                Animation slideDown = AnimationUtils.loadAnimation(itemView.getContext(), R.anim.slide_down);

                btn_close_publ.startAnimation(slideDown);
                btn_close_publ.setVisibility(View.GONE);

                btn_clip.startAnimation(slideDown);
                btn_clip.setVisibility(View.GONE);

                btn_kakao_publ.startAnimation(slideDown);
                btn_kakao_publ.setVisibility(View.GONE);

                btn_facebook_publ.startAnimation(slideDown);
                btn_facebook_publ.setVisibility(View.GONE);

                btn_pub.startAnimation(slideUp);
                btn_pub.setVisibility(View.VISIBLE);

            }
        });

        btn_facebook_publ.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (PropertyManager.getInstance().isLogin()){
                    Toast.makeText(context,"페이스북으로 요기바바 공유하기",Toast.LENGTH_SHORT).show();
                    activity = new FacebookActivity();
                    activity.post();
                }
                else
                {
                    Toast.makeText(context,"페이스북 로그인이 필요합니다",Toast.LENGTH_LONG).show();
                }
            }
        });

        btn_clip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context,"추가 공유 서비스 준비중",Toast.LENGTH_LONG).show();
            }
        });
        btn_kakao_publ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context,"카카오톡 서비스 준비중",Toast.LENGTH_LONG).show();
            }


        });
        videoLikes=(TextView)itemView.findViewById(R.id.video_likes);
        imageView=(ImageView)itemView.findViewById(R.id.imgview_video_list);
        videoTitle=(TextView)itemView.findViewById(R.id.text_video_list_title);
        imgbtn_hart=(ImageView)itemView.findViewById(R.id.imgbtn_hart);
//        imgbtn_hart.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                if(is_like){
//                    imgbtn_hart.setImageResource(R.drawable.likebtn_nomal);
//                    is_like=false;
//                }
//                else {
//                    imgbtn_hart.setImageResource(R.drawable.likebtn_press);
//                    is_like=true;
//                }
//            }
//        });
        videoViewCount=(TextView)itemView.findViewById(R.id.text_video_list_view_count);


        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener!=null) {
                    mListener.onItemClick(v, mData);
                }
            }
        });
    }
    public void setVideoListData(VideoListData data){
        mData=data;
        Glide.with(imageView.getContext()).load(data.getMovie_thumbnail()).into(imageView);
        videoTitle.setText(data.getMovie_title());
        videoViewCount.setText("조회수 :"+data.getMovie_hit());
        videoLikes.setText(""+data.getMovie_like_num());

    }

}
