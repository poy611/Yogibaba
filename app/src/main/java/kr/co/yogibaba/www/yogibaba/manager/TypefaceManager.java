package kr.co.yogibaba.www.yogibaba.manager;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by jah on 2016-06-09.
 */
public class TypefaceManager {

    private static  final TypefaceManager instance = new TypefaceManager();

    public static TypefaceManager getInstance(){
        return instance;
    }

    private TypefaceManager(){

    }

    public static final String FONT_NAME_REGULAR = "regular";
    public static final String FONT_NAME_MEDIUM = "medium";
    public static final String FONT_NAME_BOLD = "bold";
    public static final String FONT_NAME_TEST = "test";

    private Typeface regular;
    private Typeface medium;
    private Typeface bold;
    private Typeface test;

    public Typeface getTypeface(Context context, String fontName){
        if(FONT_NAME_REGULAR.equals(fontName)){
            if (regular == null){
                regular = Typeface.createFromAsset(context.getAssets(),"NotoSansCJKkr-Regular_0.otf");
            }
            return regular;
        }
        if(FONT_NAME_MEDIUM.equals(fontName)){
            if (medium == null){
                medium = Typeface.createFromAsset(context.getAssets(),"NotoSansCJKkr-Medium_1.otf");
            }
            return medium;
        }
        if(FONT_NAME_BOLD.equals(fontName)){
            if (bold == null){
                bold = Typeface.createFromAsset(context.getAssets(),"NotoSansCJKkr-Bold_1.otf");
            }
            return bold;
        }
        if(FONT_NAME_TEST.equals(fontName)){
            if (test == null){
                test = Typeface.createFromAsset(context.getAssets(),"ENBRUSH.TTF");
            }
            return test;
        }
        return null;
    }
}
