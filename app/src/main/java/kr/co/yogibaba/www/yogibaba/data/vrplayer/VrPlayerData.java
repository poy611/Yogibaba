package kr.co.yogibaba.www.yogibaba.data.vrplayer;

import java.util.List;

/**
 * Created by OhDaeKyoung on 2016. 5. 23..
 */
public class VrPlayerData {
    String message;
    public List<VrPlayerRelatedData> movie_item;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<VrPlayerRelatedData> getMovie_item() {
        return movie_item;
    }

    public void setMovie_item(List<VrPlayerRelatedData> movie_item) {
        this.movie_item = movie_item;
    }
}
