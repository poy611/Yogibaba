package kr.co.yogibaba.www.yogibaba.data.basket;

/**
 * Created by OhDaeKyoung on 2016. 5. 27..
 */
public class BasketItem {
    String option_name;
    int option_price;

    public String getOption_name() {
        return option_name;
    }

    public int getOption_price() {
        return option_price;
    }
}
