package kr.co.yogibaba.www.yogibaba.data.order;

/**
 * Created by jah on 2016-05-25.
 */
public class OrderAndDeliverySuccess {
    //OrderAndDeliverySuccess -> OrderAndDeliveryPurchases -> OrderAndDeliveryData
    public int success;
    public OrderAndDeliveryResult result;

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public OrderAndDeliveryResult getResult() {
        return result;
    }

    public void setResult(OrderAndDeliveryResult result) {
        this.result = result;
    }
}
