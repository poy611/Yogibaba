package kr.co.yogibaba.www.yogibaba.search;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

import kr.co.yogibaba.www.yogibaba.R;
import kr.co.yogibaba.www.yogibaba.data.search.SearchVideoRecom;
import kr.co.yogibaba.www.yogibaba.data.search.SearchVideoRecomResults;
import kr.co.yogibaba.www.yogibaba.data.search.SearchVideoResults;
import kr.co.yogibaba.www.yogibaba.manager.NetworkManagerOh;
import kr.co.yogibaba.www.yogibaba.search.videosearch.VideoSearchAdapter;
import kr.co.yogibaba.www.yogibaba.search.videosearch.VideoSearchRecomAdapter;
import kr.co.yogibaba.www.yogibaba.search.videosearch.VideoSearchRecomViewHolder;
import kr.co.yogibaba.www.yogibaba.videoTab.videodetail.VideoDetailActivity;
import okhttp3.Request;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link VideoSearchFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class VideoSearchFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public VideoSearchFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment VideoSearchFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static VideoSearchFragment newInstance(String param1, String param2) {
        VideoSearchFragment fragment = new VideoSearchFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }
    VideoSearchAdapter mAdapter;
    VideoSearchRecomAdapter mRecomAdapter;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        mRecomAdapter=new VideoSearchRecomAdapter();
        mRecomAdapter.setOnItemClickListener(new VideoSearchRecomViewHolder.OnItemClickListener() {
            @Override
            public void onItemClick(View view, SearchVideoRecom product) {
                Intent i = new Intent(getContext(), VideoDetailActivity.class);
                i.putExtra(VideoDetailActivity.MOVIE_ID,product.getMovie_id());
                startActivity(i);
            }
        });
        setRecomData();

    }
    RecyclerView recyclerView;
    GridLayoutManager gridLayoutManager;
    TextView textView,recomandText,likesNum;
    Spinner spinner;
    RelativeLayout relativeLayout;
    ImageView star;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_video_search, container, false);
        relativeLayout=(RelativeLayout)view.findViewById(R.id.rela_search);

        recyclerView=(RecyclerView)view.findViewById(R.id.rv_video_search);
        recyclerView.setAdapter(mRecomAdapter);

        star=(ImageView)view.findViewById(R.id.star);

        gridLayoutManager=new GridLayoutManager(getContext(),1);
        gridLayoutManager.setOrientation(GridLayout.HORIZONTAL);
       // gridLayoutManager.setSpanCount(2); //test
        recyclerView.setLayoutManager(gridLayoutManager);
        textView=(TextView)view.findViewById(R.id.text_search_count);
        spinner=(Spinner)view.findViewById(R.id.spinner);
        recomandText=(TextView)view.findViewById(R.id.text_recomand);

        return view;
    }
    void setData(String searchKeyword,String keyWord){
        if(searchKeyword.equals("조회순")){
            NetworkManagerOh.getInstance().searchVideoViewCount(getContext()
                    , keyWord
                    , new NetworkManagerOh.OnResultListener<SearchVideoResults>() {
                @Override
                public void onSuccess(Request request, SearchVideoResults result) {
                    Toast.makeText(getContext(), result.result.getMessage(), Toast.LENGTH_SHORT).show();
                    if (mAdapter != null && result.getSuccess() == 1) {
                        mAdapter.clear();
                        mAdapter.addAll(result.result.getMovies());
                        textView.setText("검색결과 : "+result.result.getMovies().size()+"개");
                    }
                }

                @Override
                public void onFail(Request request, IOException exception) {
                    Toast.makeText(getContext(),"네트워크 에러",Toast.LENGTH_SHORT).show();
                }
            });
        }
        else{
            NetworkManagerOh.getInstance().searchVideoLikeCount(getContext()
                    , keyWord
                    , new NetworkManagerOh.OnResultListener<SearchVideoResults>() {
                @Override
                public void onSuccess(Request request, SearchVideoResults result) {
                    Toast.makeText(getContext(), result.result.getMessage(), Toast.LENGTH_SHORT).show();
                    if (mAdapter != null && result.getSuccess() == 1) {
                        mAdapter.clear();
                        mAdapter.addAll(result.result.getMovies());
                        textView.setText("검색결과 : "+result.result.getMovies().size()+"개");
                    }
                }

                @Override
                public void onFail(Request request, IOException exception) {
                    Toast.makeText(getContext(), "네트워크 에러", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
    void setRecomData(){
        NetworkManagerOh.getInstance().searchVideoRecom(getContext()
                , new NetworkManagerOh.OnResultListener<SearchVideoRecomResults>() {
            @Override
            public void onSuccess(Request request, SearchVideoRecomResults result) {
                Toast.makeText(getContext(),result.result.getMessage(),Toast.LENGTH_SHORT).show();
                if(result.getSuccess()==1)
                {
                    mRecomAdapter.clear();
                    mRecomAdapter.addAll(result.result.getMovies());
                }
            }

            @Override
            public void onFail(Request request, IOException exception) {
                Toast.makeText(getContext(),"네트워크 에러",Toast.LENGTH_SHORT).show();
            }
        });
    }
}
