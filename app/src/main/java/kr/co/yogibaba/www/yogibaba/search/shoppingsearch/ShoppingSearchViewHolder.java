package kr.co.yogibaba.www.yogibaba.search.shoppingsearch;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import kr.co.yogibaba.www.yogibaba.R;
import kr.co.yogibaba.www.yogibaba.data.search.SearchItem;

/**
 * Created by OhDaeKyoung on 2016. 5. 16..
 */
public class ShoppingSearchViewHolder extends RecyclerView.ViewHolder {



    ImageView imageView;
    TextView productTitle,productManufatoruer,productPrice;
    SearchItem mdata;
    ImageView btn;


    public interface OnItemClickListener {
        void onItemClick(View view, SearchItem product);
    }

    OnItemClickListener mListener;
    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    public interface OnLikeButtonClickListener{
        void onButtonClick(View view, SearchItem product);
    }

    OnLikeButtonClickListener bListener;
    public void setOnLikeButtonClickListener(OnLikeButtonClickListener listener)
    {
        bListener=listener;
    }

    public ShoppingSearchViewHolder(View itemView) {
        super(itemView);
        Context context=itemView.getContext();
        imageView=(ImageView)itemView.findViewById(R.id.img_shopping_search);
        productTitle=(TextView)itemView.findViewById(R.id.text_shopping_search_title);

        productPrice=(TextView)itemView.findViewById(R.id.text_shopping_search_price);

        btn=(ImageView)itemView.findViewById(R.id.btn_search_like);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(bListener!=null)
                {
                    bListener.onButtonClick(v,mdata);
                }
            }
        });

        productManufatoruer=(TextView)itemView.findViewById(R.id.text_shopping_search_manufactorer);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onItemClick(v,mdata);
            }
        });
    }
    public void setShoppingDataByCategory(SearchItem data) {
        mdata=data;
        Glide.with(imageView.getContext()).load(data.getItem_image_url()).into(imageView);
        productTitle.setText(data.getItem_name());
        productManufatoruer.setText(data.getItem_brand());
        productPrice.setText(data.getItem_price()+"won");
    }

}
