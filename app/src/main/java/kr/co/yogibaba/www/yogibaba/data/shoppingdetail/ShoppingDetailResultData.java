package kr.co.yogibaba.www.yogibaba.data.shoppingdetail;

import java.util.List;

/**
 * Created by OhDaeKyoung on 2016. 5. 18..
 */
public class ShoppingDetailResultData {
    String message;
    int item_id;
    String item_image_url;
    String item_name;
    String item_brand;
    int num_basket;
    int item_price;
    int item_hit;
    int item_like_num;
    int movie_id;
    int review_num;

    public int getReview_num() {
        return review_num;
    }

    public void setReview_num(int review_num) {
        this.review_num = review_num;
    }

    public int getMovie_id() {
        return movie_id;
    }

    public void setMovie_id(int movie_id) {
        this.movie_id = movie_id;
    }

    public int getItem_hit() {
        return item_hit;
    }

    public void setItem_hit(int item_hit) {
        this.item_hit = item_hit;
    }

    public int getItem_like_num() {
        return item_like_num;
    }

    public void setItem_like_num(int item_like_num) {
        this.item_like_num = item_like_num;
    }

    public boolean is_item_like() {
        return is_item_like;
    }

    public void setIs_item_like(boolean is_item_like) {
        this.is_item_like = is_item_like;
    }

    List<ItemOptions> item_option;
    boolean is_item_like;
    String item_intro;
    String movie_url;
    String movie_thumbnail;

    List<ShoppingDetailRelateData> item_relate_items;

    public int getItem_price() {
        return item_price;
    }

    public void setItem_price(int item_price) {
        this.item_price = item_price;
    }
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getItem_id() {
        return item_id;
    }

    public void setItem_id(int item_id) {
        this.item_id = item_id;
    }

    public String getItem_image_url() {
        return item_image_url;
    }

    public void setItem_image_url(String item_image_url) {
        this.item_image_url = item_image_url;
    }

    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public String getItem_brand() {
        return item_brand;
    }

    public void setItem_brand(String item_brand) {
        this.item_brand = item_brand;
    }

    public int getNum_basket() {
        return num_basket;
    }

    public void setNum_basket(int num_basket) {
        this.num_basket = num_basket;
    }

    public List<ItemOptions> getItem_option() {
        return item_option;
    }

    public void setItem_option(List<ItemOptions> item_option) {
        this.item_option = item_option;
    }

    public boolean is_like() {
        return is_item_like;
    }

    public void setIs_like(boolean is_like) {
        this.is_item_like = is_like;
    }

    public String getItem_intro() {
        return item_intro;
    }

    public void setItem_intro(String item_intro) {
        this.item_intro = item_intro;
    }

    public String getMovie_url() {
        return movie_url;
    }

    public void setMovie_url(String movie_url) {
        this.movie_url = movie_url;
    }

    public String getMovie_thumbnail() {
        return movie_thumbnail;
    }

    public void setMovie_thumbnail(String movie_thumbnail) {
        this.movie_thumbnail = movie_thumbnail;
    }

    public List<ShoppingDetailRelateData> getItem_relate_items() {
        return item_relate_items;
    }

    public void setItem_relate_items(List<ShoppingDetailRelateData> item_relate_items) {
        this.item_relate_items = item_relate_items;
    }
}
