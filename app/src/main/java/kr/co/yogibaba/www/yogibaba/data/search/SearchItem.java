package kr.co.yogibaba.www.yogibaba.data.search;

/**
 * Created by OhDaeKyoung on 2016. 5. 26..
 */
public class SearchItem {
    int item_id;
    String item_name;
    String item_brand;
    int item_price;
    String item_image_url;

    public int getItem_price() {
        return item_price;
    }

    public int getItem_id() {
        return item_id;
    }

    public String getItem_name() {
        return item_name;
    }

    public String getItem_brand() {
        return item_brand;
    }

    public String getItem_image_url() {
        return item_image_url;
    }
}
