package kr.co.yogibaba.www.yogibaba.settingTab.orderAndDelivery;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;

import kr.co.yogibaba.www.yogibaba.R;
import kr.co.yogibaba.www.yogibaba.data.order.OrderAndDeliveryData;

import kr.co.yogibaba.www.yogibaba.manager.NetworkManagerJH;
import okhttp3.Request;

/**
 * Created by jah on 2016-05-30.
 */
public class OrderListViewHolder extends RecyclerView.ViewHolder {


    OrderListInnerAdapter mAdapter;
    RecyclerView recyclerView;
    LinearLayoutManager linearLayoutManager;

    public OrderListViewHolder(View itemView) {
        super(itemView);


        recyclerView = (RecyclerView)itemView.findViewById(R.id.rv_order_list_inner);
        mAdapter = new OrderListInnerAdapter();
        recyclerView.setAdapter(mAdapter);
        linearLayoutManager = new LinearLayoutManager(itemView.getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);

    }

    public void setList(List<OrderAndDeliveryData> items) {

        mAdapter.clear();
        mAdapter.addAll(items);
    }
}
