package kr.co.yogibaba.www.yogibaba.shoppingtab.productbuy;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;

import kr.co.yogibaba.www.yogibaba.CustomRecyclerDivider;
import kr.co.yogibaba.www.yogibaba.R;
import kr.co.yogibaba.www.yogibaba.data.buy.BuyItem;
import kr.co.yogibaba.www.yogibaba.data.buy.BuyResults;
import kr.co.yogibaba.www.yogibaba.data.etc.Click;
import kr.co.yogibaba.www.yogibaba.data.shoppingdetail.buy.BuyProductData;
import kr.co.yogibaba.www.yogibaba.manager.NetworkManagerOh;
import okhttp3.Request;

public class ProductBuyActivity extends AppCompatActivity {
    public static final String USER_ID="user_id";
    public static final String ITEM_ID="item_id";
    public static final String OPTION_NAME="option_name";
    public static final String OPTION_PRICE="option_price";
    public static final String ITEM_COUNT="item_count";
    public static final String TYPE_BUY="type_buy";
    String user_id;
    int item_id;
    String option_name;
    int option_price;
    int item_count;
    int type_buy;
    int total_price;
    RecyclerView recyclerView;
    ProductBuyAdapter mAdapter;
    TextView text_item_price,text_delv_price,text_total_price;

    EditText edit_order_name,edit_email,edit_phone;
    EditText edit_take_name,edit_take_phone,edit_home_address;


    Button btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_buy);
        Intent i=getIntent();
        user_id=i.getStringExtra(USER_ID);
        item_id=i.getIntExtra(ITEM_ID, 1);
        option_name=i.getStringExtra(OPTION_NAME);
        option_price=i.getIntExtra(OPTION_PRICE, 0);
        item_count=i.getIntExtra(ITEM_COUNT, 1);
        type_buy=i.getIntExtra(TYPE_BUY,-1);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("결제하기");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mAdapter=new ProductBuyAdapter();
        recyclerView=(RecyclerView)findViewById(R.id.rv_buy);
        recyclerView.setAdapter(mAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new CustomRecyclerDivider(ProductBuyActivity.this));
        text_item_price=(TextView)findViewById(R.id.text_item_price);   //상품가격
        text_delv_price=(TextView)findViewById(R.id.text_delv_price);   //배송비
        text_total_price=(TextView)findViewById(R.id.text_total_price); //총가격

        edit_order_name=(EditText)findViewById(R.id.edit_order_name); //주문자 명
        edit_email=(EditText)findViewById(R.id.edit_email); //이메일
        edit_phone=(EditText)findViewById(R.id.edit_phone); //휴대전화

        edit_take_name=(EditText)findViewById(R.id.edit_take_name); //수령인 명
        edit_take_phone=(EditText)findViewById(R.id.edit_take_phone); //수령인 전화번호
        edit_home_address=(EditText)findViewById(R.id.edit_home_address);//주소

        btn=(Button)findViewById(R.id.btn_purchase);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ProductBuyActivity.this, PurchaseCompleteActivity.class);
                i.putExtra(PurchaseCompleteActivity.ORDER_NAME, edit_order_name.getText().toString());
                i.putExtra(PurchaseCompleteActivity.RECIPIENT_NAME, edit_take_name.getText().toString());
                i.putExtra(PurchaseCompleteActivity.RECIPIENT_ADDRESS, edit_home_address.getText().toString());
                i.putExtra(PurchaseCompleteActivity.RECIPIENT_PHONE_NUMBER, edit_take_phone.getText().toString());
                i.putExtra(PurchaseCompleteActivity.ORDERER_PHONE_NUMBER, edit_phone.getText().toString());
                i.putExtra(PurchaseCompleteActivity.USER_ID, user_id);
                i.putExtra(PurchaseCompleteActivity.TOTAL_PRICE,total_price);
                startActivity(i);
                finish();
            }
        });




    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int type=item.getItemId();
        if(type==android.R.id.home)
        {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setData();
    }

    void setData(){
        if(type_buy==0) {
            NetworkManagerOh.getInstance().getBuyItem(ProductBuyActivity.this
                    , user_id
                    , item_id
                    , option_name
                    , option_price
                    , item_count
                    , new NetworkManagerOh.OnResultListener<BuyResults>() {
                @Override
                public void onSuccess(Request request, BuyResults result) {
                    Toast.makeText(ProductBuyActivity.this,result.result.getMessage(),Toast.LENGTH_SHORT).show();
                    if(result.getSuccess()==1) {
                        List<BuyItem> items=result.result.getPurchase_item();
                        mAdapter.clear();
                        mAdapter.addAll(result.result.getPurchase_item());
                        int totalPrice=0;
                        for(int i=0; i<items.size(); i++)
                        {
                          totalPrice+=(items.get(i).getItem_price()+items.get(i).getOption_price())*items.get(i).getItem_count();
                        }
                        text_item_price.setText(""+totalPrice);
                        text_total_price.setText(""+(totalPrice+2500));

                        edit_order_name.setText(result.result.getOrderer_name());
                        edit_email.setText(result.result.getOrderer_email());
                        edit_phone.setText(result.result.getOrderer_phone_number());

                        total_price=totalPrice+2500;
                    }
                }

                @Override
                public void onFail(Request request, IOException exception) {
                    Toast.makeText(ProductBuyActivity.this,"네트워크 에러",Toast.LENGTH_SHORT).show();
                }
            });
            return;
        }
        if(type_buy==1)
        {
            NetworkManagerOh.getInstance().getBuyItemBaskey(this
                    , user_id
                    , new NetworkManagerOh.OnResultListener<BuyResults>() {
                @Override
                public void onSuccess(Request request, BuyResults result) {
                    Toast.makeText(ProductBuyActivity.this,result.result.getMessage(),Toast.LENGTH_SHORT).show();
                    if(result.getSuccess()==1)
                    {
                        List<BuyItem> items=result.result.getPurchase_item();
                        mAdapter.clear();
                        mAdapter.addAll(result.result.getPurchase_item());
                        int totalPrice=0;
                        for(int i=0; i<items.size(); i++)
                        {
                            totalPrice+=(items.get(i).getItem_price()+items.get(i).getOption_price())*items.get(i).getItem_count();
                        }
                        text_item_price.setText(""+totalPrice);
                        text_total_price.setText(""+(totalPrice+2500));

                        edit_order_name.setText(result.result.getOrderer_name());
                        edit_email.setText(result.result.getOrderer_email());
                        edit_phone.setText(result.result.getOrderer_phone_number());
                        total_price=totalPrice+2500;
                    }
                }

                @Override
                public void onFail(Request request, IOException exception) {
                    Toast.makeText(ProductBuyActivity.this,"네트워크에러",Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}
