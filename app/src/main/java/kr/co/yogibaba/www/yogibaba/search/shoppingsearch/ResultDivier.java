package kr.co.yogibaba.www.yogibaba.search.shoppingsearch;

import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import kr.co.yogibaba.www.yogibaba.R;

/**
 * Created by OhDaeKyoung on 2016. 5. 31..
 */
public class ResultDivier extends RecyclerView.ItemDecoration {
    private  int top;
    private  int bottom;
    private  int left;
    private  int right;
    final Resources resources;


    public ResultDivier(Activity activity) {
        resources = activity.getResources();
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        // super.getItemOffsets(outRect, view, parent, state);
        int position=parent.getChildAdapterPosition(view);
        if(position==0 || position%2==0) {
            outRect.top = (int) resources.getDimension(R.dimen.search_top);
            outRect.bottom = (int) resources.getDimension(R.dimen.recycler_video_detail_margin_bottom);
            outRect.left = (int) resources.getDimension(R.dimen.search_left);
            outRect.right = (int) resources.getDimension(R.dimen.search_right_little);
        }
        else{
            outRect.top = (int) resources.getDimension(R.dimen.search_top);
            outRect.bottom = (int) resources.getDimension(R.dimen.recycler_video_detail_margin_bottom);
            outRect.left = (int) resources.getDimension(R.dimen.search_left_little);
            outRect.right = (int) resources.getDimension(R.dimen.search_right);

        }

    }
}
