package kr.co.yogibaba.www.yogibaba.data.shoppingdetail;

/**
 * Created by OhDaeKyoung on 2016. 5. 18..
 */
public class ShoppingDetailResult {
    int success;
    public ShoppingDetailResultData result;

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }
}
