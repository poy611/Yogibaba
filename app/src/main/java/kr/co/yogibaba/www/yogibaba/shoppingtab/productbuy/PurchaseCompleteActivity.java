package kr.co.yogibaba.www.yogibaba.shoppingtab.productbuy;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

import kr.co.yogibaba.www.yogibaba.R;
import kr.co.yogibaba.www.yogibaba.StartLogonActivity;
import kr.co.yogibaba.www.yogibaba.data.etc.Click;
import kr.co.yogibaba.www.yogibaba.manager.NetworkManagerOh;
import kr.co.yogibaba.www.yogibaba.manager.TypefaceManager;
import okhttp3.Request;

public class PurchaseCompleteActivity extends AppCompatActivity {
    public static final String USER_ID="user_id";
    public static final String RECIPIENT_NAME="recipient_name";
    public static final String RECIPIENT_ADDRESS="recipient_address";
    public static final String RECIPIENT_PHONE_NUMBER="recipient_phone_number";
    public static final String ORDER_NAME="order_name";
    public static final String ORDERER_PHONE_NUMBER="orderer_phone_number";
    public static final String TOTAL_PRICE="total_price";
    String user_id;
    String recipt_name;
    String recipt_address;
    String recipt_phone_number;
    String order_Name;
    String ordererPhoneNumber;
    int total_price;
    TextView totalPrice;
    Button button;
    AppCompatDelegate mDelegate;

    //TextView userid,riciname,riciAdr,reciphone,ordername,orderphone;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //글꼴테스트
        if (Build.VERSION.SDK_INT < 11){
            getLayoutInflater().setFactory(this);
        } else {
            getLayoutInflater().setFactory2(this);
        }
        mDelegate = getDelegate();
        //글꼴테스트
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase_complete);

        final Intent intent=getIntent();
        user_id=intent.getStringExtra(USER_ID);
        recipt_name=intent.getStringExtra(RECIPIENT_NAME);
        recipt_address=intent.getStringExtra(RECIPIENT_ADDRESS);
        recipt_phone_number=intent.getStringExtra(RECIPIENT_PHONE_NUMBER);
        order_Name=intent.getStringExtra(ORDER_NAME);
        ordererPhoneNumber=intent.getStringExtra(ORDERER_PHONE_NUMBER);
        total_price=intent.getIntExtra(TOTAL_PRICE,0);
        setData();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);


        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        totalPrice=(TextView)findViewById(R.id.text_purchase_total_price);
        totalPrice.setText(total_price+" won");
        button=(Button)findViewById(R.id.btn_gohome);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(PurchaseCompleteActivity.this, StartLogonActivity.class);
                startActivity(intent);
                finish();
            }
        });



       /* userid=(TextView)findViewById(R.id.text_buy_user_id);
        userid.setText(user_id);
        riciname=(TextView)findViewById(R.id.text_buy_reci_name);
        riciname.setText(recipt_name);
        riciAdr=(TextView)findViewById(R.id.text_buy_reci_address);
        riciAdr.setText(recipt_address);
        reciphone=(TextView)findViewById(R.id.text_buy_reci_phone);
        reciphone.setText(recipt_phone_number);
        ordername=(TextView)findViewById(R.id.text_buy_order_name);
        ordername.setText(order_Name);
        orderphone=(TextView)findViewById(R.id.text_buy_order_phone);
        orderphone.setText(ordererPhoneNumber);*/


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int type=item.getItemId();
        if(type==android.R.id.home)
        {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
    void setData(){
        NetworkManagerOh.getInstance().getBuyItemComplete(PurchaseCompleteActivity.this
                , user_id
                , recipt_name
                , recipt_address
                , recipt_phone_number
                , order_Name
                , ordererPhoneNumber
                , new NetworkManagerOh.OnResultListener<Click>() {
            @Override
            public void onSuccess(Request request, Click result) {
                Toast.makeText(PurchaseCompleteActivity.this
                ,result.result.getMessage(),Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFail(Request request, IOException exception) {
                Toast.makeText(PurchaseCompleteActivity.this
                        ,"네트워크 에러",Toast.LENGTH_SHORT).show();
            }
        });
    }
    public View setCustomFont(View view, String name, Context context, AttributeSet attrs){
        if(view == null){
            if(name.equals("TextView")){
                view = new TextView(context,attrs);
            }
        }
        if (view != null && view instanceof TextView){
            TypedArray typedArray = context.obtainStyledAttributes(attrs, new int[]{android.R.attr.fontFamily});
            String fontFamily = typedArray.getString(0);
            typedArray.recycle();
            Typeface typeface = TypefaceManager.getInstance().getTypeface(context,fontFamily);
            if (typeface != null){
                TextView textView = (TextView)view;
                textView.setTypeface(typeface);
            }
        }
        return view;
    }

    @Override
    public View onCreateView(String name, Context context, AttributeSet attrs) {
        View view = super.onCreateView(name, context, attrs);
        if(Build.VERSION.SDK_INT < 11){
            if (view == null){
                view = mDelegate.createView(null, name, context, attrs);
                view = setCustomFont(view, name, context, attrs);
            }
        }
        return view;
    }

    @Override
    public View onCreateView(View parent, String name, Context context, AttributeSet attrs) {
        View view  = super.onCreateView(parent, name, context, attrs);
        if (view == null){
            view = mDelegate.createView(parent, name, context, attrs);
        }
        view = setCustomFont(view, name, context, attrs);

        return  view;
    }
}
