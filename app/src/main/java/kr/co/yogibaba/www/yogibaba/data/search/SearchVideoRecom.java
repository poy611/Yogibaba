package kr.co.yogibaba.www.yogibaba.data.search;

/**
 * Created by OhDaeKyoung on 2016. 5. 26..
 */
public class SearchVideoRecom {
    int movie_id;
    String movie_title;
    String movie_strapline;
    String movie_url;
    String movie_thumbnail;

    public int getMovie_id() {
        return movie_id;
    }

    public String getMovie_title() {
        return movie_title;
    }

    public String getMovie_strapline() {
        return movie_strapline;
    }

    public String getMovie_url() {
        return movie_url;
    }

    public String getMovie_thumbnail() {
        return movie_thumbnail;
    }
}
