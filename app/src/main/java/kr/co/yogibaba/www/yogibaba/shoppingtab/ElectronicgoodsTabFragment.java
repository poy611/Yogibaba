package kr.co.yogibaba.www.yogibaba.shoppingtab;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;

import kr.co.yogibaba.www.yogibaba.R;
import kr.co.yogibaba.www.yogibaba.data.ShoppingDataByCategory;
import kr.co.yogibaba.www.yogibaba.manager.NetworkManagerJH;
import kr.co.yogibaba.www.yogibaba.shoppingtab.electronicgoods.ElectronicgoodsAdapter;
import kr.co.yogibaba.www.yogibaba.shoppingtab.electronicgoods.ElectronicgoodsViewHolder;
import kr.co.yogibaba.www.yogibaba.shoppingtab.shoppingdetail.ShoppingDetailActivity;
import okhttp3.Request;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ElectronicgoodsTabFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ElectronicgoodsTabFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    public static final String TYPE_ELECTRONICGOODS = "3";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public ElectronicgoodsTabFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ElectronicgoodsTabFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ElectronicgoodsTabFragment newInstance(String param1, String param2) {
        ElectronicgoodsTabFragment fragment = new ElectronicgoodsTabFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }
    ElectronicgoodsAdapter mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        mAdapter=new ElectronicgoodsAdapter();
        mAdapter.setOnItemClicListener(new ElectronicgoodsViewHolder.OnItemClickListener() {
            @Override
            public void onItemClick(View view, ShoppingDataByCategory product) {
                Intent intent=new Intent(getContext(), ShoppingDetailActivity.class);
                intent.putExtra(ShoppingDetailActivity.ITEM_ID,product.getItem_id());
                startActivity(intent);
            }
        });


    }
    RecyclerView recyclerView;
    GridLayoutManager gridLayoutManager;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_electronicgoods_tab, container, false);
        recyclerView=(RecyclerView)view.findViewById(R.id.rv_shopping_elec);
        recyclerView.setAdapter(mAdapter);
        gridLayoutManager=new GridLayoutManager(getContext(),1);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.addItemDecoration(new ShoppingDivider(getActivity()));
        return  view;
    }

    @Override
    public void onResume() {
        super.onResume();
        setData();
    }

    private void setData(){
        NetworkManagerJH.getInstance().getYogibabaShoppingList(this, TYPE_ELECTRONICGOODS , new NetworkManagerJH.OnResultListener<List<ShoppingDataByCategory>>() {

            @Override
            public void onSuccess(Request request, List<ShoppingDataByCategory> item) {
                mAdapter.clear();
                mAdapter.addAll(item);
            }

            @Override
            public void onFail(Request request, IOException exception) {
                Toast.makeText(getContext(), "exception : " + exception.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });


    }

}
