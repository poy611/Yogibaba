package kr.co.yogibaba.www.yogibaba.data.shoppingdetail;

/**
 * Created by OhDaeKyoung on 2016. 5. 23..
 */
public class ItemOptions {
    String option_name;
    int option_price;

    public String getOption_name() {
        return option_name;
    }

    public void setOption_name(String option_name) {
        this.option_name = option_name;
    }

    public int getOption_price() {
        return option_price;
    }

    public void setOption_price(int option_price) {
        this.option_price = option_price;
    }
}
