package kr.co.yogibaba.www.yogibaba.videoTab.videodetail;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import kr.co.yogibaba.www.yogibaba.R;

import kr.co.yogibaba.www.yogibaba.data.videodetail.ItemInMovie;
import kr.co.yogibaba.www.yogibaba.data.videodetail.ItemRelatedInMovie;

/**
 * Created by OhDaeKyoung on 2016. 5. 16..
 */
public class VideoDetailAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public final static int HEADER_OF_IN_MOVIE_TYPE=0;
    public final static int BODY_OF_ON_MOVIE_TYPE=1;
    public final static int HEADER_OF_RELATED_MOVIE=2;
    public final static int BODY_OF_RELATED_MOVIE=3;


    List<ItemInMovie> items=new ArrayList<>();
    List<ItemRelatedInMovie> items2=new ArrayList<>();
    public void add(ItemInMovie item, ItemRelatedInMovie item2)
    {
        items.add(item);
        items2.add(item2);
        notifyDataSetChanged();
    }
    public void addAll(List<ItemInMovie> items,List<ItemRelatedInMovie> items2)
    {
        this.items.addAll(items);
        this.items2.addAll(items2);
        notifyDataSetChanged();
    }
    public void clear(){
        items.clear();
        items2.clear();
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType==HEADER_OF_IN_MOVIE_TYPE)
        {
            View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.view_video_detail_movie_head,null);
            return new VideoDetailMovieHeadViewHolder(view);
        }
        else if(viewType==BODY_OF_ON_MOVIE_TYPE)
        {
            View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.view_video_detail,null);
            return new VideoDetailViewHolder(view);
        }
        else if(viewType==HEADER_OF_RELATED_MOVIE)
        {
            View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.view_video_detail_related_head,null);
            return new VideoDetailRelatedHeadViewHolder(view);
        }
        else if(viewType==BODY_OF_RELATED_MOVIE){
            View view=LayoutInflater.from(parent.getContext()).inflate(R.layout.view_video_detail_related,null);
            return new videoDetailRelatedViewHolder(view);
        }
        throw new IllegalArgumentException("invalid position");
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {

        if(position==0)
        {
            VideoDetailMovieHeadViewHolder h=(VideoDetailMovieHeadViewHolder)holder;
            return;
        }

        if(items.size()>0)
        {
            if(items.size()>=position){
                VideoDetailViewHolder h=(VideoDetailViewHolder)holder;
                h.setVideoDetailData(items.get(position - 1));
                h.setOnItemClickListener(m1Listener);

                return;
            }
            position-=items.size();
        }

        if(position==1)
        {
            VideoDetailRelatedHeadViewHolder h=(VideoDetailRelatedHeadViewHolder)holder;
            return;
        }
        position-=1;
        if(items2.size()>0)
        {
            if(items2.size()>=position)
            {
                videoDetailRelatedViewHolder h=(videoDetailRelatedViewHolder)holder;
                h.setRelatedProductData(items2.get(position-1));
                h.setOnItemClickListener(mListener);
                h.setOnButtonClikcListenter(bListener);
                return;
            }
        }
        position-=items2.size();

    }

    @Override
    public int getItemCount() {
        return items.size()+items2.size()+2;

    }

    @Override
    public int getItemViewType(int position) {
        if(position==0)
        {
            return HEADER_OF_IN_MOVIE_TYPE;
        }
        //position--;
        if(items.size()>0)
        {
            if(items.size()>=position){
                return BODY_OF_ON_MOVIE_TYPE;
            }
            position-=items.size();
        }

        if(position==1)
        {
            return HEADER_OF_RELATED_MOVIE;
        }
        position-=1;
        if(items2.size()>0)
        {
            if(items2.size()>=position)
            {
                return BODY_OF_RELATED_MOVIE;
            }
            position-=items2.size();
        }

        throw new IllegalArgumentException("invalid position");
    }

    videoDetailRelatedViewHolder.OnItemClickListener mListener;

    public void setOnItemClicListener(videoDetailRelatedViewHolder.OnItemClickListener listener) {
        mListener = listener;
    }

    VideoDetailViewHolder.OnItemClickListener m1Listener;

    public void setOnItemClicListener(VideoDetailViewHolder.OnItemClickListener listener) {
        m1Listener = listener;
    }

     videoDetailRelatedViewHolder.OnButtonClickListener bListener;
    public void setOnButtonClickListener(videoDetailRelatedViewHolder.OnButtonClickListener listener)
    {
        bListener=listener;
    }

}
