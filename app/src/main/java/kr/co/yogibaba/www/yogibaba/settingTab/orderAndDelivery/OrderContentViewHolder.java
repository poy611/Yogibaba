package kr.co.yogibaba.www.yogibaba.settingTab.orderAndDelivery;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import kr.co.yogibaba.www.yogibaba.R;
import kr.co.yogibaba.www.yogibaba.data.order.OrderAndDeliveryData;
import kr.co.yogibaba.www.yogibaba.data.order.OrderDetailPurchaseItem;

/**
 * Created by jah on 2016-05-20.
 */
public class OrderContentViewHolder extends RecyclerView.ViewHolder {


    ImageView imageView;
    TextView productNameView, productPriceView, productOptionView, productCountView;

    OrderAndDeliveryData mData;
    OrderDetailPurchaseItem iData;


    public OrderContentViewHolder(View itemView) {
        super(itemView);
        imageView = (ImageView) itemView.findViewById(R.id.image_ordered);
        productNameView = (TextView) itemView.findViewById(R.id.product_name);
        productPriceView = (TextView) itemView.findViewById(R.id.product_price);
        productOptionView = (TextView) itemView.findViewById(R.id.product_option);
        productCountView = (TextView) itemView.findViewById(R.id.product_count);
    }

    public void setOrderContentData(OrderAndDeliveryData data){
        this.mData = data;
        Glide.with(imageView.getContext()).load(mData.getItem_image_url()).into(imageView);
        productNameView.setText(data.getItem_name());
        productPriceView.setText(String.valueOf(data.getOption_price()+data.getItem_price()));
        productOptionView.setText(data.getOption_name());
        productCountView.setText(String.valueOf(data.getItem_count()));
    }

    public void setOrderDetailContentData(OrderDetailPurchaseItem iData){
        this.iData = iData;
        Glide.with(imageView.getContext()).load(iData.getItem_image_url()).into(imageView);
        productNameView.setText(iData.getItem_name());
        productPriceView.setText(String.valueOf(iData.getOption_price()+iData.getItem_price()));
        productOptionView.setText(iData.getOption_name());
        productCountView.setText(String.valueOf(iData.getItem_count()));
    }
}
