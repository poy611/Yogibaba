package kr.co.yogibaba.www.yogibaba.search.shoppingsearch;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import kr.co.yogibaba.www.yogibaba.R;
import kr.co.yogibaba.www.yogibaba.data.SearchRecomData;
import kr.co.yogibaba.www.yogibaba.data.search.SearchItemRecom;

/**
 * Created by OhDaeKyoung on 2016. 5. 16..
 */
public class ShoppingSearchRecomViewHolder extends RecyclerView.ViewHolder {
    ImageView imageView;
    TextView textView;
    SearchItemRecom mData;
    public ShoppingSearchRecomViewHolder(View itemView) {
        super(itemView);

        textView=(TextView)itemView.findViewById(R.id.text_recom_search);

        imageView=(ImageView)itemView.findViewById(R.id.img_recom_search);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener!=null)
                {
                    mListener.onItemClick(v,mData);
                }
            }
        });
    }
    public void setSearchRecomData(SearchItemRecom data){
        mData=data;
        Glide.with(imageView.getContext()).load(data.getItem_image_url()).into(imageView);
        textView.setText(data.getItem_name());


    }
    public interface OnItemClickListener {
         void onItemClick(View view, SearchItemRecom product);
    }

    OnItemClickListener mListener;

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }
}
