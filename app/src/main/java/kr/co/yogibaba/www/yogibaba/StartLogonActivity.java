package kr.co.yogibaba.www.yogibaba;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;

import java.io.IOException;

import kr.co.yogibaba.www.yogibaba.data.basket.BasketResults;
import kr.co.yogibaba.www.yogibaba.manager.NetworkManagerOh;
import kr.co.yogibaba.www.yogibaba.manager.PropertyManager;
import kr.co.yogibaba.www.yogibaba.manager.TypefaceManager;
import kr.co.yogibaba.www.yogibaba.search.SearchActivity;
import kr.co.yogibaba.www.yogibaba.settingTab.basket.BasketMainActivity;
import kr.co.yogibaba.www.yogibaba.settingTab.likes.LikesMainActivity;
import kr.co.yogibaba.www.yogibaba.settingTab.orderAndDelivery.OrderAndDeliveryMainActivity;
import kr.co.yogibaba.www.yogibaba.settingTab.setting.SettingDetailActivity;
import kr.co.yogibaba.www.yogibaba.shoppingtab.ElectronicgoodsTabFragment;
import kr.co.yogibaba.www.yogibaba.shoppingtab.FurnitureTabFragment;
import kr.co.yogibaba.www.yogibaba.shoppingtab.LightingTabFragment;
import kr.co.yogibaba.www.yogibaba.shoppingtab.NewFragmentFragment;
import kr.co.yogibaba.www.yogibaba.shoppingtab.PropsTabFragment;
import kr.co.yogibaba.www.yogibaba.videoTab.VideoListFragment;
import okhttp3.Request;

public class StartLogonActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    String user_id;
    TextView text_basket_count, text_nav_id;
    RoundedImageView nav_image_profile;
    public static Activity activity;
    AppCompatDelegate mDelegate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //글꼴테스트
        if (Build.VERSION.SDK_INT < 11){
            getLayoutInflater().setFactory(this);
        } else {
            getLayoutInflater().setFactory2(this);
        }
        mDelegate = getDelegate();
        //글꼴테스트
        super.onCreate(savedInstanceState);

//        FacebookSdk.sdkInitialize(this);
//        setContentView(R.layout.activity_start);
        setContentView(R.layout.activity_start_logon);
        activity = StartLogonActivity.this;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        user_id= PropertyManager.getInstance().getUserId();
        getSupportActionBar().setTitle(null);



        setBasketData();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_logon);
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

//        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view_logon);
        navigationView.setItemIconTintList(null);
        navigationView.setNavigationItemSelectedListener(this);

        View nav_header_item = navigationView.getHeaderView(0);
        ImageView basket = (ImageView)nav_header_item.findViewById(R.id.nav_image_basket);
        basket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(StartLogonActivity.this, BasketMainActivity.class);
                startActivity(intent);

            }
        });

        ImageView likes = (ImageView)nav_header_item.findViewById(R.id.nav_image_likes);
        likes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(StartLogonActivity.this, LikesMainActivity.class);
                startActivity(intent);
            }
        });

        text_basket_count = (TextView)nav_header_item.findViewById(R.id.text_basket_count);
        nav_image_profile = (RoundedImageView)nav_header_item.findViewById(R.id.nav_image_profile);
        text_nav_id = (TextView)nav_header_item.findViewById(R.id.text_nav_id);


    }

    private void setBasketData() {

        NetworkManagerOh.getInstance().getBasketList(this, user_id
                , new NetworkManagerOh.OnResultListener<BasketResults>() {
                    @Override
                    public void onSuccess(Request request, BasketResults result) {

                        if (result.getSuccess() == 1) {
                            //         bAdapter.clear();
                            //         bAdapter.addall(result.result.getUser_basket());
                            text_basket_count.setText(String.valueOf(result.result.getUser_basket().size()));
//                            text_basket_count.setText("왜안되냐");
                            text_nav_id.setText(result.result.getUser_name());
                            Uri uri = Uri.parse(result.result.getUser_profile());
                            Glide.with(nav_image_profile.getContext())
                                    .load(uri)
                                    .asBitmap()
                                    .into(nav_image_profile);
                        }
                    }

                    @Override
                    public void onFail(Request request, IOException exception) {
                        Toast.makeText(StartLogonActivity.this, "네트워크 에러", Toast.LENGTH_SHORT).show();
                    }
                });
    }



    @Override
    public void onBackPressed() {
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_logon);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.search_yogi) {//검색버튼임
            Intent intent=new Intent(StartLogonActivity.this, SearchActivity.class);
            startActivity(intent);// 검색 액티비티를 실행하라
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    public void onLoginButtonSelected(){
      /*  CallbackManager callbackManager;
        LoginManager loginManager;
        AccessTokenTracker tokenTracker;
        LoginButton loginButton;
        loginButton = (LoginButton)findViewById(R.id.login_button);
        callbackManager = CallbackManager.Factory.create();
        loginManager = LoginManager.getInstance();

        assert loginButton != null;
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginCheckResult>() {
            @Override
            public void onSuccess(LoginCheckResult loginResult) {
                // App code
            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
            }
        });
*/

    }
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();



        if(id== R.id.nav_home){
            getSupportFragmentManager().beginTransaction().replace(R.id.container, new VideoListFragment()).commit();
        }
        else if (id == R.id.nav_new_product) {
            getSupportFragmentManager().beginTransaction().replace(R.id.container, new NewFragmentFragment()).commit();
            // Handle the camera action
        } else if (id == R.id.nav_furniture) {
            getSupportFragmentManager().beginTransaction().replace(R.id.container, new FurnitureTabFragment()).commit();

        } else if (id == R.id.nav_electronic) {
            getSupportFragmentManager().beginTransaction().replace(R.id.container, new ElectronicgoodsTabFragment()).commit();

        } else if (id == R.id.nav_lightning) {
            getSupportFragmentManager().beginTransaction().replace(R.id.container, new LightingTabFragment()).commit();

        } else if (id == R.id.nav_props) {
            getSupportFragmentManager().beginTransaction().replace(R.id.container, new PropsTabFragment()).commit();

        }
        else if (id == R.id.nav_order_and_delivery) {
            //주문내역 / 배송조회
            Intent intent = new Intent(getApplicationContext(), OrderAndDeliveryMainActivity.class);
            startActivity(intent);
        }
        else if (id == R.id.nav_setting) {

            getSupportFragmentManager().beginTransaction().replace(R.id.container, new PropsTabFragment()).commit();
            Intent intent = new Intent(getApplicationContext(), SettingDetailActivity.class);
            startActivity(intent);
        }




//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_logon);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        setBasketData();
        getSupportFragmentManager().beginTransaction().replace(R.id.container, new VideoListFragment()).commit();

    }
    public View setCustomFont(View view, String name, Context context, AttributeSet attrs){
        if(view == null){
            if(name.equals("TextView")){
                view = new TextView(context,attrs);
            }
        }
        if (view != null && view instanceof TextView){
            TypedArray typedArray = context.obtainStyledAttributes(attrs, new int[]{android.R.attr.fontFamily});
            String fontFamily = typedArray.getString(0);
            typedArray.recycle();
            Typeface typeface = TypefaceManager.getInstance().getTypeface(context,fontFamily);
            if (typeface != null){
                TextView textView = (TextView)view;
                textView.setTypeface(typeface);
            }
        }
        return view;
    }

    @Override
    public View onCreateView(String name, Context context, AttributeSet attrs) {
        View view = super.onCreateView(name, context, attrs);
        if(Build.VERSION.SDK_INT < 11){
            if (view == null){
                view = mDelegate.createView(null, name, context, attrs);
                view = setCustomFont(view, name, context, attrs);
            }
        }
        return view;
    }

    @Override
    public View onCreateView(View parent, String name, Context context, AttributeSet attrs) {
        View view  = super.onCreateView(parent, name, context, attrs);
        if (view == null){
            view = mDelegate.createView(parent, name, context, attrs);
        }
        view = setCustomFont(view, name, context, attrs);

        return  view;
    }
}
