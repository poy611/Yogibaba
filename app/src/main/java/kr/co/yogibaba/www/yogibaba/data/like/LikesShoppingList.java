package kr.co.yogibaba.www.yogibaba.data.like;

/**
 * Created by jah on 2016-05-30.
 */
public class LikesShoppingList {
    int edit_Type = 0;

    public int getEdit_Type() {
        return edit_Type;
    }

    public void setEdit_Type(int edit_Type) {
        this.edit_Type = edit_Type;
    }

    public int item_id;
    public String item_name;
    public String item_brand;
    public int item_price;
    public  int option_price;
    public String item_image_url;

    public int getItem_id() {
        return item_id;
    }

    public void setItem_id(int item_id) {
        this.item_id = item_id;
    }

    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public String getItem_brand() {
        return item_brand;
    }

    public void setItem_brand(String item_brand) {
        this.item_brand = item_brand;
    }

    public int getItem_price() {
        return item_price;
    }

    public void setItem_price(int item_price) {
        this.item_price = item_price;
    }

    public int getOption_price() {
        return option_price;
    }

    public void setOption_price(int option_price) {
        this.option_price = option_price;
    }

    public String getItem_image_url() {
        return item_image_url;
    }

    public void setItem_image_url(String item_image_url) {
        this.item_image_url = item_image_url;
    }
}
