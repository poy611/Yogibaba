package kr.co.yogibaba.www.yogibaba.shoppingtab.lighting;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import kr.co.yogibaba.www.yogibaba.R;
import kr.co.yogibaba.www.yogibaba.data.ShoppingDataByCategory;
import kr.co.yogibaba.www.yogibaba.data.ShoppingViewPageData;


/**
 * Created by OhDaeKyoung on 2016. 5. 13..
 */
public class LightingAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    List<ShoppingDataByCategory> items = new ArrayList<>();

    public static final int VIEW_TYPE_RECYCLER = 2;

    public void add(ShoppingDataByCategory item) {
        items.add(item);
        notifyDataSetChanged();
    }



    public void addAll(List<ShoppingDataByCategory> items) {
        this.items.addAll(items);
        notifyDataSetChanged();
    }



    public void clear() {
        items.clear();
        notifyDataSetChanged();
    }



    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        LightingViewHolder h = (LightingViewHolder) holder;
        h.setShoppingDataByCategory(items.get(position));
        h.setOnItemClickListener(mListener);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_RECYCLER) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_shopping_item_by_category, null);
            return new LightingViewHolder(view);
        }

        throw new IllegalArgumentException("invalid position");
    }
    @Override
    public int getItemViewType(int position) {

        return VIEW_TYPE_RECYCLER;

    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    LightingViewHolder.OnItemClickListener mListener;

    public void setOnItemClicListener(LightingViewHolder.OnItemClickListener listener) {
        mListener = listener;
    }
}