package kr.co.yogibaba.www.yogibaba;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import kr.co.yogibaba.www.yogibaba.login.LoginActivity;
import kr.co.yogibaba.www.yogibaba.manager.TypefaceManager;
import kr.co.yogibaba.www.yogibaba.search.SearchActivity;
import kr.co.yogibaba.www.yogibaba.settingTab.orderAndDelivery.OrderAndDeliveryMainActivity;
import kr.co.yogibaba.www.yogibaba.settingTab.setting.SettingDetailActivity;
import kr.co.yogibaba.www.yogibaba.shoppingtab.ElectronicgoodsTabFragment;
import kr.co.yogibaba.www.yogibaba.shoppingtab.FurnitureTabFragment;
import kr.co.yogibaba.www.yogibaba.shoppingtab.LightingTabFragment;
import kr.co.yogibaba.www.yogibaba.shoppingtab.NewFragmentFragment;
import kr.co.yogibaba.www.yogibaba.shoppingtab.PropsTabFragment;
import kr.co.yogibaba.www.yogibaba.videoTab.VideoListFragment;

public class StartActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    Button logon_btn;
    AppCompatDelegate mDelegate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //글꼴테스트
        if (Build.VERSION.SDK_INT < 11){
            getLayoutInflater().setFactory(this);
        } else {
            getLayoutInflater().setFactory2(this);
        }
        mDelegate = getDelegate();
        //글꼴테스트
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setItemIconTintList(null);
        navigationView.setNavigationItemSelectedListener(this);
        View nav_header_item = navigationView.getHeaderView(0);
        logon_btn = (Button)nav_header_item.findViewById(R.id.btn_login);

        logon_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
                StartActivity.this.finish();
            }
        });
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.search_yogi) {//검색버튼임
            Intent intent=new Intent(StartActivity.this, SearchActivity.class);
            startActivity(intent);// 검색 액티비티를 실행하라
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getSupportFragmentManager().beginTransaction().replace(R.id.container, new VideoListFragment()).commit();

    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if(id==R.id.nav_home){
            getSupportFragmentManager().beginTransaction().replace(R.id.container, new VideoListFragment()).commit();
        }
        else if (id == R.id.nav_new_product) {
            getSupportFragmentManager().beginTransaction().replace(R.id.container, new NewFragmentFragment()).commit();
            // Handle the camera action
        } else if (id == R.id.nav_furniture) {
            getSupportFragmentManager().beginTransaction().replace(R.id.container, new FurnitureTabFragment()).commit();

        } else if (id == R.id.nav_electronic) {
            getSupportFragmentManager().beginTransaction().replace(R.id.container, new ElectronicgoodsTabFragment()).commit();

        } else if (id == R.id.nav_lightning) {
            getSupportFragmentManager().beginTransaction().replace(R.id.container, new LightingTabFragment()).commit();

        } else if (id == R.id.nav_props) {
            getSupportFragmentManager().beginTransaction().replace(R.id.container, new PropsTabFragment()).commit();

        }
        else if (id == R.id.nav_order_and_delivery) {
            //주문내역 / 배송조회

            Intent intent = new Intent(getApplicationContext(), OrderAndDeliveryMainActivity.class);
            startActivity(intent);
        }
        else if (id == R.id.nav_setting) {
            //주문내역 / 배송조회
            Intent intent = new Intent(getApplicationContext(), SettingDetailActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    public View setCustomFont(View view, String name, Context context, AttributeSet attrs){
        if(view == null){
            if(name.equals("TextView")){
                view = new TextView(context,attrs);
            }
        }
        if (view != null && view instanceof TextView){
            TypedArray typedArray = context.obtainStyledAttributes(attrs, new int[]{android.R.attr.fontFamily});
            String fontFamily = typedArray.getString(0);
            typedArray.recycle();
            Typeface typeface = TypefaceManager.getInstance().getTypeface(context,fontFamily);
            if (typeface != null){
                TextView textView = (TextView)view;
                textView.setTypeface(typeface);
            }
        }
        return view;
    }

    @Override
    public View onCreateView(String name, Context context, AttributeSet attrs) {
        View view = super.onCreateView(name, context, attrs);
        if(Build.VERSION.SDK_INT < 11){
            if (view == null){
                view = mDelegate.createView(null, name, context, attrs);
                view = setCustomFont(view, name, context, attrs);
            }
        }
        return view;
    }

    @Override
    public View onCreateView(View parent, String name, Context context, AttributeSet attrs) {
        View view  = super.onCreateView(parent, name, context, attrs);
        if (view == null){
            view = mDelegate.createView(parent, name, context, attrs);
        }
        view = setCustomFont(view, name, context, attrs);

        return  view;
    }
}
