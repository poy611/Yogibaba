package kr.co.yogibaba.www.yogibaba.data.etc;

/**
 * Created by OhDaeKyoung on 2016. 5. 25..
 */
public class ClickResult {
    String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
