package kr.co.yogibaba.www.yogibaba.videoTab.videodetail;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import kr.co.yogibaba.www.yogibaba.R;
import kr.co.yogibaba.www.yogibaba.data.VideoDetailData;
import kr.co.yogibaba.www.yogibaba.data.videodetail.ItemInMovie;

/**
 * Created by OhDaeKyoung on 2016. 5. 16..
 */
public class VideoDetailViewHolder extends RecyclerView.ViewHolder {
    ImageView productImg;
    TextView productTitle,productPrice,manufator;
    ItemInMovie data;

    public interface OnItemClickListener {
        void onItemClick(View view, ItemInMovie product);
    }

    OnItemClickListener mListener;

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }




    public VideoDetailViewHolder(View itemView) {
        super(itemView);
        Context context=itemView.getContext();

        productImg=(ImageView)itemView.findViewById(R.id.img_video_detail_movie_in);

        productTitle=(TextView)itemView.findViewById(R.id.text_video_detail_movie_title);

        productPrice=(TextView)itemView.findViewById(R.id.text_video_detail_movie_price);

        manufator=(TextView)itemView.findViewById(R.id.text_video_detail_manu);
       itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener!=null)
                {
                    mListener.onItemClick(v,data);
                }
            }
        });
    }
    public void setVideoDetailData(ItemInMovie data)
    {
        this.data=data;
        Glide.with(productImg.getContext()).load(data.getItem_image_url()).into(productImg);

        productPrice.setText(data.getItem_price()+"won");
        productTitle.setText(data.getItem_name());
        manufator.setText(data.getItem_brand());
    }
}
