package kr.co.yogibaba.www.yogibaba.data;

/**
 * Created by Tacademy on 2016-05-12.
 */
public class ShoppingDataByCategory {

    int item_id;
    String item_name;
    String item_brand;
    int item_price;
    String item_image_url;
    int item_like_num;
    int review_num;

    public int getItem_like_num() {
        return item_like_num;
    }

    public void setItem_like_num(int item_like_num) {
        this.item_like_num = item_like_num;
    }

    public int getReview_num() {
        return review_num;
    }

    public void setReview_num(int review_num) {
        this.review_num = review_num;
    }

    public int getItem_id() {
        return item_id;
    }

    public void setItem_id(int item_id) {
        this.item_id = item_id;
    }

    public String getItem_image_url() {
        return item_image_url;
    }

    public void setItem_image_url(String item_image_url) {
        this.item_image_url = item_image_url;
    }

    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public int getItem_price() {
        return item_price;
    }

    public void setItem_price(int item_price) {
        this.item_price = item_price;
    }

    public String getItem_brand() {
        return item_brand;
    }

    public void setItem_brand(String item_brand) {
        this.item_brand = item_brand;
    }
}
