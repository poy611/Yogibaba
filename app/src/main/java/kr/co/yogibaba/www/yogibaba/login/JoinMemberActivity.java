package kr.co.yogibaba.www.yogibaba.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import kr.co.yogibaba.www.yogibaba.R;

public class JoinMemberActivity extends AppCompatActivity {

    EditText edit_member_email, edit_member_phone_number, edit_member_addr;
    Button btn_join;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_join_member);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        edit_member_email=(EditText)findViewById(R.id.edit_member_email); //이메일
        edit_member_phone_number=(EditText)findViewById(R.id.edit_member_phone_number); //전화번호
        edit_member_addr=(EditText)findViewById(R.id.edit_member_addr); //주소

        btn_join = (Button)findViewById(R.id.btn_join);

        btn_join.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(JoinMemberActivity.this, RegisterMembershipActivity.class);
                i.putExtra(RegisterMembershipActivity.MEMBER_EMIAL, edit_member_email.getText().toString());
                i.putExtra(RegisterMembershipActivity.MEMBER_PHONE, edit_member_phone_number.getText().toString());
                i.putExtra(RegisterMembershipActivity.MEMBER_ADDR, edit_member_addr.getText().toString());
                startActivity(i);
                finish();
            }
        });
    }





}
