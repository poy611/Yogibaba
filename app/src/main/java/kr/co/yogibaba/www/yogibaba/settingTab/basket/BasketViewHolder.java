package kr.co.yogibaba.www.yogibaba.settingTab.basket;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import kr.co.yogibaba.www.yogibaba.R;

import kr.co.yogibaba.www.yogibaba.data.basket.Basket;

/**
 * Created by jah on 2016-05-19.
 */
public class BasketViewHolder extends RecyclerView.ViewHolder {

    ImageView imageView;
    TextView productNameView, productPriceView, productOptionView, productCountView;
    Button btn_change;
    Button btn_delete;

    Basket data;

    public interface OnChangeButtonClickListener{
        void onChangeButtonClick(View v, Basket product);
    }
    OnChangeButtonClickListener mListener;
    public void setOnChangeButtonClickListener(OnChangeButtonClickListener listener)
    {
        mListener=listener;
    }

    public BasketViewHolder(View itemView) {
        super(itemView);
        imageView = (ImageView)itemView.findViewById(R.id.image_ordered);
        productNameView = (TextView)itemView.findViewById(R.id.product_name);
        productPriceView = (TextView)itemView.findViewById(R.id.product_price);
        productOptionView = (TextView)itemView.findViewById(R.id.product_option);
        productCountView = (TextView)itemView.findViewById(R.id.product_count);
        btn_change = (Button)itemView.findViewById(R.id.btn_change);
        btn_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener!=null){
                    mListener.onChangeButtonClick(v,data);
                }
            }
        });
        btn_delete = (Button)itemView.findViewById(R.id.btn_basket_delete);


    }

    public void setBasketData(Basket data){
        this.data = data;
        Glide.with(imageView.getContext()).load(data.getItem_image_url()).into(imageView);
        productNameView.setText(data.getItem_name());
        productPriceView.setText(data.getItem_price()+"won");
        productOptionView.setText(data.getOption_name());
        productCountView.setText(data.getItem_count()+"개");

    }
}


