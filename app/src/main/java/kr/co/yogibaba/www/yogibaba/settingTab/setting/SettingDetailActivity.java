package kr.co.yogibaba.www.yogibaba.settingTab.setting;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import kr.co.yogibaba.www.yogibaba.R;
import kr.co.yogibaba.www.yogibaba.StartActivity;
import kr.co.yogibaba.www.yogibaba.StartLogonActivity;
import kr.co.yogibaba.www.yogibaba.gcm.RegistrationIntentService;
import kr.co.yogibaba.www.yogibaba.manager.PropertyManager;
import kr.co.yogibaba.www.yogibaba.manager.TypefaceManager;

public class SettingDetailActivity extends AppCompatActivity {

    View view;
    TextView push_alarm;
    TextView terms_of_service;
    TextView personal_info_policy;
    TextView app_info;
    TextView logout;
    Switch switch_alarm;

    AppCompatDelegate mDelegate;


    class Gcmpatch extends AsyncTask<String,String,String> {
        @Override
        protected String doInBackground(String... params) {

            try {

                RegistrationIntentService.patchScribeTopics(SettingDetailActivity.this, PropertyManager.getInstance().getRegistrationToken());
                PropertyManager.getInstance().setIsGcm(true);

            }catch (Exception e)
            {
                //Toast.makeText(SettingDetailActivity.this,e.toString(),Toast.LENGTH_SHORT).show();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
    }
    class GcmDispatch extends AsyncTask<String,String,String>{
        @Override
        protected String doInBackground(String... params) {

            try {
                RegistrationIntentService.dispatchTopics(SettingDetailActivity.this, PropertyManager.getInstance().getRegistrationToken());
                PropertyManager.getInstance().setIsGcm(false);
            }catch (Exception e)
            {
                //Toast.makeText(SettingDetailActivity.this,e.toString(), Toast.LENGTH_SHORT).show();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //글꼴테스트
        if (Build.VERSION.SDK_INT < 11){
            getLayoutInflater().setFactory(this);
        } else {
            getLayoutInflater().setFactory2(this);
        }
        mDelegate = getDelegate();
        //글꼴테스트

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final StartLogonActivity startLoginActivity = (StartLogonActivity) StartLogonActivity.activity;
        //   final LoginActivity loginActivity = (LoginActivity) LoginActivity.loginActivity;
        terms_of_service = (TextView)findViewById(R.id.text_terms_of_service);

        personal_info_policy = (TextView)findViewById(R.id.text_personal_information_policy);
//        personal_info_policy.setTypeface(Typeface.createFromAsset(getAssets(),"ENBRUSH.TTF"));
//        personal_info_policy.setTypeface(Typeface.createFromAsset(getAssets(),"NotoSansCJKkr-Bold_1.otf"));
//        personal_info_policy.setTypeface(Typeface.createFromAsset(getAssets(),"NotoSansCJKkr-Regular_0.otf"));
//        personal_info_policy.setTypeface(Typeface.createFromAsset(getAssets(),"NotoSansCJKkr-Medium_1.otf"));

        app_info =  (TextView)findViewById(R.id.text_app_info);

        logout = (TextView)findViewById(R.id.text_logout);
        String user_id=PropertyManager.getInstance().getUserId();
        if(!PropertyManager.getInstance().isLogin())
        {
            logout.setVisibility(View.GONE);
        }
        else{
            logout.setVisibility(View.VISIBLE);
        }
        switch_alarm = (Switch)findViewById(R.id.switch_alarm);

        switch_alarm.setChecked(PropertyManager.getInstance().getIsGcm());

        switch_alarm.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    Gcmpatch gcmpatch=new Gcmpatch();
                    gcmpatch.execute(PropertyManager.getInstance().getRegistrationToken());

                }
                else{
                    GcmDispatch gcmDispatch=new GcmDispatch();
                    gcmDispatch.execute(PropertyManager.getInstance().getRegistrationToken());
                }
            }
        });

        terms_of_service.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), TermsOfServiceActivity.class);
                startActivity(intent);
            }
        });
        personal_info_policy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), PersonalInfoPolicyActivity.class);
                startActivity(intent);
            }
        });
        app_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), AppInfoActivity.class);
                startActivity(intent);
            }
        });
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // sharedpreference false
                PropertyManager.getInstance().setLogin(false);
                PropertyManager.getInstance().setUserId("");
//                loginActivity.setLogout();
                Intent intent = new Intent(getBaseContext(), StartActivity.class);
                startActivity(intent);
                SettingDetailActivity.this.finish();
                startLoginActivity.finish();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public View setCustomFont(View view, String name, Context context, AttributeSet attrs){
        if(view == null){
            if(name.equals("TextView")){
                view = new TextView(context,attrs);
            }
        }
        if (view != null && view instanceof TextView){
            TypedArray typedArray = context.obtainStyledAttributes(attrs, new int[]{android.R.attr.fontFamily});
            String fontFamily = typedArray.getString(0);
            typedArray.recycle();
            Typeface typeface = TypefaceManager.getInstance().getTypeface(context,fontFamily);
            if (typeface != null){
                TextView textView = (TextView)view;
                textView.setTypeface(typeface);
            }
        }
        return view;
    }

    @Override
    public View onCreateView(String name, Context context, AttributeSet attrs) {
        View view = super.onCreateView(name, context, attrs);
        if(Build.VERSION.SDK_INT < 11){
            if (view == null){
                view = mDelegate.createView(null, name, context, attrs);
                view = setCustomFont(view, name, context, attrs);
            }
        }
        return view;
    }

    @Override
    public View onCreateView(View parent, String name, Context context, AttributeSet attrs) {
        View view  = super.onCreateView(parent, name, context, attrs);
        if (view == null){
            view = mDelegate.createView(parent, name, context, attrs);
        }
        view = setCustomFont(view, name, context, attrs);

        return  view;
    }
}