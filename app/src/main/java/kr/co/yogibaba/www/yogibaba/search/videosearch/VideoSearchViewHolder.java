package kr.co.yogibaba.www.yogibaba.search.videosearch;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import kr.co.yogibaba.www.yogibaba.R;
import kr.co.yogibaba.www.yogibaba.data.search.SearchVideo;
import kr.co.yogibaba.www.yogibaba.facebook.FacebookActivity;
import kr.co.yogibaba.www.yogibaba.manager.PropertyManager;


/**
 * Created by OhDaeKyoung on 2016. 5. 16..
 */
public class VideoSearchViewHolder extends RecyclerView.ViewHolder {
    ImageView imageView;
    TextView videoTitle,videoViewCount,likesNum;

    SearchVideo mData;
    ImageView btn_pub,btn_close_publ,btn_clip,btn_kakao_publ,btn_facebook_publ;
    FacebookActivity activity;
    public interface OnButtonClickListener{
        void onButtonClick(View view,SearchVideo product);
    }
    OnButtonClickListener bListener;

    public void setOnButtonClickListener(OnButtonClickListener listener){
        bListener=listener;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, SearchVideo product);
    }

    OnItemClickListener mListener;

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    public VideoSearchViewHolder(View itemView) {
        super(itemView);
        final Context context=itemView.getContext();

        btn_pub=(ImageView)itemView.findViewById(R.id.btn_pub);

        btn_close_publ=(ImageView)itemView.findViewById(R.id.btn_close_publ);
        btn_clip=(ImageView)itemView.findViewById(R.id.btn_clip);
        btn_kakao_publ=(ImageView)itemView.findViewById(R.id.btn_kakao_publ);
        btn_facebook_publ=(ImageView)itemView.findViewById(R.id.btn_facebook_publ);

        btn_pub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Animation slideUp = AnimationUtils.loadAnimation(context, R.anim.slide_up);
                Animation slideDown = AnimationUtils.loadAnimation(context, R.anim.slide_down);

                btn_pub.startAnimation(slideDown);
                btn_pub.setVisibility(View.GONE);


                btn_close_publ.startAnimation(slideUp);
                btn_close_publ.setVisibility(View.VISIBLE);

                btn_clip.startAnimation(slideUp);
                btn_clip.setVisibility(View.VISIBLE);

                btn_kakao_publ.startAnimation(slideUp);
                btn_kakao_publ.setVisibility(View.VISIBLE);

                btn_facebook_publ.startAnimation(slideUp);
                btn_facebook_publ.setVisibility(View.VISIBLE);
            }
        });
        btn_close_publ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Animation slideUp = AnimationUtils.loadAnimation(context, R.anim.slide_up);
                Animation slideDown = AnimationUtils.loadAnimation(context, R.anim.slide_down);


                btn_close_publ.startAnimation(slideDown);
                btn_close_publ.setVisibility(View.GONE);

                btn_clip.startAnimation(slideDown);
                btn_clip.setVisibility(View.GONE);

                btn_kakao_publ.startAnimation(slideDown);
                btn_kakao_publ.setVisibility(View.GONE);

                btn_facebook_publ.startAnimation(slideDown);
                btn_facebook_publ.setVisibility(View.GONE);
                btn_pub.startAnimation(slideUp);
                btn_pub.setVisibility(View.VISIBLE);

            }
        });
        btn_facebook_publ.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (PropertyManager.getInstance().isLogin()){
                    Toast.makeText(context,"페이스북으로 요기바바 공유하기",Toast.LENGTH_SHORT).show();
                    activity = new FacebookActivity();
                    activity.post();
                }
                else
                {
                    Toast.makeText(context,"페이스북 로그인이 필요합니다",Toast.LENGTH_LONG).show();
                }
            }
        });

        btn_clip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context,"추가 공유 서비스 준비중",Toast.LENGTH_LONG).show();
            }
        });

        btn_kakao_publ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context,"카카오톡 서비스 준비중",Toast.LENGTH_LONG).show();
            }
        });

        imageView=(ImageView)itemView.findViewById(R.id.imgview_video_search);

        videoTitle=(TextView)itemView.findViewById(R.id.text_video_search_title);
        videoViewCount=(TextView)itemView.findViewById(R.id.text_video_search_view_count);
        likesNum=(TextView)itemView.findViewById(R.id.likes_num);


        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener!=null) {
                    mListener.onItemClick(v, mData);
                }
            }
        });
    }
    public void setVideoListData(SearchVideo data){
        mData=data;
        Glide.with(imageView.getContext()).load(data.getMovie_thumbnail()).into(imageView);
        videoTitle.setText(data.getMovie_title());
        videoViewCount.setText("조회수 : " + data.getMovie_hit());
        likesNum.setText(""+data.getMovie_like_num());
    }

}
