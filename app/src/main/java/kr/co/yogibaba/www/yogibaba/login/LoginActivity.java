package kr.co.yogibaba.www.yogibaba.login;//package kr.co.yogibaba.www.yogibaba.login;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;

import kr.co.yogibaba.www.yogibaba.R;
import kr.co.yogibaba.www.yogibaba.StartActivity;
import kr.co.yogibaba.www.yogibaba.StartLogonActivity;

import kr.co.yogibaba.www.yogibaba.data.login.LoginCheckSuccess;
import kr.co.yogibaba.www.yogibaba.etc.PolicyActivity;
import kr.co.yogibaba.www.yogibaba.manager.NetworkManagerJH;
import kr.co.yogibaba.www.yogibaba.manager.PropertyManager;
import okhttp3.Request;

public class LoginActivity extends AppCompatActivity {

    CallbackManager callbackManager;
    LoginManager mLoginManager;
    AccessTokenTracker mTracker;
    public static Activity loginActivity;

    Button btn,btn_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        loginActivity = LoginActivity.this;
        callbackManager = CallbackManager.Factory.create();
        mLoginManager = LoginManager.getInstance();
        btn = (Button) findViewById(R.id.login_button);
        btn_back = (Button)findViewById(R.id.back_button);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, StartActivity.class);
                startActivity(intent);
                LoginActivity.this.finish();
            }
        });
        btn.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {

                if (!isLogin()) {
                    mLoginManager.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                        @Override
                        public void onSuccess(LoginResult loginResult) {

                            AccessToken token = AccessToken.getCurrentAccessToken();
                            NetworkManagerJH.getInstance().setLoginCheck(this, token.getToken(), new NetworkManagerJH.OnResultListener<LoginCheckSuccess>() {
                                @Override
                                public void onSuccess(Request request, LoginCheckSuccess result) {
                                    PropertyManager.getInstance().setUserId(""+result.result.getUser_id());
                                    Toast.makeText(LoginActivity.this, result.result.getMessage(), Toast.LENGTH_SHORT).show();

                                    //0이면 기존회원
                                    //1이면 신규회원

                                    if (result.getSuccess() == 0){
                                        //기존회원 STARTLOGONACTIVITY
                                        PropertyManager.getInstance().setLogin(true);
                                        Intent intent = new Intent(LoginActivity.this, StartLogonActivity.class);
                                        startActivity(intent);
                                        LoginActivity.this.finish();
                                    }
                                    else{
                                        //1이므로 신규회원가입
                                        Intent intent = new Intent(LoginActivity.this, PolicyActivity.class);
                                        startActivity(intent);
                                        LoginActivity.this.finish();
                                    }
                                }

                                @Override
                                public void onFail(Request request, IOException exception) {
                                    Toast.makeText(LoginActivity.this, "exception : " + exception.getMessage(), Toast.LENGTH_LONG).show();
                                }
                            });
                        }

                        @Override
                        public void onCancel() {
                            Toast.makeText(LoginActivity.this, "login cancel", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onError(FacebookException error) {
                            Toast.makeText(LoginActivity.this, "login fail", Toast.LENGTH_SHORT).show();
                        }
                    });
                    mLoginManager.logInWithReadPermissions(LoginActivity.this, Arrays.asList("email"));
                    mLoginManager.logInWithPublishPermissions(LoginActivity.this, Arrays.asList("publish_actions"));
//                    mLoginManager.getLoginBehavior();
//                    LoginActivity.this.finish();
                } else {
                    mLoginManager.logOut();
                }

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(LoginActivity.this, StartActivity.class);
        startActivity(intent);
        LoginActivity.this.finish();
    }

    public void setLogout(){
        mLoginManager.logOut();
    }


    private boolean isLogin() {
        AccessToken token = AccessToken.getCurrentAccessToken();
        return token != null;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
