package kr.co.yogibaba.www.yogibaba.etc;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import kr.co.yogibaba.www.yogibaba.R;
import kr.co.yogibaba.www.yogibaba.data.basket.BasketItem;
import kr.co.yogibaba.www.yogibaba.data.basket.CustomBasket;
import kr.co.yogibaba.www.yogibaba.data.etc.Click;
import kr.co.yogibaba.www.yogibaba.data.shoppingdetail.ItemOptions;
import kr.co.yogibaba.www.yogibaba.manager.NetworkManagerOh;
import kr.co.yogibaba.www.yogibaba.settingTab.basket.BasketMainActivity;
import kr.co.yogibaba.www.yogibaba.shoppingtab.productbuy.ProductBuyActivity;
import okhttp3.Request;

public class CustomEditDialog extends Dialog {

    private TextView count;
    private Button mLeftButton;
    private Button mRightButton;
    private Spinner spinner;
    private ImageView up;
    private ImageView down;


    private int item_id;
    private String user_id;

    public interface onItemClickListener{
        void onItemClick(Context context,CustomBasket product);
    }
    onItemClickListener listener;
    public void setOnItemClickListener(onItemClickListener onItemClickListener)
    {
        listener=onItemClickListener;
    }

    private Context context;
    private View.OnClickListener mLeftClickListener;
    private View.OnClickListener mRightClickListener;
    private List<BasketItem> itemOptions;
    CustomBasket product;

    private String itemOption;
    private int itemPrice;



    public CustomEditDialog(Context context, int item_id,
                        String user_id, List<BasketItem> itemOptions) {
        super(context, android.R.style.Theme_Translucent_NoTitleBar);
        this.itemOptions=itemOptions;
        this.context=context;
        this.item_id = item_id;
        this.user_id = user_id;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
        lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        lpWindow.dimAmount = 0.8f;
        getWindow().setAttributes(lpWindow);

        setContentView(R.layout.activity_custom_edit_dialog);

        product=new CustomBasket();
        product.setUser_id(user_id);
        product.setItem_id(item_id);

        count=(TextView)findViewById(R.id.text_count);
        mLeftButton=(Button)findViewById(R.id.btn_bucket);
        mRightButton=(Button)findViewById(R.id.btn_buy);
        up=(ImageView)findViewById(R.id.btn_up);
        up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int currentCount=Integer.parseInt(count.getText().toString());
                currentCount++;
                count.setText(Integer.toString(currentCount));
                product.setItem_count(currentCount);
            }
        });
        down=(ImageView)findViewById(R.id.btn_down);
        down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int currentCount=Integer.parseInt(count.getText().toString());
                if(currentCount>1) {
                    currentCount--;
                }
                product.setItem_count(currentCount);
                count.setText(Integer.toString(currentCount));
            }
        });
        spinner=(Spinner)findViewById(R.id.spinner_option);
        ArrayList<String> arrayList=new ArrayList<>();
        for(int i=0; i<itemOptions.size(); i++)
        {
            arrayList.add(itemOptions.get(i).getOption_name());
        }
        final ArrayAdapter<String> adapter=new ArrayAdapter<String>(getContext(),android.R.layout.simple_spinner_item,arrayList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(view.getContext(), adapter.getItem(position), Toast.LENGTH_SHORT).show();
                itemOption = adapter.getItem(position);
                itemPrice = itemOptions.get(position).getOption_price();
                product.setItem_otion(itemOption);
                product.setItem_price(itemPrice);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mLeftButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onItemClick(context, product);

                }
                dismiss();
            }
        });
        mRightButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

    }
}
