package kr.co.yogibaba.www.yogibaba.data.videodetail;

/**
 * Created by OhDaeKyoung on 2016. 5. 23..
 */
public class VideoDetailResults {
    int success;
    public VideoDetailResult result;

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }
}
