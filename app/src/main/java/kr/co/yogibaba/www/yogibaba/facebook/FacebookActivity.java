package kr.co.yogibaba.www.yogibaba.facebook;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.ShareApi;
import com.facebook.share.model.ShareContent;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.ShareMediaContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.model.ShareVideo;
import com.facebook.share.model.ShareVideoContent;
import com.facebook.share.widget.ShareButton;
import com.facebook.share.widget.ShareDialog;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;

import kr.co.yogibaba.www.yogibaba.R;
import kr.co.yogibaba.www.yogibaba.manager.PropertyManager;

public class FacebookActivity extends AppCompatActivity {
    CallbackManager callbackManager;
    LoginManager loginManager;
    ShareButton faceboButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_facebook);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        callbackManager = CallbackManager.Factory.create();
        loginManager = LoginManager.getInstance();
        faceboButton = (ShareButton)findViewById(R.id.facebook_btn);
    }

    public void post() {
        callbackManager = CallbackManager.Factory.create();
        loginManager = LoginManager.getInstance();
        AccessToken accessToken = AccessToken.getCurrentAccessToken();

        if (accessToken != null){
            if (accessToken.getPermissions().contains("publish_actions")){
                ShareLinkContent content = new ShareLinkContent.Builder()
                        .setContentUrl(Uri.parse("http://www.bjmedia.co.kr"))
                        .build();
                ShareApi.share(content,null);
            }
            return;
        }
        loginManager.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                post();
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });
        loginManager.logInWithPublishPermissions(this, Arrays.asList("publish_actions"));
    }

}
