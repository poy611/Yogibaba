package kr.co.yogibaba.www.yogibaba.data.search;

import java.util.List;

/**
 * Created by OhDaeKyoung on 2016. 5. 26..
 */
public class SearchItemResult {
    String message;
    List<SearchItem> items;

    public String getMessage() {
        return message;
    }

    public List<SearchItem> getItems() {
        return items;
    }
}
