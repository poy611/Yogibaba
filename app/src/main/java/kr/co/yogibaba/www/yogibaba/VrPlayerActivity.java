package kr.co.yogibaba.www.yogibaba;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.bumptech.glide.Glide;
import com.google.vr.sdk.widgets.video.VrVideoEventListener;
import com.google.vr.sdk.widgets.video.VrVideoView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import kr.co.yogibaba.www.yogibaba.data.vrplayer.VrPlayerRelatedData;
import kr.co.yogibaba.www.yogibaba.data.vrplayer.VrPlayerResult;
import kr.co.yogibaba.www.yogibaba.manager.NetworkManagerOh;
import kr.co.yogibaba.www.yogibaba.manager.TypefaceManager;
import kr.co.yogibaba.www.yogibaba.shoppingtab.shoppingdetail.ShoppingDetailActivity;
import okhttp3.Request;

public class VrPlayerActivity extends AppCompatActivity {

    public static final String RELATED_ITEM="relateditem";
    public static final String MOVIE_URL="movie_url";
    public static final String MOVIE_ID="movie_id";

    private static final String TAG = VrPlayerActivity.class.getSimpleName();

    String movie_url=null;
    int movie_id;
    int current_position=0;
    AppCompatDelegate mDelegate;
    boolean is_show=true;
    int count=0;
    /**
     * Preserve the video's state when rotating the phone.
     */
    private static final String STATE_IS_PAUSED = "isPaused";
    private static final String STATE_PROGRESS_TIME = "progressTime";
    /**
     * The video duration doesn't need to be preserved, but it is saved in this example. This allows
     * the seekBar to be configured during {@link #onRestoreInstanceState(Bundle)} rather than waiting
     * for the video to be reloaded and analyzed. This avoid UI jank.
     */
    private static final String STATE_VIDEO_DURATION = "videoDuration";

    /**
     * Arbitrary constants and variable to track load status. In this example, this variable should
     * only be accessed on the UI thread. In a real app, this variable would be code that performs
     * some UI actions when the video is fully loaded.
     */
    public static final int LOAD_VIDEO_STATUS_UNKNOWN = 0;
    public static final int LOAD_VIDEO_STATUS_SUCCESS = 1;
    public static final int LOAD_VIDEO_STATUS_ERROR = 2;

    private int loadVideoStatus = LOAD_VIDEO_STATUS_UNKNOWN;

    public int getLoadVideoStatus() {
        return loadVideoStatus;
    }

    /** Tracks the file to be loaded across the lifetime of this app. **/
    private Uri fileUri;
    private VideoLoaderTask backgroundVideoLoaderTask;

    /**
     * The video view and its custom UI elements.
     */
    private VrVideoView videoWidgetView;

    /**
     * Seeking UI & progress indicator. The seekBar's progress value represents milliseconds in the
     * video.
     */
    private SeekBar seekBar;
    // RecyclerView recyclerView;
    // VrPlayerAdapter mAdapter;
    // ArrayList<VideoDetailData> items;
    /**
     * By default, the video will start playing as soon as it is loaded. This can be changed by using
     * {@link VrVideoView#pauseVideo()} after loading the video.
     */
    private boolean isPaused = false;
    //  GridLayoutManager gridLayoutManager;
    ImageView btn,coackmark;
    List<VrPlayerRelatedData> movie_item;
    ViewFlipper viewFlipper;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        //글꼴테스트
        if (Build.VERSION.SDK_INT < 11){
            getLayoutInflater().setFactory(this);
        } else {
            getLayoutInflater().setFactory2(this);
        }
        mDelegate = getDelegate();
//글꼴테스트
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vr_player);
        //movie_url=getIntent().getStringExtra(MOVIE_URL);
        //movie_id=getIntent().getIntExtra(MOVIE_ID, 1);

        List<VrPlayerRelatedData> movie_item=new ArrayList<>();
        viewFlipper=(ViewFlipper)findViewById(R.id.viewflipper);
        Animation showIn= AnimationUtils.loadAnimation(this, android.R.anim.slide_in_left);
        viewFlipper.setInAnimation(showIn);
        seekBar = (SeekBar) findViewById(R.id.seek_bar);
        seekBar.setOnSeekBarChangeListener(new SeekBarListener());

    /*    gridLayoutManager=new GridLayoutManager(VrPlayerActivity.this,1);
        gridLayoutManager.setOrientation(GridLayoutManager.VERTICAL);
        recyclerView=(RecyclerView)findViewById(R.id.rv_vr);
        mAdapter=new VrPlayerAdapter();
        recyclerView.setLayoutManager(gridLayoutManager);
        //recyclerView.setLayoutManager(new LinearLayoutManager(VrPlayerActivity.this,LinearLayoutManager.VERTICAL,false));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClicListener(new VrPlayerViewHolder.OnItemClickListener() {
            @Override
            public void onItemClick(View view, VrPlayerRelatedData product) {
                Intent i = new Intent(VrPlayerActivity.this, ShoppingDetailActivity.class);
                i.putExtra(ShoppingDetailActivity.ITEM_ID, product.getItem_id());
                startActivity(i);
            }
        });
        mAdapter.setOnCanccelClickListener(new VrPlayerViewHolder.OnCancelClickListener() {
            @Override
            public void onCancleClick(View view) {
                recyclerView.setVisibility(View.GONE);
            }
        });

*/
        btn=(ImageView)findViewById(R.id.btn_test);
        coackmark=(ImageView)findViewById(R.id.coackmark);
        Animation btnclick = AnimationUtils.loadAnimation(VrPlayerActivity.this, R.anim.player_btn);
        coackmark.startAnimation(btnclick);
        //  Animation btnclick = AnimationUtils.loadAnimation(VrPlayerActivity.this, R.anim.player_btn);
        //   coackmark.startAnimation(btnclick);
        //coackmark.setVisibility(View.VISIBLE);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(viewFlipper.getVisibility()==View.GONE) {
                    viewFlipper.setVisibility(View.VISIBLE);
                    is_show=true;
                    Animation slidedown = AnimationUtils.loadAnimation(VrPlayerActivity.this, R.anim.slide_down);
                    coackmark.startAnimation(slidedown);
                    coackmark.setVisibility(View.GONE);
                }
                else{
                    viewFlipper.setVisibility(View.GONE);
                    is_show=false;
                    Animation btnclick = AnimationUtils.loadAnimation(VrPlayerActivity.this, R.anim.player_btn);
                    coackmark.startAnimation(btnclick);
                    coackmark.setVisibility(View.VISIBLE);
                }
              /*  if(recyclerView.getVisibility()==View.GONE) {
                    recyclerView.setVisibility(View.VISIBLE);
                }
                else{
                    recyclerView.setVisibility(View.GONE);
                }*/
            }
        });


        // Bind input and output objects for the view.
        videoWidgetView = (VrVideoView) findViewById(R.id.video_view);
        videoWidgetView.setEventListener(new ActivityEventListener());

        loadVideoStatus = LOAD_VIDEO_STATUS_UNKNOWN;

        // Initial launch of the app or an Activity recreation due to rotation.
        handleIntent(getIntent());
    }

    /**
     * Called when the Activity is already running and it's given a new intent.
     */
    @Override
    protected void onNewIntent(Intent intent) {
        Log.i(TAG, this.hashCode() + ".onNewIntent()");
        // Save the intent. This allows the getIntent() call in onCreate() to use this new Intent during
        // future invocations.
        setIntent(intent);
        // Load the new image.
        handleIntent(intent);
    }

    /**
     * Load custom videos based on the Intent or load the default video. See the Javadoc for this
     * class for information on generating a custom intent via adb.
     */
    private void handleIntent(Intent intent) {
        // Determine if the Intent contains a file to load.

//        fileUri=intent.getData();
        fileUri = Uri.parse(intent.getStringExtra(MOVIE_URL));
        movie_id=intent.getIntExtra(MOVIE_ID,8);


        // Load the bitmap in a background thread to avoid blocking the UI thread. This operation can
        // take 100s of milliseconds.
        if (backgroundVideoLoaderTask != null) {
            // Cancel any task from a previous intent sent to this activity.
            backgroundVideoLoaderTask.cancel(true);
        }
        backgroundVideoLoaderTask = new VideoLoaderTask();
        backgroundVideoLoaderTask.execute(fileUri);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putLong(STATE_PROGRESS_TIME, videoWidgetView.getCurrentPosition());
        savedInstanceState.putLong(STATE_VIDEO_DURATION, videoWidgetView.getDuration());
        savedInstanceState.putBoolean(STATE_IS_PAUSED, isPaused);
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        long progressTime = savedInstanceState.getLong(STATE_PROGRESS_TIME);
        videoWidgetView.seekTo(progressTime);
        seekBar.setMax((int) savedInstanceState.getLong(STATE_VIDEO_DURATION));
        seekBar.setProgress((int) progressTime);

        isPaused = savedInstanceState.getBoolean(STATE_IS_PAUSED);
        if (isPaused) {
            videoWidgetView.pauseVideo();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Prevent the view from rendering continuously when in the background.
        videoWidgetView.pauseRendering();
        // If the video is playing when onPause() is called, the default behavior will be to pause
        // the video and keep it paused when onResume() is called.
        isPaused = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Resume the 3D rendering.
        videoWidgetView.resumeRendering();
        // Update the text to account for the paused video in onPause().
        updateStatusText();
        setVrRelatedData();
    }

    @Override
    protected void onDestroy() {
        // Destroy the widget and free memory.
        videoWidgetView.shutdown();
        super.onDestroy();
    }

    private void togglePause() {
        if (isPaused) {
            videoWidgetView.playVideo();
        } else {
            videoWidgetView.pauseVideo();
        }
        isPaused = !isPaused;
        updateStatusText();
    }

    private void updateStatusText() {
        StringBuilder status = new StringBuilder();
        status.append(isPaused ? "Paused: " : "Playing: ");
        status.append(String.format("%.2f", videoWidgetView.getCurrentPosition() / 1000f));
        status.append(" / ");
        status.append(videoWidgetView.getDuration() / 1000f);
        status.append(" seconds.");
        Log.i("time:",""+videoWidgetView.getCurrentPosition());
        if(videoWidgetView.getCurrentPosition()>5000)
        {
            if(viewFlipper.getVisibility()==View.GONE) {
                Animation btnclick = AnimationUtils.loadAnimation(VrPlayerActivity.this, R.anim.player_btn);
                coackmark.startAnimation(btnclick);
                coackmark.setVisibility(View.VISIBLE);
            }


        }
        if(videoWidgetView.getCurrentPosition()<10000)
        {
            viewFlipper.setDisplayedChild(0);
            viewFlipper.setVisibility(View.GONE);
        }
        if(videoWidgetView.getCurrentPosition()>=10000 && videoWidgetView.getCurrentPosition()<15000)
        {
            if(count==0)
            {
                count++;
                is_show=true;
                viewFlipper.setDisplayedChild(0);
            }
            if(viewFlipper.getVisibility()==View.GONE && is_show)
            {
                viewFlipper.setVisibility(View.VISIBLE);


            }
        }
        if(videoWidgetView.getCurrentPosition()>=15000 && videoWidgetView.getCurrentPosition()<20000)
        {

            viewFlipper.setVisibility(View.GONE);

        }
        if(videoWidgetView.getCurrentPosition()>=20000 && videoWidgetView.getCurrentPosition()<25000)
        {

            if(count==1)
            {
                viewFlipper.setDisplayedChild(1);
                count++;
                is_show=true;
            }
            if(viewFlipper.getVisibility()==View.GONE && is_show)
            {

                viewFlipper.setVisibility(View.VISIBLE);

            }
        }
        if(videoWidgetView.getCurrentPosition()>=25000 && videoWidgetView.getCurrentPosition()<30000)
        {


            viewFlipper.setVisibility(View.GONE);

        }
        if(videoWidgetView.getCurrentPosition()>=30000 && videoWidgetView.getCurrentPosition()<35000)
        {

            if(count==2)
            {
                viewFlipper.setDisplayedChild(2);
                count++;
                is_show=true;
            }
            if(viewFlipper.getVisibility()==View.GONE && is_show)
            {
                viewFlipper.setVisibility(View.VISIBLE);


            }
        }
        if(videoWidgetView.getCurrentPosition()>=35000 && videoWidgetView.getCurrentPosition()<40000)
        {


            viewFlipper.setVisibility(View.GONE);


        }
        if(videoWidgetView.getCurrentPosition()>=40000 && videoWidgetView.getCurrentPosition()<45000)
        {

            if(count==3)
            {
                viewFlipper.setDisplayedChild(3);
                count++;
                is_show=true;
            }
            if(viewFlipper.getVisibility()==View.GONE && is_show)
            {
                viewFlipper.setVisibility(View.VISIBLE);


            }
        }
        if(videoWidgetView.getCurrentPosition()>=45000 && videoWidgetView.getCurrentPosition()<50000)
        {



            viewFlipper.setVisibility(View.GONE);


        }
        if(videoWidgetView.getCurrentPosition()>=50000 && videoWidgetView.getCurrentPosition()<55000)
        {

            if(count==4)
            {
                viewFlipper.showNext();
                count++;
                is_show=true;
            }
            if(viewFlipper.getVisibility()==View.GONE && is_show)
            {
                viewFlipper.setVisibility(View.VISIBLE);


            }
        }
        if(videoWidgetView.getCurrentPosition()>=55000 && videoWidgetView.getCurrentPosition()<60000)
        {



            viewFlipper.setVisibility(View.GONE);


        }
        if(videoWidgetView.getCurrentPosition()>=60000 && videoWidgetView.getCurrentPosition()<65000)
        {

            if(count==5)
            {
                viewFlipper.showNext();
                count++;
                is_show=true;
            }
            if(viewFlipper.getVisibility()==View.GONE && is_show)
            {
                viewFlipper.setVisibility(View.VISIBLE);


            }
        }
        if(videoWidgetView.getCurrentPosition()>=65000 && videoWidgetView.getCurrentPosition()<70000)
        {


            viewFlipper.setVisibility(View.GONE);


        }
        if(videoWidgetView.getCurrentPosition()>=70000 && videoWidgetView.getCurrentPosition()<75000)
        {

            if(count==6)
            {
                viewFlipper.showNext();
                count++;
                is_show=true;
            }
            if(viewFlipper.getVisibility()==View.GONE && is_show)
            {
                viewFlipper.setVisibility(View.VISIBLE);


            }
        }
        if(videoWidgetView.getCurrentPosition()>=75000 && videoWidgetView.getCurrentPosition()<80000)
        {



            viewFlipper.setVisibility(View.GONE);


        }



    }

    /**
     * When the user manipulates the seek bar, update the video position.
     */
    private class SeekBarListener implements SeekBar.OnSeekBarChangeListener {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            if (fromUser) {
                videoWidgetView.seekTo(progress);
                updateStatusText();
                is_show=true;

                if(videoWidgetView.getCurrentPosition()>=10000 && videoWidgetView.getCurrentPosition()<15000)
                {
                    count=0;
                    is_show=true;

                }

                if(videoWidgetView.getCurrentPosition()>=20000 && videoWidgetView.getCurrentPosition()<30000)
                {

                    count=1;
                    is_show=true;

                }
                if(videoWidgetView.getCurrentPosition()>=30000 && videoWidgetView.getCurrentPosition()<40000)
                {

                    count=2;
                    is_show=true;

                }
                if(videoWidgetView.getCurrentPosition()>=40000 && videoWidgetView.getCurrentPosition()<50000)
                {

                    count=3;
                    is_show=true;

                }
                if(videoWidgetView.getCurrentPosition()>=50000 && videoWidgetView.getCurrentPosition()<60000)
                {

                    count=4;
                    is_show=true;

                }
                if(videoWidgetView.getCurrentPosition()>=60000 && videoWidgetView.getCurrentPosition()<70000)
                {

                    count=5;
                    is_show=true;

                }
                if(videoWidgetView.getCurrentPosition()>=70000 && videoWidgetView.getCurrentPosition()<80000)
                {

                    count=6;
                    is_show=true;

                }


            } // else this was from the ActivityEventHandler.onNewFrame()'s seekBar.setProgress update.
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) { }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) { }
    }

    /**
     * Listen to the important events from widget.
     */
    private class ActivityEventListener extends VrVideoEventListener {
        /**
         * Called by video widget on the UI thread when it's done loading the video.
         */
        @Override
        public void onLoadSuccess() {
            Log.i(TAG, "Sucessfully loaded video " + videoWidgetView.getDuration());
            loadVideoStatus = LOAD_VIDEO_STATUS_SUCCESS;
            seekBar.setMax((int) videoWidgetView.getDuration());
            updateStatusText();
        }

        /**
         * Called by video widget on the UI thread on any asynchronous error.
         */
        @Override
        public void onLoadError(String errorMessage) {
            // An error here is normally due to being unable to decode the video format.
            loadVideoStatus = LOAD_VIDEO_STATUS_ERROR;
            Toast.makeText(
                    VrPlayerActivity.this, "Error loading video: " + errorMessage, Toast.LENGTH_LONG)
                    .show();
            Log.e(TAG, "Error loading video: " + errorMessage);
        }

        @Override
        public void onClick() {
            togglePause();
        }

        /**
         * Update the UI every frame.
         */
        @Override
        public void onNewFrame() {
            updateStatusText();
            seekBar.setProgress((int) videoWidgetView.getCurrentPosition());
        }

        /**
         * Make the video play in a loop. This method could also be used to move to the next video in
         * a playlist.
         */
        @Override
        public void onCompletion() {
            videoWidgetView.seekTo(0);
        }
    }

    /**
     * Helper class to manage threading.
     */
    class VideoLoaderTask extends AsyncTask<Uri, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Uri... uri) {
            try {
                if (uri == null || uri.length < 1 || uri[0] == null) {
                    videoWidgetView.loadVideoFromAsset("test.mp4");
                } else {
                    videoWidgetView.loadVideo(uri[0]);
                }
            } catch (IOException e) {
                // An error here is normally due to being unable to locate the file.
                loadVideoStatus = LOAD_VIDEO_STATUS_ERROR;
                // Since this is a background thread, we need to switch to the main thread to show a toast.
                videoWidgetView.post(new Runnable() {
                    @Override
                    public void run() {
                        Toast
                                .makeText(VrPlayerActivity.this, "Error opening file. ", Toast.LENGTH_LONG)
                                .show();
                    }
                });
                Log.e(TAG, "Could not open video: " + e);
            }

            return true;
        }
    }
    ImageView imageView;
    TextView itemName,itemPrice;
    VrPlayerRelatedData data;
    Button btn_close, btn_go_detail;
    void setVrRelatedData(){
        NetworkManagerOh.getInstance().setVideoClick(VrPlayerActivity.this
                , movie_id
                , new NetworkManagerOh.OnResultListener<VrPlayerResult>() {
                    @Override
                    public void onSuccess(Request request, VrPlayerResult result) {
                        Toast.makeText(VrPlayerActivity.this,result.result.getMessage(),Toast.LENGTH_SHORT).show();
                        if(result.getSuccess()==1)
                        {
                            movie_item=result.result.getMovie_item();
                            //  mAdapter.clear();
                            //  mAdapter.addAll(result.result.getMovie_item());
                            for(int i=0; i<movie_item.size(); i++)
                            {
                                View view= LayoutInflater.from(VrPlayerActivity.this).inflate(R.layout.view_vr_player,null);
                                imageView = (ImageView) view.findViewById(R.id.img_vr_related);
                                itemName = (TextView) view.findViewById(R.id.text_vr_related_name);
                                itemPrice = (TextView) view.findViewById(R.id.text_vr_related_price);
                                btn_close = (Button)view.findViewById(R.id.btn_close);
                                btn_close.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        viewFlipper.setVisibility(View.GONE);
                                        is_show=false;
                                        Animation btnclick = AnimationUtils.loadAnimation(VrPlayerActivity.this, R.anim.player_btn);
                                        coackmark.startAnimation(btnclick);
                                        coackmark.setVisibility(View.VISIBLE);
                                    }
                                });
                                final int index=i;
                                btn_go_detail = (Button) view.findViewById(R.id.btn_go_detail);
                                btn_go_detail.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent(VrPlayerActivity.this, ShoppingDetailActivity.class);
                                        intent.putExtra(ShoppingDetailActivity.ITEM_ID, movie_item.get(index).getItem_id());
                                        startActivity(intent);
                                    }
                                });
                                Glide.with(imageView.getContext()).load(movie_item.get(index).getItem_image_url()).into(imageView);
                                itemName.setText(movie_item.get(index).getItem_name());
                                itemPrice.setText(movie_item.get(index).getItem_price() + "won");
                                viewFlipper.addView(view);

                            }
                        }
                    }

                    @Override
                    public void onFail(Request request, IOException exception) {
                        Toast.makeText(VrPlayerActivity.this,"네트워크 에러",Toast.LENGTH_SHORT).show();
                    }
                });
    }
    public View setCustomFont(View view, String name, Context context, AttributeSet attrs){
        if(view == null){
            if(name.equals("TextView")){
                view = new TextView(context,attrs);
            }
        }
        if (view != null && view instanceof TextView){
            TypedArray typedArray = context.obtainStyledAttributes(attrs, new int[]{android.R.attr.fontFamily});
            String fontFamily = typedArray.getString(0);
            typedArray.recycle();
            Typeface typeface = TypefaceManager.getInstance().getTypeface(context,fontFamily);
            if (typeface != null){
                TextView textView = (TextView)view;
                textView.setTypeface(typeface);
            }
        }
        return view;
    }

    @Override
    public View onCreateView(String name, Context context, AttributeSet attrs) {
        View view = super.onCreateView(name, context, attrs);
        if(Build.VERSION.SDK_INT < 11){
            if (view == null){
                view = mDelegate.createView(null, name, context, attrs);
                view = setCustomFont(view, name, context, attrs);
            }
        }
        return view;
    }

    @Override
    public View onCreateView(View parent, String name, Context context, AttributeSet attrs) {
        View view  = super.onCreateView(parent, name, context, attrs);
        if (view == null){
            view = mDelegate.createView(parent, name, context, attrs);
        }
        view = setCustomFont(view, name, context, attrs);

        return  view;
    }
}