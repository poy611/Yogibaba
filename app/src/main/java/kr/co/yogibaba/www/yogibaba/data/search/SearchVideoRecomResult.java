package kr.co.yogibaba.www.yogibaba.data.search;

import java.util.List;

/**
 * Created by OhDaeKyoung on 2016. 5. 26..
 */
public class SearchVideoRecomResult {
    String message;
    List<SearchVideoRecom> movies;

    public String getMessage() {
        return message;
    }

    public List<SearchVideoRecom> getMovies() {
        return movies;
    }
}
