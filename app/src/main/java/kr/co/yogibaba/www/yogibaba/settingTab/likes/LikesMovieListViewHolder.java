package kr.co.yogibaba.www.yogibaba.settingTab.likes;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import kr.co.yogibaba.www.yogibaba.R;
import kr.co.yogibaba.www.yogibaba.Splash;
import kr.co.yogibaba.www.yogibaba.data.like.LikesVideoList;

/**
 * Created by jah on 2016-05-19.
 */
public class LikesMovieListViewHolder extends RecyclerView.ViewHolder {


    public LikesMovieListViewHolder(View itemView) {
        super(itemView);
    }

    ImageView removeButton;
    ImageView imageView;
    TextView videoTitle,videoViewCount;
    ImageView btn_pub,btn_close_publ,btn_clip,btn_kakao_publ,btn_facebook_publ;
    LikesVideoList mData;

    public interface OnItemClickListener {
        public void onItemClick(View view, LikesVideoList product);
    }
    public interface OnButtonClickListener {
        public void onItemClick(View view, LikesVideoList product);
    }

    OnItemClickListener mListener;
    OnButtonClickListener bListener;

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }
    public void setOnButtonClickListener(OnButtonClickListener listener) {
        bListener = listener;
    }

    public LikesMovieListViewHolder(final View itemView, final Context context) {
        super(itemView);


        btn_pub=(ImageView)itemView.findViewById(R.id.btn_pub);

        btn_close_publ=(ImageView)itemView.findViewById(R.id.btn_close_publ);
        btn_clip=(ImageView)itemView.findViewById(R.id.btn_clip);
        btn_kakao_publ=(ImageView)itemView.findViewById(R.id.btn_kakao_publ);
        btn_facebook_publ=(ImageView)itemView.findViewById(R.id.btn_facebook_publ);

        btn_pub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Animation slideUp = AnimationUtils.loadAnimation(itemView.getContext(), R.anim.slide_up);
                Animation slideDown = AnimationUtils.loadAnimation(itemView.getContext(), R.anim.slide_down);

                btn_pub.startAnimation(slideDown);
                btn_pub.setVisibility(View.GONE);

                btn_close_publ.startAnimation(slideUp);
                btn_close_publ.setVisibility(View.VISIBLE);

                btn_clip.startAnimation(slideUp);
                btn_clip.setVisibility(View.VISIBLE);

                btn_kakao_publ.startAnimation(slideUp);
                btn_kakao_publ.setVisibility(View.VISIBLE);

                btn_facebook_publ.startAnimation(slideUp);
                btn_facebook_publ.setVisibility(View.VISIBLE);
            }
        });
        btn_close_publ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Animation slideUp = AnimationUtils.loadAnimation(itemView.getContext(), R.anim.slide_up);
                Animation slideDown = AnimationUtils.loadAnimation(itemView.getContext(), R.anim.slide_down);

                btn_close_publ.startAnimation(slideDown);
                btn_close_publ.setVisibility(View.GONE);

                btn_clip.startAnimation(slideDown);
                btn_clip.setVisibility(View.GONE);

                btn_kakao_publ.startAnimation(slideDown);
                btn_kakao_publ.setVisibility(View.GONE);

                btn_facebook_publ.startAnimation(slideDown);
                btn_facebook_publ.setVisibility(View.GONE);

                btn_pub.startAnimation(slideUp);
                btn_pub.setVisibility(View.VISIBLE);

            }
        });
        imageView=(ImageView)itemView.findViewById(R.id.imgview_video_list);
        videoTitle=(TextView)itemView.findViewById(R.id.text_video_list_title);
        videoViewCount=(TextView)itemView.findViewById(R.id.text_video_list_view_count);

        removeButton = (ImageView)itemView.findViewById(R.id.remove_button);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener!=null) {
                    mListener.onItemClick(v, mData);
                }
            }
        });
        //설정
        removeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(bListener!=null) {
                    bListener.onItemClick(v, mData);
                }
            }
        });
    }

    public void setVideoListData(LikesVideoList data){
        mData=data;
        if(data.getEdit_Type() == 1){
            removeButton.setVisibility(View.VISIBLE);
        } else {
            removeButton.setVisibility(View.GONE);
        }
//        imageView.setImageResource(R.drawable.exampler);
//        Glide.with(imageView.getContext()).load(data.getMovie_url()).into(imageView);
        Glide.with(imageView.getContext()).load(data.getMovie_thumbnail()).into(imageView);
        videoTitle.setText(data.getMovie_title());
        videoViewCount.setText(String.valueOf(data.getMovie_hit()));
    }
}