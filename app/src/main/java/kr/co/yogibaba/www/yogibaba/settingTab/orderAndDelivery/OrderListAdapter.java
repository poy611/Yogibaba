package kr.co.yogibaba.www.yogibaba.settingTab.orderAndDelivery;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import kr.co.yogibaba.www.yogibaba.R;
import kr.co.yogibaba.www.yogibaba.data.order.OrderAndDeliveryPurchases;
import kr.co.yogibaba.www.yogibaba.data.order.OrderAndDeliveryResult;
import kr.co.yogibaba.www.yogibaba.data.order.OrderAndDeliverySuccess;

/**
 * Created by jah on 2016-05-20.
 */
public class OrderListAdapter extends RecyclerView.Adapter<OrderListViewHolder>{
    public static final String CONDITON_0 = "배송준비";
    public static final String CONDITON_1 = "배송중";
    public static final String CONDITON_2 = "구매완료";
    public static final String CONDITON_3 = "환불완료";

    public Button btn_order_detail;
    public TextView text_order_price;
    public TextView text_order_date;
    public TextView order_condition;



    List<OrderAndDeliveryPurchases> mData=new ArrayList<>();
    OrderAndDeliverySuccess success;
    void add(OrderAndDeliveryPurchases data)
    {
        mData.add(data);
        notifyDataSetChanged();
    }
    void addAll(List<OrderAndDeliveryPurchases> mData)
    {

        this.mData.addAll(mData);
        notifyDataSetChanged();
    }
    void clear(){
        mData.clear();
        notifyDataSetChanged();
    }

    public void setData(OrderAndDeliverySuccess success){
        this.success = success;
        notifyDataSetChanged();
    }



    @Override
    public OrderListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_order_list_outline,null);
        btn_order_detail = (Button)view.findViewById(R.id.btn_order_detail);

        text_order_date = (TextView)view.findViewById(R.id.text_order_date);
        text_order_price = (TextView)view.findViewById(R.id.text_order_price);
        order_condition = (TextView)view.findViewById(R.id.text_purchase_condition);

        return new OrderListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(OrderListViewHolder holder, final int position) {
        holder.setList(mData.get(position).getPurchase_item());
        btn_order_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onButtonClick(mData.get(position));
                }
            }
        });
        text_order_date.setText(mData.get(position).getPurchase_date());
        text_order_price.setText(""+mData.get(position).getTotal_price());
        switch (mData.get(position).getOrder_category()){
            case 0 :
                order_condition.setText(CONDITON_0);
                break;
            case 1:
                order_condition.setText(CONDITON_1);
                break;
            case 2:
                order_condition.setText(CONDITON_2);
                break;
            case 3:
                order_condition.setText(CONDITON_3);
                break;
        }
    }

    @Override
    public int getItemCount() {
        int size;
        size = mData.size();
        return size;
    }
    public interface OnButtonClickListenr{
        void onButtonClick(OrderAndDeliveryPurchases product);
    }
    OnButtonClickListenr mListener;
    public void setOnButtonClickListner(OnButtonClickListenr listener){
        mListener=listener;
    }

    public OrderAndDeliverySuccess getSuccess() {
        return success;
    }


}