package kr.co.yogibaba.www.yogibaba.settingTab.orderAndDelivery;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import kr.co.yogibaba.www.yogibaba.R;
import kr.co.yogibaba.www.yogibaba.data.order.OrderDetailPurchases;

/**
 * Created by jah on 2016-05-26.
 */
public class OrderDetailFinishAdapter extends RecyclerView.Adapter<OrderDetailFinishViewHolder>  {
    OrderDetailPurchases purchasesData;

    public void setItems(OrderDetailPurchases purchasesData){
        this.purchasesData = purchasesData;
        notifyDataSetChanged();
    }
//    public void clear(){
//        iData.clear();
//        notifyDataSetChanged();
//    }
//
//    public void add(OrderDetailPurchases data){
//        iData.add(data);
//        notifyDataSetChanged();
//    }
//
//    public void addall(List<OrderDetailPurchases> iData){
//        this.iData.addAll(iData);
//        notifyDataSetChanged();
//    }

    @Override
    public OrderDetailFinishViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_order_detail_bottom_entry,null);
        return new OrderDetailFinishViewHolder(view);
    }

    @Override
    public void onBindViewHolder(OrderDetailFinishViewHolder holder, int position) {
        holder.setOrderConditionData(purchasesData);
    }

    @Override
    public int getItemCount() {
        return 0;
    }
}
