package kr.co.yogibaba.www.yogibaba.shoppingtab.productbuy;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;


import kr.co.yogibaba.www.yogibaba.R;
import kr.co.yogibaba.www.yogibaba.data.buy.BuyItem;
import kr.co.yogibaba.www.yogibaba.data.shoppingdetail.buy.BuyProductData;

/**
 * Created by OhDaeKyoung on 2016. 5. 19..
 */
public class ProductBuyAdapter extends RecyclerView.Adapter<ProductBuyViewHolder> {


    List<BuyItem> items=new ArrayList<>();
    public void add(BuyItem item)
    {
        items.add(item);
        notifyDataSetChanged();
    }
    public void addAll(List<BuyItem> items)
    {
        this.items.addAll(items);
        notifyDataSetChanged();
    }
    public void clear(){
        items.clear();
        notifyDataSetChanged();
    }

    @Override
    public ProductBuyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.view_buy_list,null);


        return new ProductBuyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ProductBuyViewHolder holder, int position) {
        holder.setBuyProductData(items.get(position));

    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}
