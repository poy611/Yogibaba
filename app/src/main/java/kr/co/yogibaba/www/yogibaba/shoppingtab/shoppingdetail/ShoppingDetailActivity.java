package kr.co.yogibaba.www.yogibaba.shoppingtab.shoppingdetail;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.io.IOException;
import java.util.List;

import kr.co.yogibaba.www.yogibaba.R;
import kr.co.yogibaba.www.yogibaba.StartActivity;
import kr.co.yogibaba.www.yogibaba.StartLogonActivity;
import kr.co.yogibaba.www.yogibaba.VrPlayerActivity;
import kr.co.yogibaba.www.yogibaba.data.etc.Click;
import kr.co.yogibaba.www.yogibaba.data.shoppingdetail.ItemOptions;
import kr.co.yogibaba.www.yogibaba.data.shoppingdetail.ShoppingDetailRelateData;
import kr.co.yogibaba.www.yogibaba.data.shoppingdetail.ShoppingDetailResult;
import kr.co.yogibaba.www.yogibaba.etc.CustomDialog;
import kr.co.yogibaba.www.yogibaba.facebook.FacebookActivity;
import kr.co.yogibaba.www.yogibaba.manager.NetworkManagerOh;
import kr.co.yogibaba.www.yogibaba.manager.PropertyManager;
import kr.co.yogibaba.www.yogibaba.manager.TypefaceManager;
import kr.co.yogibaba.www.yogibaba.settingTab.basket.BasketMainActivity;
import okhttp3.Request;


public class ShoppingDetailActivity extends AppCompatActivity {

    public static final String ITEM_ID="itemid";

    String user_id= null;

    int item_id;
    boolean is_like=false;
    RecyclerView recyclerView;
    ShoppingDetailAdapter mAdapter;
    ShoppingDetailResult resultData;
    ActionBar actionBar;
    ImageView img_shopping_detail,img_shopping_video,img_shopping_detail_toolin;
    Button btn_shopping_detail_info,btn_shopping_detail_qna,btn_shopping_detail_buy,btn_go_to_buy;
    ImageView btn_shopping_detail_hart;
    TextView text_shopping_detail_price,text_shopping_detail_like,text_shopping_detail_review_count;
    TextView text_temp,text_temp2;
    //content_shopping_detail의 text_temp (연관 상품 / 360 VR 영상)의 폰트 수정을 위해 추가
    private CustomDialog mCustomDialog;
    List<ItemOptions> itemOptions;
    Button btn;
    ImageView btn_pub,btn_close_publ,btn_clip,btn_kakao_publ,btn_facebook_publ;
    String itemName;
    ImageView imgbtn_review;
    FacebookActivity activity;

    AppCompatDelegate mDelegate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //글꼴테스트
        if (Build.VERSION.SDK_INT < 11){
            getLayoutInflater().setFactory(this);
        } else {
            getLayoutInflater().setFactory2(this);
        }
        mDelegate = getDelegate();
        //글꼴테스트
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_detail);
        btn_pub=(ImageView)findViewById(R.id.btn_pub);

        btn_close_publ=(ImageView)findViewById(R.id.btn_close_publ);
        btn_clip=(ImageView)findViewById(R.id.btn_clip);
        btn_kakao_publ=(ImageView)findViewById(R.id.btn_kakao_publ);
        btn_facebook_publ=(ImageView)findViewById(R.id.btn_facebook_publ);

        btn_pub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Animation slideUp = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_up);
                Animation slideDown = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_down);

                btn_pub.startAnimation(slideDown);
                btn_pub.setVisibility(View.GONE);


                btn_close_publ.startAnimation(slideUp);
                btn_close_publ.setVisibility(View.VISIBLE);

                btn_clip.startAnimation(slideUp);
                btn_clip.setVisibility(View.VISIBLE);

                btn_kakao_publ.startAnimation(slideUp);
                btn_kakao_publ.setVisibility(View.VISIBLE);

                btn_facebook_publ.startAnimation(slideUp);
                btn_facebook_publ.setVisibility(View.VISIBLE);
            }
        });
        btn_close_publ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Animation slideUp = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_up);
                Animation slideDown = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_down);

                btn_close_publ.startAnimation(slideDown);
                btn_close_publ.setVisibility(View.GONE);

                btn_clip.startAnimation(slideDown);
                btn_clip.setVisibility(View.GONE);

                btn_kakao_publ.startAnimation(slideDown);
                btn_kakao_publ.setVisibility(View.GONE);

                btn_facebook_publ.startAnimation(slideDown);
                btn_facebook_publ.setVisibility(View.GONE);
                btn_pub.startAnimation(slideUp);
                btn_pub.setVisibility(View.VISIBLE);

            }
        });

        btn_facebook_publ.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (PropertyManager.getInstance().isLogin()){
                    Toast.makeText(ShoppingDetailActivity.this,"페이스북으로 요기바바 공유하기",Toast.LENGTH_SHORT).show();
                    activity = new FacebookActivity();
                    activity.post();
                }
                else
                {
                    Toast.makeText(ShoppingDetailActivity.this,"페이스북 로그인이 필요합니다",Toast.LENGTH_LONG).show();
                }
            }
        });

        btn_clip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(ShoppingDetailActivity.this,"추가 공유 서비스 준비중",Toast.LENGTH_LONG).show();
            }
        });

        btn_kakao_publ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(ShoppingDetailActivity.this,"카카오톡 서비스 준비중",Toast.LENGTH_LONG).show();
            }
        });


        user_id= PropertyManager.getInstance().getUserId();
        item_id=getIntent().getIntExtra(ITEM_ID,1);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar=getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(null);

        text_shopping_detail_price=(TextView)findViewById(R.id.text_shopping_detail_price);
        text_shopping_detail_review_count=(TextView)findViewById(R.id.text_shopping_detail_review_count);
        text_shopping_detail_like=(TextView)findViewById(R.id.text_shopping_detail_like);

        btn_shopping_detail_hart=(ImageView)findViewById(R.id.btn_shopping_detail_hart);
        btn_shopping_detail_hart.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (user_id != null && user_id!="") {

                    if (!is_like) {

                        NetworkManagerOh.getInstance().onClickLikeButton(ShoppingDetailActivity.this
                                , item_id
                                , user_id
                                , new NetworkManagerOh.OnResultListener<Click>() {
                                    @Override
                                    public void onSuccess(Request request, Click result) {
                                        Toast.makeText(ShoppingDetailActivity.this, result.result.getMessage(), Toast.LENGTH_SHORT).show();

                                        if (result.getSuccess() == 1) {
                                            onResume();
                                        }

                                    }

                                    @Override
                                    public void onFail(Request request, IOException exception) {
                                        Toast.makeText(ShoppingDetailActivity.this, "네트워크 에러", Toast.LENGTH_SHORT).show();
                                    }
                                });
                    } else {
                        NetworkManagerOh.getInstance().onClickUnLikeButton(ShoppingDetailActivity.this,
                                item_id
                                , user_id
                                , new NetworkManagerOh.OnResultListener<Click>() {
                                    @Override
                                    public void onSuccess(Request request, Click result) {
                                        Toast.makeText(ShoppingDetailActivity.this, result.result.getMessage(), Toast.LENGTH_SHORT).show();
                                        if (result.getSuccess() == 1) {
                                            onResume();
                                        }
                                    }

                                    @Override
                                    public void onFail(Request request, IOException exception) {
                                        Toast.makeText(ShoppingDetailActivity.this, "네트워크 에러", Toast.LENGTH_SHORT).show();
                                    }
                                });
                    }
                }
                else{
                    Toast.makeText(ShoppingDetailActivity.this, "로그인이 필요합니다", Toast.LENGTH_SHORT).show();
                }
            }
        });


        img_shopping_detail_toolin=(ImageView)findViewById(R.id.img_shopping_detail_toolin);

        img_shopping_detail=(ImageView)findViewById(R.id.img_shopping_detail);
        text_temp = (TextView)findViewById(R.id.text_temp);
        text_temp2 = (TextView)findViewById(R.id.text_temp2);
        img_shopping_video=(ImageView)findViewById(R.id.img_shopping_video);
        img_shopping_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(ShoppingDetailActivity.this,VrPlayerActivity.class);
                Uri uri=Uri.parse(resultData.result.getMovie_url());
                i.putExtra(VrPlayerActivity.MOVIE_ID, resultData.result.getMovie_id());
                i.putExtra(VrPlayerActivity.MOVIE_URL, resultData.result.getMovie_url());
                //i.putExtra(ITEM_ID,)
                startActivity(i);
            }
        });
        btn_shopping_detail_info=(Button)findViewById(R.id.btn_shopping_detail_info);
        btn_shopping_detail_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(ShoppingDetailActivity.this,ShoppingDetailPageActivity.class);
                i.putExtra(ITEM_ID, item_id);
                startActivity(i);
            }
        });
        imgbtn_review=(ImageView)findViewById(R.id.imgbtn_review);
        imgbtn_review.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(ShoppingDetailActivity.this,ShoppingDetailReviewActivity.class);
                i.putExtra(ITEM_ID,item_id);
                startActivity(i);
            }
        });
        btn_shopping_detail_qna=(Button)findViewById(R.id.btn_shopping_detail_qna);
        btn_shopping_detail_qna.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(ShoppingDetailActivity.this,ShoppingDetailReviewActivity.class);
                i.putExtra(ITEM_ID,item_id);
                startActivity(i);
            }
        });
        btn_shopping_detail_buy=(Button)findViewById(R.id.btn_shopping_detail_buy);
        btn_shopping_detail_buy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(ShoppingDetailActivity.this,ShoppingDetailDelivInfoActivity.class);
                startActivity(i);
            }
        });
        btn_go_to_buy=(Button)findViewById(R.id.btn_go_to_buy);     //다이얼로그 띄워야함
        btn_go_to_buy.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if(user_id!="" && user_id!=null) {
                    mCustomDialog=new CustomDialog(ShoppingDetailActivity.this,item_id,user_id,itemOptions,null,null);
                    mCustomDialog.show();
                }
                else{
                    Toast.makeText(ShoppingDetailActivity.this,"로그인이 필요합니다",Toast.LENGTH_SHORT).show();
                }



            }
        });
        recyclerView=(RecyclerView)findViewById(R.id.rv_shopping_detail);
        recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));
        mAdapter=new ShoppingDetailAdapter();
        mAdapter.setOnItemClicListener(new ShoppingDetailViewHolder.OnItemClickListener() {
            @Override
            public void onItemClick(View view, ShoppingDetailRelateData product) {
                Intent i = new Intent(ShoppingDetailActivity.this, ShoppingDetailActivity.class);
                i.putExtra(ITEM_ID, product.getItem_id());
                startActivity(i);
            }
        });
        recyclerView.setAdapter(mAdapter);


    }

    @Override
    protected void onResume() {
        super.onResume();
        setData();
    }

    void setData(){
//        네트워크르르 이용해 데이터를 가져온다
        // user_id=null;
        if(user_id==null || user_id=="")   // 로그인 x일때
        {
            NetworkManagerOh.getInstance().getShoppingDetail(ShoppingDetailActivity.this,
                    item_id
                    , new NetworkManagerOh.OnResultListener<ShoppingDetailResult>() {
                        @Override
                        public void onSuccess(Request request, ShoppingDetailResult result) {
                            Glide.with(img_shopping_detail_toolin.getContext()).load(result.result.getItem_image_url()).into(img_shopping_detail_toolin);
                            Glide.with(img_shopping_video.getContext()).load(result.result.getMovie_thumbnail()).into(img_shopping_video);
                            Glide.with(img_shopping_detail.getContext()).load(result.result.getItem_intro()).into(img_shopping_detail);

                            text_shopping_detail_price.setText(result.result.getItem_price() + " won");
                            text_shopping_detail_like.setText("" + result.result.getItem_like_num());
                            text_shopping_detail_review_count.setText(""+result.result.getReview_num());
                            itemName=result.result.getItem_name();
                            mAdapter.clear();
                            mAdapter.addAll(result.result.getItem_relate_items());
                            itemOptions=result.result.getItem_option();
                            is_like=result.result.is_like();
                            resultData=result;
                            actionBar.setTitle(itemName);
                            if(is_like) {
                                btn_shopping_detail_hart.setImageResource(R.drawable.likebtn_press);
                            }
                            else{
                                btn_shopping_detail_hart.setImageResource(R.drawable.likebtn_nomal);
                            }
                        }

                        @Override
                        public void onFail(Request request, IOException exception) {
                            Toast.makeText(ShoppingDetailActivity.this,"네트워크 에러",Toast.LENGTH_SHORT).show();
                        }
                    });
        }
        else{
            NetworkManagerOh.getInstance().getShoppingDetailLogin(ShoppingDetailActivity.this,
                    item_id
                    , user_id
                    , new NetworkManagerOh.OnResultListener<ShoppingDetailResult>() {
                        @Override
                        public void onSuccess(Request request, ShoppingDetailResult result) {
                            Glide.with(img_shopping_detail_toolin.getContext()).load(result.result.getItem_image_url()).into(img_shopping_detail_toolin);
                            Glide.with(img_shopping_video.getContext()).load(result.result.getMovie_thumbnail()).into(img_shopping_video);
                            Glide.with(img_shopping_detail.getContext()).load(result.result.getItem_intro()).into(img_shopping_detail);

                            text_shopping_detail_price.setText(result.result.getItem_price() + "won");

                            text_shopping_detail_like.setText("" + result.result.getItem_like_num());

                            itemName = result.result.getItem_name();
                            mAdapter.clear();
                            mAdapter.addAll(result.result.getItem_relate_items());
                            itemOptions = result.result.getItem_option();
                            is_like = result.result.is_like();
                            resultData = result;
                            actionBar.setTitle(itemName);
                            if(is_like) {
                                btn_shopping_detail_hart.setImageResource(R.drawable.likebtn_press);
                            }
                            else{
                                btn_shopping_detail_hart.setImageResource(R.drawable.likebtn_nomal);
                            }
                        }

                        @Override
                        public void onFail(Request request, IOException exception) {
                            Toast.makeText(ShoppingDetailActivity.this, "네트워크 에러", Toast.LENGTH_SHORT).show();

                        }
                    });

        }


//        data.result의 리스트를 얻어와 apater에 뿌려줘야한다



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.mene_shopping, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int type=item.getItemId();
        if(type==android.R.id.home)
        {
            finish();
        }
        if(type==R.id.menu_home)
        {
            if(user_id!="" && user_id!=null) {
                Intent i = new Intent(ShoppingDetailActivity.this, StartLogonActivity.class);
                startActivity(i);
                finish();
            }
            else{
                Intent i = new Intent(ShoppingDetailActivity.this, StartActivity.class);
                startActivity(i);
                finish();
            }
        }
        if(type==R.id.menu_interest)
        {
            if(user_id!="" && user_id!=null) {
                Intent i=new Intent(ShoppingDetailActivity.this, BasketMainActivity.class);
                startActivity(i);
                finish();
            }
            else{
                Toast.makeText(ShoppingDetailActivity.this,"로그인이 필요합니다",Toast.LENGTH_SHORT).show();
            }


        }
        return super.onOptionsItemSelected(item);
    }

    public View setCustomFont(View view, String name, Context context, AttributeSet attrs){
        if(view == null){
            if(name.equals("TextView")){
                view = new TextView(context,attrs);
            }
        }
        if (view != null && view instanceof TextView){
            TypedArray typedArray = context.obtainStyledAttributes(attrs, new int[]{android.R.attr.fontFamily});
            String fontFamily = typedArray.getString(0);
            typedArray.recycle();
            Typeface typeface = TypefaceManager.getInstance().getTypeface(context,fontFamily);
            if (typeface != null){
                TextView textView = (TextView)view;
                textView.setTypeface(typeface);
            }
        }
        return view;
    }

    @Override
    public View onCreateView(String name, Context context, AttributeSet attrs) {
        View view = super.onCreateView(name, context, attrs);
        if(Build.VERSION.SDK_INT < 11){
            if (view == null){
                view = mDelegate.createView(null, name, context, attrs);
                view = setCustomFont(view, name, context, attrs);
            }
        }
        return view;
    }

    @Override
    public View onCreateView(View parent, String name, Context context, AttributeSet attrs) {
        View view  = super.onCreateView(parent, name, context, attrs);
        if (view == null){
            view = mDelegate.createView(parent, name, context, attrs);
        }
        view = setCustomFont(view, name, context, attrs);

        return  view;
    }
}