package kr.co.yogibaba.www.yogibaba.shoppingtab.productbuy;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import kr.co.yogibaba.www.yogibaba.R;
import kr.co.yogibaba.www.yogibaba.data.buy.BuyItem;
import kr.co.yogibaba.www.yogibaba.data.shoppingdetail.buy.BuyProductData;

/**
 * Created by OhDaeKyoung on 2016. 5. 19..
 */
public class ProductBuyViewHolder extends RecyclerView.ViewHolder {
    ImageView imageView;
    TextView text_item_title,text_item_price,text_item_option,text_item_count;
    TextView text_buy_info,text_buy_price;
    public ProductBuyViewHolder(View itemView) {
        super(itemView);
        imageView=(ImageView)itemView.findViewById(R.id.img_buy_item);
        text_item_title=(TextView)itemView.findViewById(R.id.text_item_title);
        text_item_price=(TextView)itemView.findViewById(R.id.text_item_price);
        text_item_option=(TextView)itemView.findViewById(R.id.text_item_option);
        text_item_count=(TextView)itemView.findViewById(R.id.text_item_count);

        text_buy_info=(TextView)itemView.findViewById(R.id.text_buy_info);
        text_buy_price=(TextView)itemView.findViewById(R.id.text_buy_price);
    }
    public void setBuyProductData(BuyItem data)
    {
        Glide.with(imageView.getContext()).load(data.getItem_image_url()).into(imageView);
        text_item_title.setText(data.getItem_name());
        text_item_price.setText(data.getItem_price() + "won");
        text_item_option.setText("옵션 : " + data.getOption_name());
        text_item_count.setText("수량 :" + data.getItem_count());
        text_buy_info.setText(data.getItem_name()+"/"+data.getOption_name()+" X "+data.getItem_count()+"개");
        text_buy_price.setText((data.getItem_price()+data.getOption_price())+" won");
    }
}
