package kr.co.yogibaba.www.yogibaba.videoTab.videodetail;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.io.IOException;

import kr.co.yogibaba.www.yogibaba.R;
import kr.co.yogibaba.www.yogibaba.VrPlayerActivity;
import kr.co.yogibaba.www.yogibaba.data.ClickLike;
import kr.co.yogibaba.www.yogibaba.data.ClickUnLike;
import kr.co.yogibaba.www.yogibaba.data.etc.Click;
import kr.co.yogibaba.www.yogibaba.data.videodetail.ItemInMovie;
import kr.co.yogibaba.www.yogibaba.data.videodetail.ItemRelatedInMovie;
import kr.co.yogibaba.www.yogibaba.data.videodetail.VideoDetailResults;
import kr.co.yogibaba.www.yogibaba.facebook.FacebookActivity;
import kr.co.yogibaba.www.yogibaba.manager.NetworkManagerOh;
import kr.co.yogibaba.www.yogibaba.manager.PropertyManager;
import kr.co.yogibaba.www.yogibaba.manager.TypefaceManager;
import kr.co.yogibaba.www.yogibaba.shoppingtab.shoppingdetail.ShoppingDetailActivity;
import okhttp3.Request;


public class VideoDetailActivity extends AppCompatActivity {
    public static final String MOVIE_ID="movie_id";
    //임시로 로그인 햇다고 가정

    ////////////

    int movieId;

    String user_id;

    String movie_url=null;
    boolean is_movie_like=false;

    VideoDetailAdapter mAdapter;
    RecyclerView recyclerView;
    GridLayoutManager manager;
    ImageView imageView;
    TextView movie_strapline,movie_like_num,movie_date,movie_hit;
    ImageView btn_pub,btn_close_publ,btn_clip,btn_kakao_publ,btn_facebook_publ;
    String movieTitle=null;
    ImageView btn;
    android.support.v7.app.ActionBar actionBar;
    AppCompatDelegate mDelegate;
    FacebookActivity activity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //글꼴테스트
        if (Build.VERSION.SDK_INT < 11){
            getLayoutInflater().setFactory(this);
        } else {
            getLayoutInflater().setFactory2(this);
        }
        mDelegate = getDelegate();
//글꼴테스트
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_detail);

        movieId=getIntent().getIntExtra(MOVIE_ID, 1);
        user_id=PropertyManager.getInstance().getUserId();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar=getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(null);
        //이미지 버튼 공유하기
        btn_pub=(ImageView)findViewById(R.id.btn_pub);

        btn_close_publ=(ImageView)findViewById(R.id.btn_close_publ);
        btn_clip=(ImageView)findViewById(R.id.btn_clip);
        btn_kakao_publ=(ImageView)findViewById(R.id.btn_kakao_publ);
        btn_facebook_publ=(ImageView)findViewById(R.id.btn_facebook_publ);

        btn_pub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Animation slideUp = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_up);
                Animation slideDown = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_down);

                btn_pub.startAnimation(slideDown);
                btn_pub.setVisibility(View.GONE);


                btn_close_publ.startAnimation(slideUp);
                btn_close_publ.setVisibility(View.VISIBLE);

                btn_clip.startAnimation(slideUp);
                btn_clip.setVisibility(View.VISIBLE);

                btn_kakao_publ.startAnimation(slideUp);
                btn_kakao_publ.setVisibility(View.VISIBLE);

                btn_facebook_publ.startAnimation(slideUp);
                btn_facebook_publ.setVisibility(View.VISIBLE);
            }
        });
        btn_close_publ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Animation slideUp = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_up);
                Animation slideDown = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_down);



                btn_close_publ.startAnimation(slideDown);
                btn_close_publ.setVisibility(View.GONE);

                btn_clip.startAnimation(slideDown);
                btn_clip.setVisibility(View.GONE);

                btn_kakao_publ.startAnimation(slideDown);
                btn_kakao_publ.setVisibility(View.GONE);

                btn_facebook_publ.startAnimation(slideDown);
                btn_facebook_publ.setVisibility(View.GONE);
                btn_pub.startAnimation(slideUp);
                btn_pub.setVisibility(View.VISIBLE);

            }
        });
        btn_facebook_publ.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (PropertyManager.getInstance().isLogin()){
                    Toast.makeText(VideoDetailActivity.this,"페이스북으로 요기바바 공유하기",Toast.LENGTH_SHORT).show();
                    activity = new FacebookActivity();
                    activity.post();
                }
                else
                {
                    Toast.makeText(VideoDetailActivity.this,"페이스북 로그인이 필요합니다",Toast.LENGTH_LONG).show();
                }
            }
        });

        btn_clip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(VideoDetailActivity.this,"추가 공유 서비스 준비중",Toast.LENGTH_LONG).show();
            }
        });

        btn_kakao_publ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(VideoDetailActivity.this,"카카오톡 서비스 준비중",Toast.LENGTH_LONG).show();
            }
        });


        movie_strapline=(TextView)findViewById(R.id.text_video_detail_strapline_head);

        movie_like_num=(TextView)findViewById(R.id.text_video_detail_like_num_head);

        movie_date=(TextView)findViewById(R.id.text_video_detail_date_head);
        movie_hit=(TextView)findViewById(R.id.text_video_detail_head_hit);

        imageView=(ImageView)findViewById(R.id.img_video_detail_toolin);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(VideoDetailActivity.this, VrPlayerActivity.class);
                if (movie_url != null) {
                    Uri uri = Uri.parse(movie_url);
                    intent.setDataAndType(uri, "video/mp4");
                }
                intent.putExtra(VrPlayerActivity.MOVIE_ID, movieId);
                intent.putExtra(VrPlayerActivity.MOVIE_URL, movie_url);
                startActivity(intent);

            }
        });

        btn=(ImageView)findViewById(R.id.btn_video_like);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (user_id != null && user_id !="") {

                    if (!is_movie_like) {
                        //btn.setImageResource(R.drawable.likebtn_press);

                        NetworkManagerOh.getInstance().setEventLikeClick(VideoDetailActivity.this
                                , movieId
                                , user_id
                                , new NetworkManagerOh.OnResultListener<ClickLike>() {
                                    @Override
                                    public void onSuccess(Request request, ClickLike result) {
                                        if (result.getSuccess() == 1) {

                                            Toast.makeText(VideoDetailActivity.this, result.result.getMessage(), Toast.LENGTH_SHORT).show();
                                            onResume();
                                        } else {
                                            Toast.makeText(VideoDetailActivity.this, result.result.getMessage(), Toast.LENGTH_SHORT).show();
                                        }
                                    }

                                    @Override
                                    public void onFail(Request request, IOException exception) {
                                        Toast.makeText(VideoDetailActivity.this, "네트워크 애러", Toast.LENGTH_SHORT).show();
                                    }
                                });
                    } else {
                        // btn.setImageResource(R.drawable.likebtn_nomal);

                        NetworkManagerOh.getInstance().setEventUNLikeClick(VideoDetailActivity.this
                                , movieId
                                , user_id
                                , new NetworkManagerOh.OnResultListener<ClickUnLike>() {
                                    @Override
                                    public void onSuccess(Request request, ClickUnLike result) {
                                        if (result.getSuccess() == 1) {

                                            Toast.makeText(VideoDetailActivity.this, result.result.getMessage(), Toast.LENGTH_SHORT).show();
                                            onResume();
                                        } else {
                                            Toast.makeText(VideoDetailActivity.this, result.result.getMessage(), Toast.LENGTH_SHORT).show();
                                        }
                                    }

                                    @Override
                                    public void onFail(Request request, IOException exception) {
                                        Toast.makeText(VideoDetailActivity.this, "네트워크 애러", Toast.LENGTH_SHORT).show();
                                    }
                                });
                    }

                } else {
                    Toast.makeText(VideoDetailActivity.this, "로그인이 필요합니다", Toast.LENGTH_SHORT).show();
                }
            }
        });
        recyclerView=(RecyclerView)findViewById(R.id.rv_video_detail);
        mAdapter=new VideoDetailAdapter();
        mAdapter.setOnItemClicListener(new VideoDetailViewHolder.OnItemClickListener() {
            @Override
            public void onItemClick(View view, ItemInMovie product) {
                Intent intent=new Intent(VideoDetailActivity.this, ShoppingDetailActivity.class);
                intent.putExtra(ShoppingDetailActivity.ITEM_ID,product.getItem_id());
                startActivity(intent);
            }
        });
        mAdapter.setOnItemClicListener(new videoDetailRelatedViewHolder.OnItemClickListener() {
            @Override
            public void onItemClick(View view, ItemRelatedInMovie product) {
                Intent intent = new Intent(VideoDetailActivity.this, ShoppingDetailActivity.class);
                intent.putExtra(ShoppingDetailActivity.ITEM_ID, product.getItem_id());
                startActivity(intent);
            }
        });
        mAdapter.setOnButtonClickListener(new videoDetailRelatedViewHolder.OnButtonClickListener() {
            @Override
            public void onButtonClick(View view, ItemRelatedInMovie product,ImageView btn) {
                if (user_id != null && user_id != "") {
                    if (!product.is_like()) {
                        // product.setIs_like(!product.is_like());
                        // btn.setImageResource(R.drawable.likebtn01_press);
                        NetworkManagerOh.getInstance().onClickLikeButton(VideoDetailActivity.this
                                , product.getItem_id()
                                , user_id
                                , new NetworkManagerOh.OnResultListener<Click>() {
                                    @Override
                                    public void onSuccess(Request request, Click result) {
                                        Toast.makeText(VideoDetailActivity.this, result.result.getMessage(), Toast.LENGTH_SHORT).show();
                                        onResume();
                                    }

                                    @Override
                                    public void onFail(Request request, IOException exception) {
                                        Toast.makeText(VideoDetailActivity.this, "네트워크 에러", Toast.LENGTH_SHORT).show();
                                    }
                                });
                    } else {
                        // product.setIs_like(!product.is_like());
                        // btn.setImageResource(R.drawable.likebtn01_nomal);
                        NetworkManagerOh.getInstance().onClickUnLikeButton(VideoDetailActivity.this
                                , product.getItem_id()
                                , user_id
                                , new NetworkManagerOh.OnResultListener<Click>() {
                                    @Override
                                    public void onSuccess(Request request, Click result) {
                                        Toast.makeText(VideoDetailActivity.this, result.result.getMessage(), Toast.LENGTH_SHORT).show();
                                        onResume();

                                    }

                                    @Override
                                    public void onFail(Request request, IOException exception) {
                                        Toast.makeText(VideoDetailActivity.this, "네트워크 에러", Toast.LENGTH_SHORT).show();
                                    }
                                });
                    }
                }
                else{
                    Toast.makeText(VideoDetailActivity.this, "로그인이 필요합니다", Toast.LENGTH_SHORT).show();

                }
            }

        });
        manager=new GridLayoutManager(this,2);
        manager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                int type = mAdapter.getItemViewType(position);
                if (type == mAdapter.BODY_OF_ON_MOVIE_TYPE) {
                    return 2;
                } else if (type == mAdapter.BODY_OF_RELATED_MOVIE) {
                    return 1;
                } else {
                    return 2;
                }
            }
        });
        recyclerView.setAdapter(mAdapter);
        recyclerView.setLayoutManager(manager);
        recyclerView.addItemDecoration(new CustomMovieDetailDivider(VideoDetailActivity.this, mAdapter.items.size()));


    }

    @Override
    protected void onResume() {
        super.onResume();
        setData();


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int type=item.getItemId();
        if(type==android.R.id.home)
        {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    void setData(){


        if(user_id!=null && user_id !="")
        {
            NetworkManagerOh.getInstance().getVideoDetailListLogin(VideoDetailActivity.this,
                    movieId,
                    user_id
                    , new NetworkManagerOh.OnResultListener<VideoDetailResults>() {
                        @Override
                        public void onSuccess(Request request, VideoDetailResults result) {
                            if (result.getSuccess() == 1) {
                                Glide.with(imageView.getContext()).load(result.result.getMovie_thumbnail()).into(imageView);
                                movie_hit.setText("" + result.result.getMovie_hit());
                                movie_date.setText("" + result.result.getMovie_date().substring(0,10));
                                movie_like_num.setText("" + result.result.getMovie_like_num());
                                movie_strapline.setText(result.result.getMovie_strapline());

                                movieTitle=result.result.getMovie_title();
                                mAdapter.clear();
                                mAdapter.addAll(result.result.getMovie_item(), result.result.getMovie_relate_item());
                                movie_url=result.result.getMovie_url();
                                is_movie_like=result.result.is_movie_like();
                                actionBar.setTitle(movieTitle);
                                if(is_movie_like){
                                    btn.setImageResource(R.drawable.likebtn_press);
                                }
                                else{
                                    btn.setImageResource(R.drawable.likebtn_nomal);
                                }
                            }
                        }

                        @Override
                        public void onFail(Request request, IOException exception) {
                            Toast.makeText(VideoDetailActivity.this, "안됨", Toast.LENGTH_SHORT).show();

                        }
                    });
        }
        else {
            NetworkManagerOh.getInstance().getVideoDetailList(VideoDetailActivity.this, movieId, new NetworkManagerOh.OnResultListener<VideoDetailResults>() {
                @Override
                public void onSuccess(Request request, VideoDetailResults result) {
                    if (result.getSuccess() == 1) {
                        Glide.with(imageView.getContext()).load(result.result.getMovie_thumbnail()).into(imageView);
                        movie_hit.setText("조회수 : " + result.result.getMovie_hit());
                        movie_date.setText("" + result.result.getMovie_date().substring(0, 10));
                        movie_like_num.setText("" + result.result.getMovie_like_num());
                        movie_strapline.setText(result.result.getMovie_strapline());

                        movieTitle = result.result.getMovie_title();
                        mAdapter.clear();
                        mAdapter.addAll(result.result.getMovie_item(), result.result.getMovie_relate_item());
                        movie_url = result.result.getMovie_url();
                        is_movie_like = result.result.is_movie_like();
                        actionBar.setTitle(movieTitle);
                        if (is_movie_like) {
                            btn.setImageResource(R.drawable.likebtn_press);
                        } else {
                            btn.setImageResource(R.drawable.likebtn_nomal);
                        }
                    }
                }

                @Override
                public void onFail(Request request, IOException exception) {
                    Toast.makeText(VideoDetailActivity.this, "안됨ㅋㅋ", Toast.LENGTH_SHORT).show();

                }
            });
        }

    }
    public View setCustomFont(View view, String name, Context context, AttributeSet attrs){
        if(view == null){
            if(name.equals("TextView")){
                view = new TextView(context,attrs);
            }
        }
        if (view != null && view instanceof TextView){
            TypedArray typedArray = context.obtainStyledAttributes(attrs, new int[]{android.R.attr.fontFamily});
            String fontFamily = typedArray.getString(0);
            typedArray.recycle();
            Typeface typeface = TypefaceManager.getInstance().getTypeface(context,fontFamily);
            if (typeface != null){
                TextView textView = (TextView)view;
                textView.setTypeface(typeface);
            }
        }
        return view;
    }

    @Override
    public View onCreateView(String name, Context context, AttributeSet attrs) {
        View view = super.onCreateView(name, context, attrs);
        if(Build.VERSION.SDK_INT < 11){
            if (view == null){
                view = mDelegate.createView(null, name, context, attrs);
                view = setCustomFont(view, name, context, attrs);
            }
        }
        return view;
    }

    @Override
    public View onCreateView(View parent, String name, Context context, AttributeSet attrs) {
        View view  = super.onCreateView(parent, name, context, attrs);
        if (view == null){
            view = mDelegate.createView(parent, name, context, attrs);
        }
        view = setCustomFont(view, name, context, attrs);

        return  view;
    }
}