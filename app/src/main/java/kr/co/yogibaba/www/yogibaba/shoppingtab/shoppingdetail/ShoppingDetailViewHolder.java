package kr.co.yogibaba.www.yogibaba.shoppingtab.shoppingdetail;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import kr.co.yogibaba.www.yogibaba.R;
import kr.co.yogibaba.www.yogibaba.data.shoppingdetail.ShoppingDetailRelateData;

/**
 * Created by OhDaeKyoung on 2016. 5. 18..
 */
public class ShoppingDetailViewHolder extends RecyclerView.ViewHolder {
    ImageView imageView;
    TextView title,price;
    ShoppingDetailRelateData mData;
    Context context;

    public ShoppingDetailViewHolder(View itemView) {
        super(itemView);
        imageView=(ImageView)itemView.findViewById(R.id.img_shopping_detail_relate);
        title=(TextView)itemView.findViewById(R.id.text_shopping_detail_relate_title);
        price=(TextView)itemView.findViewById(R.id.text_shopping_detail_relate_price);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener!=null)
                {
                    mListener.onItemClick(v,mData);
                }
            }
        });

    }
    void setShoppingDetailRelateData(ShoppingDetailRelateData data)
    {
        mData=data;
        //비트맵팩토리이용해서 유알엘을 통한 이미지 가져오기 작업 필요
        //mData.get이미지를 이용하라
        Glide.with(imageView.getContext()).load(data.getItem_image_url()).into(imageView);

        title.setText(data.getItem_name());
        price.setText(data.getItem_price()+"won");
    }



    public interface OnItemClickListener {
        public void onItemClick(View view, ShoppingDetailRelateData product);
    }

    OnItemClickListener mListener;

    public void setOnItemClickListener(OnItemClickListener listener) {

        mListener = listener;
    }

}