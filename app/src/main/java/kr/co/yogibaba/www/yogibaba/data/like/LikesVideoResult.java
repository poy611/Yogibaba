package kr.co.yogibaba.www.yogibaba.data.like;

import java.util.List;

/**
 * Created by jah on 2016-05-30.
 */
public class LikesVideoResult {
    public String message;
    public List<LikesVideoList> user_movie_likes;
}
