package kr.co.yogibaba.www.yogibaba.data.search;

import java.util.List;

/**
 * Created by OhDaeKyoung on 2016. 5. 26..
 */
public class SearchVideoResult {
    String message;
    List<SearchVideo> movies;

    public String getMessage() {
        return message;
    }

    public List<SearchVideo> getMovies() {
        return movies;
    }
}
