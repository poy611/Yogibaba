package kr.co.yogibaba.www.yogibaba.shoppingtab.shoppingdetail.review;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import kr.co.yogibaba.www.yogibaba.R;
import kr.co.yogibaba.www.yogibaba.data.review.Reviews;
import kr.co.yogibaba.www.yogibaba.videoTab.videodetail.VideoDetailViewHolder;


/**
 * Created by OhDaeKyoung on 2016. 5. 19..
 */
public class ReviewViewHolder extends RecyclerView.ViewHolder {
    TextView textData, textId, textContent;
    ImageView btn;
    Reviews data;


    public ReviewViewHolder(View itemView) {
        super(itemView);
        textData=(TextView)itemView.findViewById(R.id.text_review_data);
        textId=(TextView)itemView.findViewById(R.id.text_review_user_id);
        textContent=(TextView)itemView.findViewById(R.id.text_review_content);
        btn=(ImageView)itemView.findViewById(R.id.btn_review_delete);


    }
    public void setReview(Reviews data)
    {
        this.data=data;
        textData.setText(""+data.getReview_date().substring(0,10));
        textContent.setText(data.getReview_content());
        textId.setText("ID : "+data.getUser_id());
    }
}
