package kr.co.yogibaba.www.yogibaba.settingTab.basket;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

import kr.co.yogibaba.www.yogibaba.R;
import kr.co.yogibaba.www.yogibaba.StartLogonActivity;
import kr.co.yogibaba.www.yogibaba.data.basket.Basket;
import kr.co.yogibaba.www.yogibaba.data.basket.BasketResults;
import kr.co.yogibaba.www.yogibaba.data.basket.CustomBasket;
import kr.co.yogibaba.www.yogibaba.data.etc.Click;
import kr.co.yogibaba.www.yogibaba.etc.CustomEditDialog;
import kr.co.yogibaba.www.yogibaba.manager.NetworkManagerOh;
import kr.co.yogibaba.www.yogibaba.manager.PropertyManager;
import kr.co.yogibaba.www.yogibaba.manager.TypefaceManager;
import kr.co.yogibaba.www.yogibaba.shoppingtab.productbuy.ProductBuyActivity;
import okhttp3.Request;

public class BasketMainActivity extends AppCompatActivity {

    RecyclerView listview;
    //    RecyclerView checkView;
    BasketAdapter mAdapter;
    String user_id;
    TextView textCount, textPrice, textTotalPrice;
    Button btnShopping, btnBuy;
    TextView textOrderPrice;
    AppCompatDelegate mDelegate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //글꼴테스트
        if (Build.VERSION.SDK_INT < 11){
            getLayoutInflater().setFactory(this);
        } else {
            getLayoutInflater().setFactory2(this);
        }
        mDelegate = getDelegate();
        //글꼴테스트
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basket_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        textCount=(TextView)findViewById(R.id.text_basket);
        textPrice=(TextView)findViewById(R.id.text_basket_price);
        textTotalPrice=(TextView)findViewById(R.id.text_basket_totalprice);
        textOrderPrice=(TextView)findViewById(R.id.text_basket_delivery_fee);
        btnShopping=(Button)findViewById(R.id.btn_basket_goshopping);
        btnShopping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(BasketMainActivity.this, StartLogonActivity.class);
                startActivity(i);
                finish();
            }
        });
        btnBuy=(Button)findViewById(R.id.btn_basket_buy);
        btnBuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(BasketMainActivity.this, ProductBuyActivity.class);
                i.putExtra(ProductBuyActivity.TYPE_BUY, 1);
                i.putExtra(ProductBuyActivity.USER_ID, user_id);
                startActivity(i);
            }
        });


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        user_id= PropertyManager.getInstance().getUserId();

        listview = (RecyclerView)findViewById(R.id.rv_list);
        mAdapter = new BasketAdapter();
        mAdapter.setOnDeleteButtonClickListener(new BasketAdapter.OnDeleteButtonClickListener() {
            @Override
            public void OnDeleteButtonClick(View v, Basket product) {
                NetworkManagerOh.getInstance().deleteBasket(this
                        , user_id
                        , product.getItem_id()
                        , product.getOption_name()
                        , product.getOption_price()
                        , product.getItem_count()
                        , new NetworkManagerOh.OnResultListener<Click>() {
                    @Override
                    public void onSuccess(Request request, Click result) {
                        Toast.makeText(BasketMainActivity.this, result.result.getMessage(), Toast.LENGTH_SHORT).show();
                        onResume();
                    }

                    @Override
                    public void onFail(Request request, IOException exception) {
                        Toast.makeText(BasketMainActivity.this, "네트워크 에러", Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });

        mAdapter.setOnChangeButtonClickListener(new BasketViewHolder.OnChangeButtonClickListener() {
            @Override
            public void onChangeButtonClick(View v, Basket product) {
                CustomEditDialog mCutomDialog = new CustomEditDialog(BasketMainActivity.this
                        , product.getItem_id(), user_id, product.getItem_option());
                mCutomDialog.setOnItemClickListener(new CustomEditDialog.onItemClickListener() {
                    @Override
                    public void onItemClick(Context context, CustomBasket product) {
                        Request request = NetworkManagerOh.getInstance().updateBasket(this
                                , product.getUser_id()
                                , product.getItem_id()
                                , product.getItem_otion()
                                , product.getItem_price()
                                , product.getItem_count()
                                , new NetworkManagerOh.OnResultListener<Click>() {
                            @Override
                            public void onSuccess(Request request, Click result) {
                                Toast.makeText(BasketMainActivity.this, result.result.getMessage(), Toast.LENGTH_SHORT).show();
                                onResume();
                            }

                            @Override
                            public void onFail(Request request, IOException exception) {
                                Toast.makeText(BasketMainActivity.this, "네트워크에러", Toast.LENGTH_SHORT).show();
                            }
                        });

                    }
                });

                mCutomDialog.show();



            }
        });
        listview.setAdapter(mAdapter);

        listview.setLayoutManager(new LinearLayoutManager(this));


    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("resume","gg");
        setData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void setData() {
        NetworkManagerOh.getInstance().getBasketList(this, user_id
                , new NetworkManagerOh.OnResultListener<BasketResults>() {
            @Override
            public void onSuccess(Request request, BasketResults result) {
                Toast.makeText(BasketMainActivity.this, result.result.getMessage(), Toast.LENGTH_SHORT).show();
                if (result.getSuccess() == 1) {
                    mAdapter.clear();
                    mAdapter.addall(result.result.getUser_basket());
                    textCount.setText("검색결과 : " + result.result.getUser_basket().size() + "개");
                    int totalPrice = 0;
                    for (int i = 0; i < result.result.getUser_basket().size(); i++) {
                        totalPrice += (result.result.getUser_basket().get(i).getItem_price() + result.result.getUser_basket().get(i).getOption_price())
                                * result.result.getUser_basket().get(i).getItem_count();
                    }
                    textPrice.setText("상품 가격 : " + totalPrice);
                    textTotalPrice.setText("총 상품 가격 : " + (totalPrice + 2500));
                }
                if (result.result.getUser_basket().size()==0){
                    btnBuy.setVisibility(View.GONE);
                    textTotalPrice.setVisibility(View.GONE);
                    textOrderPrice.setText("장바구니가 비었습니다");
                }
            }

            @Override
            public void onFail(Request request, IOException exception) {
                Toast.makeText(BasketMainActivity.this, "네트워크 에러", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public View setCustomFont(View view, String name, Context context, AttributeSet attrs){
        if(view == null){
            if(name.equals("TextView")){
                view = new TextView(context,attrs);
            }
        }
        if (view != null && view instanceof TextView){
            TypedArray typedArray = context.obtainStyledAttributes(attrs, new int[]{android.R.attr.fontFamily});
            String fontFamily = typedArray.getString(0);
            typedArray.recycle();
            Typeface typeface = TypefaceManager.getInstance().getTypeface(context,fontFamily);
            if (typeface != null){
                TextView textView = (TextView)view;
                textView.setTypeface(typeface);
            }
        }
        return view;
    }

    @Override
    public View onCreateView(String name, Context context, AttributeSet attrs) {
        View view = super.onCreateView(name, context, attrs);
        if(Build.VERSION.SDK_INT < 11){
            if (view == null){
                view = mDelegate.createView(null, name, context, attrs);
                view = setCustomFont(view, name, context, attrs);
            }
        }
        return view;
    }

    @Override
    public View onCreateView(View parent, String name, Context context, AttributeSet attrs) {
        View view  = super.onCreateView(parent, name, context, attrs);
        if (view == null){
            view = mDelegate.createView(parent, name, context, attrs);
        }
        view = setCustomFont(view, name, context, attrs);

        return  view;
    }

}
