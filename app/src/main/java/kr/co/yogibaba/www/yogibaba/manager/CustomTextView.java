package kr.co.yogibaba.www.yogibaba.manager;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by jah on 2016-06-09.
 */
public class CustomTextView extends TextView {
    public CustomTextView(Context context) {
        super(context);
        init();
    }

    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {

        setTypeface(TypefaceManager.getInstance().getTypeface(getContext(),TypefaceManager.FONT_NAME_REGULAR));
        setTypeface(TypefaceManager.getInstance().getTypeface(getContext(),TypefaceManager.FONT_NAME_MEDIUM));
        setTypeface(TypefaceManager.getInstance().getTypeface(getContext(),TypefaceManager.FONT_NAME_BOLD));
        setTypeface(TypefaceManager.getInstance().getTypeface(getContext(),TypefaceManager.FONT_NAME_TEST));
    }
}
