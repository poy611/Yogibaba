package kr.co.yogibaba.www.yogibaba.search.videosearch;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import kr.co.yogibaba.www.yogibaba.R;
import kr.co.yogibaba.www.yogibaba.data.search.SearchVideo;
import kr.co.yogibaba.www.yogibaba.data.videolist.VideoListData;

/**
 * Created by OhDaeKyoung on 2016. 5. 16..
 */
public class VideoSearchAdapter extends RecyclerView.Adapter<VideoSearchViewHolder> {
    //Context context;
    //영상목록이랑 같은 형식이므로 같은 데이터 클래스를 사용한다
   List<SearchVideo> items=new ArrayList<>();
    public void add(SearchVideo item){
        items.add(item);
        notifyDataSetChanged();
    }
    public void addAll(List<SearchVideo> items)
    {
        this.items.addAll(items);
        notifyDataSetChanged();
    }
    public void clear(){
        items.clear();
        notifyDataSetChanged();
    }
    @Override
    public int getItemCount() {
        return items.size();
    }



    @Override
    public VideoSearchViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.view_video_search,null);
        return new VideoSearchViewHolder(view);
    }

    @Override
    public void onBindViewHolder(VideoSearchViewHolder holder, int position) {
        holder.setVideoListData(items.get(position));
        holder.setOnItemClickListener(mListener);
        holder.setOnButtonClickListener(bListener);
    }
    VideoSearchViewHolder.OnItemClickListener mListener;
    public void setOnItemClicListener(VideoSearchViewHolder.OnItemClickListener listener){
        mListener=listener;
    }
    VideoSearchViewHolder.OnButtonClickListener bListener;
    public void setOnButtonClickListener(VideoSearchViewHolder.OnButtonClickListener listener){
        bListener=listener;
    }
}
