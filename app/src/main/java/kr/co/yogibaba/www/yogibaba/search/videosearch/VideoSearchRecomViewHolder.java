package kr.co.yogibaba.www.yogibaba.search.videosearch;


import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.w3c.dom.Text;

import kr.co.yogibaba.www.yogibaba.R;
import kr.co.yogibaba.www.yogibaba.data.SearchRecomData;
import kr.co.yogibaba.www.yogibaba.data.search.SearchVideoRecom;

/**
 * Created by OhDaeKyoung on 2016. 5. 16..
 */
public class VideoSearchRecomViewHolder extends RecyclerView.ViewHolder{
    ImageView imageView;
    TextView textView;
    SearchVideoRecom mData;
    public VideoSearchRecomViewHolder(View itemView) {
        super(itemView);
        textView=(TextView)itemView.findViewById(R.id.text_recom_search);
        imageView=(ImageView)itemView.findViewById(R.id.img_recom_search);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener!=null)
                {
                    mListener.onItemClick(v,mData);
                }
            }
        });
    }
    public void setSearchRecomData(SearchVideoRecom data){
        mData=data;
        Glide.with(imageView.getContext()).load(data.getMovie_thumbnail()).into(imageView);
        textView.setText(data.getMovie_title());

    }
    public interface OnItemClickListener {
        public void onItemClick(View view, SearchVideoRecom product);
    }

    OnItemClickListener mListener;

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

}
