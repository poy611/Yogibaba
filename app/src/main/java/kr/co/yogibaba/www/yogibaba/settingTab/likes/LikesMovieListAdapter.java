package kr.co.yogibaba.www.yogibaba.settingTab.likes;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import kr.co.yogibaba.www.yogibaba.R;
import kr.co.yogibaba.www.yogibaba.data.like.LikesVideoList;

/**
 * Created by jah on 2016-05-19.
 */
public class LikesMovieListAdapter extends RecyclerView.Adapter<LikesMovieListViewHolder> {
    Context context;
    List<LikesVideoList> likesVideoListDatas = new ArrayList<>();
    public void add(LikesVideoList item){
        likesVideoListDatas.add(item);
        notifyDataSetChanged();
    }
    public void addAll(List<LikesVideoList> items){
        this.likesVideoListDatas.addAll(items);
        notifyDataSetChanged();
    }
    public void clear(){
        likesVideoListDatas.clear();
        notifyDataSetChanged();
    }
    public LikesMovieListAdapter() {
        super();
    }
    public LikesMovieListAdapter(Context context) {
        super();
        this.context=context;
    }

    @Override
    public LikesMovieListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.view_video_list,null);
//        return new LikesMovieListViewHolder(view);
        return new LikesMovieListViewHolder(view,context);
    }
    LikesMovieListViewHolder.OnItemClickListener mListener;
    LikesMovieListViewHolder.OnButtonClickListener bListener;
    @Override
    public void onBindViewHolder(LikesMovieListViewHolder holder, int position) {
        holder.setVideoListData(likesVideoListDatas.get(position));
        holder.setOnItemClickListener(mListener);
        holder.setOnButtonClickListener(bListener);
    }

    @Override
    public int getItemCount() {
        return likesVideoListDatas.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setOnItemClickListener(LikesMovieListViewHolder.OnItemClickListener listener){
        mListener=listener;
    }
    public void setOnButtonClickListener(LikesMovieListViewHolder.OnButtonClickListener listener){
        bListener=listener;
    }
}