package kr.co.yogibaba.www.yogibaba.etc;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import kr.co.yogibaba.www.yogibaba.R;
import kr.co.yogibaba.www.yogibaba.data.etc.Click;
import kr.co.yogibaba.www.yogibaba.data.shoppingdetail.ItemOptions;
import kr.co.yogibaba.www.yogibaba.manager.NetworkManagerOh;
import kr.co.yogibaba.www.yogibaba.settingTab.basket.BasketMainActivity;
import kr.co.yogibaba.www.yogibaba.shoppingtab.productbuy.ProductBuyActivity;
import okhttp3.Request;

public class CustomDialog extends Dialog {

    private TextView count;
    private Button mLeftButton;
    private Button mRightButton;
    private Spinner spinner;
    private ImageView up;
    private ImageView down;
    private int item_id;
    private String user_id;
    private Context context;
    private View.OnClickListener mLeftClickListener;
    private View.OnClickListener mRightClickListener;
    private List<ItemOptions> itemOptions;
    private String itemOption;
    private int itemPrice;
    public CustomDialog(Context context, int item_id,
                        View.OnClickListener singleListener) {
        super(context, android.R.style.Theme_Translucent_NoTitleBar);
        this.context=context;
        this.item_id = item_id;
        this.mLeftClickListener = singleListener;
    }

    // 클릭버튼이 확인과 취소 두개일때 생성자 함수로 이벤트를 받는다
    public CustomDialog(Context context, int item_id,
                        String user_id, List<ItemOptions> itemOptions,View.OnClickListener leftListener,
                        View.OnClickListener rightListener) {
        super(context, android.R.style.Theme_Translucent_NoTitleBar);
        this.itemOptions=itemOptions;
        this.context=context;
        this.item_id = item_id;
        this.user_id = user_id;
        this.mLeftClickListener = leftListener;
        this.mRightClickListener = rightListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
        lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        lpWindow.dimAmount = 0.8f;
        getWindow().setAttributes(lpWindow);

        setContentView(R.layout.activity_custom_dialog);

        count=(TextView)findViewById(R.id.text_count);
        mLeftButton=(Button)findViewById(R.id.btn_bucket);
        mRightButton=(Button)findViewById(R.id.btn_buy);
        up=(ImageView)findViewById(R.id.btn_up);
        up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int currentCount=Integer.parseInt(count.getText().toString());
                currentCount++;
                count.setText(Integer.toString(currentCount));
            }
        });
        down=(ImageView)findViewById(R.id.btn_down);
        down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int currentCount=Integer.parseInt(count.getText().toString());
                if(currentCount>1) {
                    currentCount--;
                }
                count.setText(Integer.toString(currentCount));
            }
        });
        spinner=(Spinner)findViewById(R.id.spinner_option);
        ArrayList<String> arrayList=new ArrayList<>();
        for(int i=0; i<itemOptions.size(); i++)
        {
            arrayList.add(itemOptions.get(i).getOption_name());
        }
        final ArrayAdapter<String> adapter=new ArrayAdapter<String>(getContext(),android.R.layout.simple_spinner_item,arrayList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(view.getContext(),adapter.getItem(position),Toast.LENGTH_SHORT).show();
                itemOption=adapter.getItem(position);
                itemPrice=itemOptions.get(position).getOption_price();


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        if(mLeftClickListener==null)
        {
            mLeftClickListener=new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    NetworkManagerOh.getInstance().onClickBucketButton(getContext()
                            , item_id
                            , user_id
                            , Integer.parseInt(count.getText().toString())
                            , itemOption
                            ,itemPrice
                            , new NetworkManagerOh.OnResultListener<Click>() {
                        @Override
                        public void onSuccess(Request request, Click result) {
                            if (result.getSuccess() == 1) {
                                Toast.makeText(getContext(), result.result.getMessage(), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getContext(), result.result.getMessage(), Toast.LENGTH_SHORT).show();

                            }
                        }

                        @Override
                        public void onFail(Request request, IOException exception) {
                            Toast.makeText(getContext(), "네트 워크 에러", Toast.LENGTH_SHORT).show();

                        }
                    });
                    Intent intent=new Intent(context,BasketMainActivity.class);
                    context.startActivity(intent);
                    dismiss();

                }
            };
            mLeftButton.setOnClickListener(mLeftClickListener);
        }

        else{
            mLeftButton.setOnClickListener(mLeftClickListener);
        }
        if(mRightClickListener==null)
        {
            mRightClickListener=new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent=new Intent(context,ProductBuyActivity.class);
                    intent.putExtra(ProductBuyActivity.USER_ID,user_id);
                    intent.putExtra(ProductBuyActivity.ITEM_ID,item_id);
                    intent.putExtra(ProductBuyActivity.OPTION_NAME,itemOption);
                    intent.putExtra(ProductBuyActivity.OPTION_PRICE,itemPrice);
                    intent.putExtra(ProductBuyActivity.ITEM_COUNT, Integer.parseInt(count.getText().toString()));
                    intent.putExtra(ProductBuyActivity.TYPE_BUY,0);
                    context.startActivity(intent);
                    dismiss();
                }
            };
            mRightButton.setOnClickListener(mRightClickListener);
        }
        else
        {
            mRightButton.setOnClickListener(mRightClickListener);
        }
    }
}
