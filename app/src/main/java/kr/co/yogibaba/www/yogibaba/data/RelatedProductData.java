package kr.co.yogibaba.www.yogibaba.data;

/**
 * Created by OhDaeKyoung on 2016. 5. 16..
 */
public class RelatedProductData {
    int price;
    String productTtile;
    String imgUrl;

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getProductTtile() {
        return productTtile;
    }

    public void setProductTtile(String productTtile) {
        this.productTtile = productTtile;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }
}
