package kr.co.yogibaba.www.yogibaba.data.buy;

/**
 * Created by OhDaeKyoung on 2016. 5. 25..
 */
public class BuyResults {
    int success;
    public BuyResult result;

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }
}
