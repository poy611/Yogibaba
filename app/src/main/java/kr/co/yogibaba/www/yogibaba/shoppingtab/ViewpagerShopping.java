package kr.co.yogibaba.www.yogibaba.shoppingtab;

import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import kr.co.yogibaba.www.yogibaba.R;
import kr.co.yogibaba.www.yogibaba.data.ShoppingDataByCategory;
import kr.co.yogibaba.www.yogibaba.data.ShoppingViewPageData;

/**
 * Created by OhDaeKyoung on 2016. 5. 13..
 */
public class ViewpagerShopping extends PagerAdapter {
    LayoutInflater inflater;
    List<ShoppingDataByCategory> mData=new ArrayList<>();
    public interface OnViewPagerClickListener{
        void onViewPagerClick(View view, ShoppingDataByCategory product);

    }
    OnViewPagerClickListener mListener;
    public void setOnViewPagerClickListener(OnViewPagerClickListener listener)
    {
        mListener=listener;
    }
    public void setmData( List<ShoppingDataByCategory> data)
    {
        mData.addAll(data);
        notifyDataSetChanged();
    }
    public void clearData()
    {

            mData.clear();
            notifyDataSetChanged();

    }
    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view==object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        container.removeView((View)object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        final View view=inflater.inflate(R.layout.view_viewpager_shoppingtab,null);;

        ImageView imageView=(ImageView)view.findViewById(R.id.img_viewpager_childimage);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener!=null)
                {
                    mListener.onViewPagerClick(view,mData.get(position));
                }
            }
        });
        Glide.with(imageView.getContext()).load(mData.get(position).getItem_image_url()).into(imageView);
        container.addView(view);

        return view;
    }

    public ViewpagerShopping(LayoutInflater inflater) {
        this.inflater=inflater;
    }

}
