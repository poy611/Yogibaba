package kr.co.yogibaba.www.yogibaba.data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by OhDaeKyoung on 2016. 5. 16..
 */
public class VideoDetailData {
    String imageUrl;
    String productTitle;
    int price;
    String manufateror;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getProductTitle() {
        return productTitle;
    }

    public void setProductTitle(String productTitle) {
        this.productTitle = productTitle;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getManufateror() {
        return manufateror;
    }

    public void setManufateror(String manufateror) {
        this.manufateror = manufateror;
    }


}
