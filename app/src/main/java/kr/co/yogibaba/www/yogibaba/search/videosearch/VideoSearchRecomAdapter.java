package kr.co.yogibaba.www.yogibaba.search.videosearch;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import kr.co.yogibaba.www.yogibaba.R;
import kr.co.yogibaba.www.yogibaba.data.SearchRecomData;
import kr.co.yogibaba.www.yogibaba.data.search.SearchVideoRecom;

/**
 * Created by OhDaeKyoung on 2016. 5. 16..
 */
public class VideoSearchRecomAdapter extends RecyclerView.Adapter<VideoSearchRecomViewHolder> {
    List<SearchVideoRecom> items=new ArrayList<>();
    public void add(SearchVideoRecom item)
    {
        items.add(item);
        notifyDataSetChanged();
    }
    public void addAll(List<SearchVideoRecom> items)
    {
        this.items.addAll(items);
        notifyDataSetChanged();
    }
    public void clear(){
        items.clear();
        notifyDataSetChanged();
    }
    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public VideoSearchRecomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.view_recomand_search,null);
        return new VideoSearchRecomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(VideoSearchRecomViewHolder holder, int position) {
        holder.setSearchRecomData(items.get(position));
        holder.setOnItemClickListener(mListener);

    }
    VideoSearchRecomViewHolder.OnItemClickListener mListener;
    public void setOnItemClickListener(VideoSearchRecomViewHolder.OnItemClickListener listener){
        mListener=listener;
    }
}
