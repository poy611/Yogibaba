package kr.co.yogibaba.www.yogibaba.data.review;

/**
 * Created by OhDaeKyoung on 2016. 5. 25..
 */
public class ReviewResults {
    int success;
    public ReviewResult result;

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }
}
