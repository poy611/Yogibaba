package kr.co.yogibaba.www.yogibaba.data.search;

/**
 * Created by OhDaeKyoung on 2016. 5. 26..
 */
public class SearchVideo {
    int movie_id;
    String movie_title;
    String movie_strapline;
    int movie_hit;
    String movie_thumbnail;
    String movie_url;

    int movie_like_num;

    public int getMovie_like_num() {
        return movie_like_num;
    }

    public void setMovie_like_num(int movie_like_num) {
        this.movie_like_num = movie_like_num;
    }

    public String getMovie_thumbnail() {
        return movie_thumbnail;
    }

    public int getMovie_id() {
        return movie_id;
    }

    public String getMovie_title() {
        return movie_title;
    }

    public String getMovie_strapline() {
        return movie_strapline;
    }

    public int getMovie_hit() {
        return movie_hit;
    }

    public String getMovie_url() {
        return movie_url;
    }
}
