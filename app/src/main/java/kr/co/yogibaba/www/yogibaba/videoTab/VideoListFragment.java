package kr.co.yogibaba.www.yogibaba.videoTab;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.IOException;

import kr.co.yogibaba.www.yogibaba.CustomRecyclerDivider;
import kr.co.yogibaba.www.yogibaba.R;
import kr.co.yogibaba.www.yogibaba.data.etc.Click;
import kr.co.yogibaba.www.yogibaba.data.videolist.VideoListData;
import kr.co.yogibaba.www.yogibaba.data.videolist.VideoListResults;
import kr.co.yogibaba.www.yogibaba.manager.NetworkManagerOh;
import kr.co.yogibaba.www.yogibaba.manager.PropertyManager;
import kr.co.yogibaba.www.yogibaba.videoTab.videodetail.VideoDetailActivity;
import okhttp3.Request;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link VideoListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class VideoListFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String user_id;


    public VideoListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment VideoListFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static VideoListFragment newInstance(String param1, String param2) {
        VideoListFragment fragment = new VideoListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }
    VideoListAdapter mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        mAdapter=new VideoListAdapter(getContext());
        mAdapter.setOnItemClicListener(new VideoListViewHolder.OnItemClickListener() {
            @Override
            public void onItemClick(View view, VideoListData product) {
                Intent intent = new Intent(getContext(), VideoDetailActivity.class);
                intent.putExtra(VideoDetailActivity.MOVIE_ID, product.getMovie_id());
                startActivity(intent);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        setData();
    }

    RecyclerView recyclerView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_video_list, container, false);
        recyclerView=(RecyclerView)view.findViewById(R.id.rv_videolist);
        recyclerView.setAdapter(mAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.addItemDecoration(new CustomRecyclerDivider(getActivity()));
        return view;
    }
    private void setData(){

        NetworkManagerOh.getInstance().getVideoList(getContext(), new NetworkManagerOh.OnResultListener<VideoListResults>() {
            @Override
            public void onSuccess(Request request, VideoListResults result) {
                if(result.getSuccess()==1) {
                    //Toast.makeText(getContext(),result.result.getMessage(),Toast.LENGTH_SHORT).show();
                    mAdapter.clear();
                    mAdapter.addAll(result.result.movies);
                }
                else{
                    Toast.makeText(getContext(),result.result.getMessage(),Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFail(Request request, IOException exception) {
                Toast.makeText(getContext(),"영상 리스트 조회에 실패했습니다",Toast.LENGTH_SHORT).show();
            }
        });
    }

}
