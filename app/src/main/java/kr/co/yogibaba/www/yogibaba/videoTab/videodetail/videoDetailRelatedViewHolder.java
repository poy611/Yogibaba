package kr.co.yogibaba.www.yogibaba.videoTab.videodetail;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import kr.co.yogibaba.www.yogibaba.R;
import kr.co.yogibaba.www.yogibaba.data.videodetail.ItemRelatedInMovie;

/**
 * Created by OhDaeKyoung on 2016. 5. 17..
 */
public class videoDetailRelatedViewHolder extends RecyclerView.ViewHolder {
    ImageView imageView;
    TextView title,price;
    ItemRelatedInMovie data;
    ImageView btn;
    public interface OnItemClickListener {
        void onItemClick(View view, ItemRelatedInMovie product);
    }
    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }


    OnItemClickListener mListener;

    public interface OnButtonClickListener{
        void onButtonClick(View view, ItemRelatedInMovie product,ImageView btn);
    }

    OnButtonClickListener bListener;
    public void setOnButtonClikcListenter(OnButtonClickListener listenter)
    {
        bListener=listenter;
    }




    public videoDetailRelatedViewHolder(View itemView) {
        super(itemView);
        Context context=itemView.getContext();
        btn=(ImageView)itemView.findViewById(R.id.btn_video_detail_like);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bListener != null) {
                    bListener.onButtonClick(v, data,btn);

                }
            }
        });
        imageView=(ImageView)itemView.findViewById(R.id.img_video_detail_related);
        title=(TextView)itemView.findViewById(R.id.text_video_detail_title);

        price=(TextView)itemView.findViewById(R.id.text_video_detail_price);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener!=null)
                {
                    mListener.onItemClick(v,data);
                }
            }
        });
    }
    public void setRelatedProductData(ItemRelatedInMovie data)
    {
        this.data=data;
        if(data.is_like())
        {
            btn.setImageResource(R.drawable.likebtn01_press);
        }
        else{
            btn.setImageResource(R.drawable.likebtn01_nomal);
        }
        Glide.with(imageView.getContext()).load(data.getItem_image_url()).into(imageView);
        title.setText(data.getItem_name());
        price.setText(data.getItem_price()+"won");
    }

}
