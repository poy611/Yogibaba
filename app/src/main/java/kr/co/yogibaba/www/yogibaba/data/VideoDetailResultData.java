package kr.co.yogibaba.www.yogibaba.data;

import java.util.List;

/**
 * Created by OhDaeKyoung on 2016. 5. 17..
 */
public class VideoDetailResultData {
    List<RelatedProductData> relatedProductDatas;
    List<VideoDetailData> videoDetailDatas;

    public List<RelatedProductData> getRelatedProductDatas() {
        return relatedProductDatas;
    }

    public void setRelatedProductDatas(List<RelatedProductData> relatedProductDatas) {
        this.relatedProductDatas = relatedProductDatas;
    }

    public List<VideoDetailData> getVideoDetailDatas() {
        return videoDetailDatas;
    }

    public void setVideoDetailDatas(List<VideoDetailData> videoDetailDatas) {
        this.videoDetailDatas = videoDetailDatas;
    }

}
