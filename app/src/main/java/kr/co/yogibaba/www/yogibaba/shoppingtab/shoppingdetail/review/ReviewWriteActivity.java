package kr.co.yogibaba.www.yogibaba.shoppingtab.shoppingdetail.review;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.IOException;

import kr.co.yogibaba.www.yogibaba.R;
import kr.co.yogibaba.www.yogibaba.data.etc.Click;
import kr.co.yogibaba.www.yogibaba.data.review.ReviewResults;
import kr.co.yogibaba.www.yogibaba.manager.NetworkManagerOh;
import kr.co.yogibaba.www.yogibaba.manager.PropertyManager;
import kr.co.yogibaba.www.yogibaba.shoppingtab.shoppingdetail.ShoppingDetailActivity;
import okhttp3.Request;

public class ReviewWriteActivity extends AppCompatActivity {
    String user_id;
    int item_id;
    Button btn;
    EditText editText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_write);
        editText=(EditText)findViewById(R.id.edit_review);
        user_id= PropertyManager.getInstance().getUserId();
        item_id= getIntent().getIntExtra(ShoppingDetailActivity.ITEM_ID,1);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("리뷰쓰기");
        btn=(Button)findViewById(R.id.btn_review_write);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NetworkManagerOh.getInstance().writeReivew(ReviewWriteActivity.this
                        , item_id
                        , user_id
                        , editText.getText().toString()
                        , new NetworkManagerOh.OnResultListener<Click>() {
                    @Override
                    public void onSuccess(Request request, Click result) {
                        if(result.getSuccess()==1)
                        {
                            Toast.makeText(ReviewWriteActivity.this
                            ,result.result.getMessage(),Toast.LENGTH_SHORT).show();
                            finish();
                        }
                        else {
                            Toast.makeText(ReviewWriteActivity.this
                                    ,result.result.getMessage(),Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFail(Request request, IOException exception) {
                        Toast.makeText(ReviewWriteActivity.this
                                ,"네트워크 에러",Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });
        btn=(Button)findViewById(R.id.btn_review_cancle);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int type=item.getItemId();
        if(type==android.R.id.home)
        {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
