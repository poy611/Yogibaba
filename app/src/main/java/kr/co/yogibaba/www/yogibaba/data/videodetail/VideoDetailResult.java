package kr.co.yogibaba.www.yogibaba.data.videodetail;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by OhDaeKyoung on 2016. 5. 23..
 */
public class VideoDetailResult {
    int movie_id;
    String movie_url;
    String movie_title;
    String movie_strapline;
    int movie_hit;
    String movie_intro;
    String movie_date;
    int movie_like_num;
    boolean is_movie_like;
    List<ItemInMovie> movie_item;
    List<ItemRelatedInMovie> movie_relate_item;
    Date movieDate;
    String movie_thumbnail;
    String message;

    public String getMovie_date() {
        return movie_date;
    }

    public void setMovie_date(String movie_date) {
        this.movie_date = movie_date;
    }

    public boolean is_movie_like() {
        return is_movie_like;
    }

    public void setIs_movie_like(boolean is_movie_like) {
        this.is_movie_like = is_movie_like;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy.MM.dd");

    public void changeStringToDate() {
        try {
            movieDate = simpleDateFormat.parse(movie_date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public Date getMovieDate() {
        if(movie_date!=null){
        changeStringToDate();
        }
        return movieDate;
    }

    public void setMovieDate(Date movieDate) {
        this.movieDate = movieDate;
    }

    public String getMovie_thumbnail() {
        return movie_thumbnail;
    }

    public void setMovie_thumbnail(String movie_thumbnail) {
        this.movie_thumbnail = movie_thumbnail;
    }

    public int getMovie_id() {
        return movie_id;
    }

    public void setMovie_id(int movie_id) {
        this.movie_id = movie_id;
    }

    public String getMovie_url() {
        return movie_url;
    }

    public void setMovie_url(String movie_url) {
        this.movie_url = movie_url;
    }

    public String getMovie_title() {
        return movie_title;
    }

    public void setMovie_title(String movie_title) {
        this.movie_title = movie_title;
    }

    public String getMovie_strapline() {
        return movie_strapline;
    }

    public void setMovie_strapline(String movie_strapline) {
        this.movie_strapline = movie_strapline;
    }

    public int getMovie_hit() {
        return movie_hit;
    }

    public void setMovie_hit(int movie_hit) {
        this.movie_hit = movie_hit;
    }

    public String getMovie_intro() {
        return movie_intro;
    }

    public void setMovie_intro(String movie_intro) {
        this.movie_intro = movie_intro;
    }



    public int getMovie_like_num() {
        return movie_like_num;
    }

    public void setMovie_like_num(int movie_like_num) {
        this.movie_like_num = movie_like_num;
    }

    public List<ItemInMovie> getMovie_item() {
        return movie_item;
    }

    public void setMovie_item(List<ItemInMovie> movie_item) {
        this.movie_item = movie_item;
    }

    public List<ItemRelatedInMovie> getMovie_relate_item() {
        return movie_relate_item;
    }

    public void setMovie_relate_item(List<ItemRelatedInMovie> movie_relate_item) {
        this.movie_relate_item = movie_relate_item;
    }
}
