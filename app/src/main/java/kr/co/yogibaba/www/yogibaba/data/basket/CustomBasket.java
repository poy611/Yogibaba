package kr.co.yogibaba.www.yogibaba.data.basket;

/**
 * Created by OhDaeKyoung on 2016. 6. 2..
 */
public class CustomBasket {
    String user_id;
    int item_id;
    String item_otion;
    int item_price;
    int item_count;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public int getItem_id() {
        return item_id;
    }

    public void setItem_id(int item_id) {
        this.item_id = item_id;
    }

    public String getItem_otion() {
        return item_otion;
    }

    public void setItem_otion(String item_otion) {
        this.item_otion = item_otion;
    }

    public int getItem_price() {
        return item_price;
    }

    public void setItem_price(int item_price) {
        this.item_price = item_price;
    }

    public int getItem_count() {
        return item_count;
    }

    public void setItem_count(int item_count) {
        this.item_count = item_count;
    }
}
