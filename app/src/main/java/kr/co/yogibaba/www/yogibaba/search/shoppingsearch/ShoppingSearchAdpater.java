package kr.co.yogibaba.www.yogibaba.search.shoppingsearch;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import kr.co.yogibaba.www.yogibaba.R;
import kr.co.yogibaba.www.yogibaba.data.ShoppingDataByCategory;
import kr.co.yogibaba.www.yogibaba.data.search.SearchItem;

/**
 * Created by OhDaeKyoung on 2016. 5. 16..
 */
public class ShoppingSearchAdpater extends RecyclerView.Adapter<ShoppingSearchViewHolder> {
    List<SearchItem> items=new ArrayList<>();

    public void add(SearchItem item)
    {
        items.add(item);
        notifyDataSetChanged();
    }
    public void addAll( List<SearchItem> items)
    {
        this.items.addAll(items);
        notifyDataSetChanged();
    }
    public void clear(){
        items.clear();
        notifyDataSetChanged();
    }
    @Override
    public ShoppingSearchViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.view_shopping_search,null);

        return new ShoppingSearchViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ShoppingSearchViewHolder holder, int position) {
            holder.setShoppingDataByCategory(items.get(position));
            holder.setOnItemClickListener(mListener);
            holder.setOnLikeButtonClickListener(bListener);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    ShoppingSearchViewHolder.OnItemClickListener mListener;
    public void setOnItemClicListener(ShoppingSearchViewHolder.OnItemClickListener listener){
        mListener=listener;
    }

    ShoppingSearchViewHolder.OnLikeButtonClickListener bListener;
    public void setOnLikeButtonClickListener(ShoppingSearchViewHolder.OnLikeButtonClickListener listener)
    {
        bListener=listener;
    }

}
