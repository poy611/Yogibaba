package kr.co.yogibaba.www.yogibaba.data.login;

/**
 * Created by jah on 2016-06-07.
 */
public class LoginCheckResult {
    public int user_id;
    public String message;

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
