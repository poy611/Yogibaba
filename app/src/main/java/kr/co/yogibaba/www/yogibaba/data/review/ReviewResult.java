package kr.co.yogibaba.www.yogibaba.data.review;


import java.util.List;



/**
 * Created by OhDaeKyoung on 2016. 5. 25..
 */
public class ReviewResult {
    String message;
    List<Reviews> reviews;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Reviews> getReviews() {
        return reviews;
    }

    public void setReviews(List<Reviews> reviews) {
        this.reviews = reviews;
    }
}
