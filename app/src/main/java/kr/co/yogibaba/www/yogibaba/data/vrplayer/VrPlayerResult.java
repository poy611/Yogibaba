package kr.co.yogibaba.www.yogibaba.data.vrplayer;

/**
 * Created by OhDaeKyoung on 2016. 5. 23..
 */
public class VrPlayerResult {
    int success;
    public VrPlayerData result;

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }
}
