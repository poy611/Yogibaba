package kr.co.yogibaba.www.yogibaba.shoppingtab.shoppingdetail;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import kr.co.yogibaba.www.yogibaba.R;
import kr.co.yogibaba.www.yogibaba.data.shoppingdetail.ShoppingDetailRelateData;

/**
 * Created by OhDaeKyoung on 2016. 5. 17..
 */
public class ShoppingDetailAdapter extends RecyclerView.Adapter<ShoppingDetailViewHolder> {
    List<ShoppingDetailRelateData> items=new ArrayList<>();
    void add(ShoppingDetailRelateData item)
    {
        items.add(item);
        notifyDataSetChanged();
    }
    void addAll(List<ShoppingDetailRelateData> items)
    {
        this.items.addAll(items);
        notifyDataSetChanged();
    }
    void clear(){
        items.clear();
        notifyDataSetChanged();
    }
    @Override
    public ShoppingDetailViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.view_shopping_detail_relate,null);
        return new ShoppingDetailViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ShoppingDetailViewHolder holder, int position) {
        holder.setShoppingDetailRelateData(items.get(position));
        holder.setOnItemClickListener(mListener);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    ShoppingDetailViewHolder.OnItemClickListener mListener;
    public void setOnItemClicListener(ShoppingDetailViewHolder.OnItemClickListener listener) {
        mListener = listener;
    }
}
