package kr.co.yogibaba.www.yogibaba.data.basket;

import java.util.List;

/**
 * Created by OhDaeKyoung on 2016. 5. 27..
 */
public class BasketResult {
    String message;
    List<Basket> user_basket;
    String user_name;
    String user_profile;

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_profile() {
        return user_profile;
    }

    public void setUser_profile(String user_profile) {
        this.user_profile = user_profile;
    }

    public String getMessage() {
        return message;
    }

    public List<Basket> getUser_basket() {
        return user_basket;
    }
}
