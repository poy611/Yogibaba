package kr.co.yogibaba.www.yogibaba.search;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

import kr.co.yogibaba.www.yogibaba.R;
import kr.co.yogibaba.www.yogibaba.data.search.SearchItemRecom;
import kr.co.yogibaba.www.yogibaba.data.search.SearchItemRecomResults;
import kr.co.yogibaba.www.yogibaba.data.search.SearchItemResults;
import kr.co.yogibaba.www.yogibaba.manager.NetworkManagerOh;
import kr.co.yogibaba.www.yogibaba.search.shoppingsearch.ShoppingSearchAdpater;
import kr.co.yogibaba.www.yogibaba.search.shoppingsearch.ShoppingSearchRecomAdapter;
import kr.co.yogibaba.www.yogibaba.search.shoppingsearch.ShoppingSearchRecomViewHolder;
import kr.co.yogibaba.www.yogibaba.shoppingtab.shoppingdetail.ShoppingDetailActivity;
import okhttp3.Request;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ShoppingSearchFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ShoppingSearchFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public ShoppingSearchFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ShoppingSearchFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ShoppingSearchFragment newInstance(String param1, String param2) {
        ShoppingSearchFragment fragment = new ShoppingSearchFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }
    ShoppingSearchAdpater mAdapter;
    ShoppingSearchRecomAdapter mRecomAdapter;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        mRecomAdapter=new ShoppingSearchRecomAdapter();
        mRecomAdapter.setOnItemClickListener(new ShoppingSearchRecomViewHolder.OnItemClickListener() {
            @Override
            public void onItemClick(View view, SearchItemRecom product) {
                Intent i = new Intent(getContext(), ShoppingDetailActivity.class);
                i.putExtra(ShoppingDetailActivity.ITEM_ID,product.getItem_id());
                startActivity(i);
            }
        });

        setRecomData();

    }
    RecyclerView recyclerView;
    GridLayoutManager gridLayoutManager;
    TextView textView,recomandText;
    Spinner spinner;
    RelativeLayout relativeLayout;
    ImageView star;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_shopping_search, container, false);
        recyclerView=(RecyclerView)view.findViewById(R.id.rv_shopping_search);

        relativeLayout=(RelativeLayout)view.findViewById(R.id.rela_search);

        gridLayoutManager=new GridLayoutManager(getContext(),1);
        gridLayoutManager.setOrientation(GridLayout.HORIZONTAL);
        recyclerView.setAdapter(mRecomAdapter);
        recyclerView.setLayoutManager(gridLayoutManager);

        star=(ImageView)view.findViewById(R.id.star);
        textView=(TextView)view.findViewById(R.id.text_search_count);
        spinner=(Spinner)view.findViewById(R.id.spinner);
        recomandText=(TextView)view.findViewById(R.id.text_recomand);

        return view;
    }
    void setData(String searchKeyword,String keyWord){
        if(searchKeyword.equals("조회순")){
            NetworkManagerOh.getInstance().searchItemViewCount(getContext()
                    , keyWord
                    , new NetworkManagerOh.OnResultListener<SearchItemResults>() {
                @Override
                public void onSuccess(Request request, SearchItemResults result) {
                    Toast.makeText(getContext(),result.result.getMessage(),Toast.LENGTH_SHORT).show();
                    if(mAdapter!=null && result.getSuccess()==1)
                    {
                        mAdapter.clear();
                        mAdapter.addAll(result.result.getItems());
                        textView.setText("검색결과 :"+result.result.getItems().size());
                    }
                }

                @Override
                public void onFail(Request request, IOException exception) {
                    Toast.makeText(getContext(),"네트워크 에러",Toast.LENGTH_SHORT).show();
                }
            });
        }
        else{
            NetworkManagerOh.getInstance().searchItemLikeCount(getContext()
                    , keyWord
                    , new NetworkManagerOh.OnResultListener<SearchItemResults>() {
                @Override
                public void onSuccess(Request request, SearchItemResults result) {
                    Toast.makeText(getContext(),result.result.getMessage(),Toast.LENGTH_SHORT).show();
                    if(mAdapter!=null && result.getSuccess()==1)
                    {
                        mAdapter.clear();
                        mAdapter.addAll(result.result.getItems());
                        textView.setText("검색결과 :"+result.result.getItems().size());
                    }
                }

                @Override
                public void onFail(Request request, IOException exception) {
                    Toast.makeText(getContext(),"네트워크 에러",Toast.LENGTH_SHORT).show();
                }
            });

        }
    }
    void setRecomData(){
            NetworkManagerOh.getInstance().searchItemRecom(getContext()
                    , new NetworkManagerOh.OnResultListener<SearchItemRecomResults>() {
                @Override
                public void onSuccess(Request request, SearchItemRecomResults result) {
                    Toast.makeText(getContext(),result.result.getMessage(),Toast.LENGTH_SHORT).show();
                    if(result.getSuccess()==1){
                        mRecomAdapter.clear();
                        mRecomAdapter.addAll(result.result.getItems());

                    }
                }

                @Override
                public void onFail(Request request, IOException exception) {
                    Toast.makeText(getContext(),"네트워크 에러",Toast.LENGTH_SHORT).show();
                }
            });
    }
}
